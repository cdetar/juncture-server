# Support websocket upgrades
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

# Ignore unknown hostnames.
server {
  listen 443;
  include /etc/nginx/includes/letsencrypt_ssl.conf;
  return 404;
}

# Redirect domain aliases
{% if juncture_domain_aliases|length > 0 %}
server {
  listen 443;
  include /etc/nginx/includes/letsencrypt_ssl.conf;
  server_name {{juncture_domain_aliases|join(" ")}};
  return 301 https://{{juncture_domain}}$request_uri;
}
{% endif %}

# Main server config
server {
  listen 443;
  server_name {{django_domain}};
  include /etc/nginx/includes/letsencrypt_ssl.conf;

  # Static files and uploads
  location /static/ {
    alias {{django_static_dir}};
    # Cache static files for a long time.
    expires 30d;
    add_header Pragma public;
    add_header Cache-Control "public";
    gzip on;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;
  }
  location /media/ {
    alias {{django_media_dir}};
    # Cache static files for a long time.
    expires 30d;
    add_header Pragma public;
    add_header Cache-Control "public";
    gzip on;
    gzip_min_length 256;
    gzip_types text/plain text/css application/json application/x-javascript application/javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;
  }

  location = /favicon.ico {
    return 404;
  }

  # Django backend
  location / {
    proxy_http_version 1.1;
    proxy_set_header Host $host;
    proxy_set_header X-Forwraded-Proto $scheme;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    # https://www.nginx.com/blog/mitigating-the-httpoxy-vulnerability-with-nginx/
    proxy_set_header Proxy "";
    proxy_pass http://127.0.0.1:{{django_wsgi_port}};
  }
}



