#!/bin/bash
set -e

DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

GECKODRIVER_VERSION="0.11.1"
CHROMEDRIVER_VERSION="2.25"
PHANTOMJS_VERSION="2.1.1"

# Install geckodriver
mkdir -p $DIR/bin/

cd $DIR/bin
GECKODRIVER_SRC=https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz
GECKODRIVER_TGZ=geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz
curl -L $GECKODRIVER_SRC > "$GECKODRIVER_TGZ"
tar xzvf "$GECKODRIVER_TGZ"
rm "$GECKODRIVER_TGZ"

# Install chromedriver
cd $DIR/bin
CHROMEDRIVER_ZIP=chromedriver_linux64.zip
CHROMEDRIVER_SRC=https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/$CHROMEDRIVER_ZIP
curl -L $CHROMEDRIVER_SRC > "$CHROMEDRIVER_ZIP"
unzip "$CHROMEDRIVER_ZIP"
rm "$CHROMEDRIVER_ZIP"

# Install phantomjs
cd $DIR/bin
PHANTOMJS=phantomjs-$PHANTOMJS_VERSION-linux-x86_64
PHANTOMJS_BZ2=$PHANTOMJS.tar.bz2
PHANTOMJS_SRC=https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOMJS_BZ2
curl -L $PHANTOMJS_SRC > "$PHANTOMJS_BZ2"
tar xjvf "$PHANTOMJS_BZ2" -C . "$PHANTOMJS/bin/phantomjs"
mv "$PHANTOMJS/bin/phantomjs" .
rm -r "$PHANTOMJS"
rm "$PHANTOMJS_BZ2"
