from django.db import models
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from django_gravatar.helpers import get_gravatar_url
from sorl.thumbnail import get_thumbnail
import requests


class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **kwargs):
        user = self.model(username=username, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **kwargs):
        user = self.model(username=username, **kwargs)
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


def user_display(user):
    return user.get_display_name()


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        max_length=30,
        unique=True,
        # Forbid '@' in username to avoid confusion with emails. We allow login
        # by either username or email, and identify invitees by a charfield
        # that contains both.
        validators=[RegexValidator(regex=r"^[^@\s]+$")],
        error_messages={"unique": _("An account with that username already exists.")},
    )
    email = models.EmailField(
        _("Email address"), unique=True, blank=True, null=True, default=None
    )
    display_name = models.CharField(
        max_length=30,
        blank=True,
        help_text=_("Name to display publicly in chats and lists."),
    )
    profile_image = models.ImageField(
        upload_to="accounts/profile_images", blank=True, null=True
    )

    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []  # Used only by createsuperuser management command

    def save(self, *args, **kwargs):
        if not self.email:
            self.email = None
        return super(User, self).save(*args, **kwargs)

    def clean(self):
        super().clean()
        if self.email:
            self.email = UserManager.normalize_email(self.email)

    def get_short_name(self):
        return self.get_display_name().split()[0]

    def get_display_name(self):
        if self.display_name:
            return self.display_name

        cache_key = "display-name-{}".format(self.pk)
        cached = cache.get(cache_key)
        if cached:
            return cached
        else:
            result = self.get_display_name_nocache()
            cache.set(cache_key, result, 60)
        return result

    def get_display_name_nocache(self):
        if self.display_name:
            return self.display_name

        socialaccounts = {}
        for acct in self.socialaccount_set.all():
            socialaccounts[acct.provider] = acct

        if "facebook" in socialaccounts:
            name = socialaccounts["facebook"].extra_data.get("name")
            if name:
                return name

        if "google" in socialaccounts:
            name = socialaccounts["google"].extra_data.get("name")
            if name:
                return name

        if "twitter" in socialaccounts:
            name = socialaccounts["twitter"].extra_data.get("name")
            if name:
                return name

        return self.username

    def get_profile_image(self):
        if self.profile_image:
            return get_thumbnail(self.profile_image, "64x64", crop="center").url

        cache_key = "profile-image-{}".format(self.pk)
        cached = cache.get(cache_key)
        if cached:
            return cached
        else:
            result = self.get_profile_image_nocache()
            cache.set(cache_key, result, 60)
        return result

    def get_profile_image_nocache(self):
        if self.profile_image:
            return get_thumbnail(self.profile_image, "64x64", crop="center").url

        socialaccounts = {}
        for acct in self.socialaccount_set.all():
            socialaccounts[acct.provider] = acct

        if "facebook" in socialaccounts:
            redirect = "https://graph.facebook.com/{}/picture".format(acct.uid)
            res = requests.head(redirect)
            if res.status_code == 302:
                return res.headers["Location"]
        if "google" in socialaccounts:
            try:
                return acct.extra_data["picture"]
            except KeyError:
                pass
        if "twitter" in socialaccounts:
            try:
                return acct.extra_data["profile_image_url_https"]
            except KeyError:
                pass
        if self.email:
            return get_gravatar_url(
                self.email, size=64, secure=True, default=self.default_profile_image()
            )
        return self.default_profile_image()

    @classmethod
    def default_profile_image(cls):
        # Absolute path with protocol/domain required to feed this as a default
        # to gravatar.
        return "".join(
            (
                settings.MEDIA_PROTOCOL,
                "://",
                Site.objects.get_current().domain,
                settings.MEDIA_URL,
                "assets/default_avatar.jpg",
            )
        )

    def __str__(self):
        return self.get_display_name()
