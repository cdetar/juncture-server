from django.conf.urls import url, include
from django.shortcuts import redirect

from accounts import views

urlpatterns = [
    url("^profile/$", views.profile, name="accounts_profile"),
    url("^settings/(?P<slug>.+)?$", views.settings, name="accounts_settings"),
    url("^delete/$", views.delete_account, name="accounts_delete_account"),
    url(
        "^social/connections/$", lambda r: redirect("accounts_settings", "connections")
    ),
    url("^email/$", lambda r: redirect("accounts_settings", "email")),
    url("^", include("allauth.urls")),
]
