def serialize_auth_state(user):
    from accounts.models import User

    if not user.is_authenticated:
        return {
            "is_authenticated": False,
            "username": "Anonymous",
            "display_name": "Anonymous",
            "image": User.default_profile_image(),
        }

    return {
        "is_authenticated": True,
        "id": user.id,
        "username": user.username,
        "display_name": user.get_display_name(),
        "has_display_name": bool(user.display_name),
        "image": user.get_profile_image(),
        "has_default_image": user.get_profile_image() == User.default_profile_image(),
        "email": user.email,
        "is_superuser": user.is_superuser,
        "is_staff": user.is_staff,
    }
