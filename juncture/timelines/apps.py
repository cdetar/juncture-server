from django.apps import AppConfig


class TimelinesConfig(AppConfig):
    name = "timelines"

    def ready(self):
        from .signals import associate_who_invitees  # noqa: F401
