import csv
from io import StringIO, TextIOWrapper
from django.db import transaction
from django.http import HttpResponse, Http404, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import ensure_csrf_cookie
from openpyxl import Workbook, load_workbook
from openpyxl.writer.excel import save_virtual_workbook

from helpdocs.models import HelpText
from timelines.models import Timeline, Who
from timelines import serializers
from timelines import utils


def _prep_slide(obj):
    if obj is None:
        return None
    for key in ("start_date", "end_date", "media", "text"):
        if key in obj and not obj[key]:
            del obj[key]
    if "media" in obj and "url" not in obj["media"]:
        del obj["media"]
    obj["unique_id"] = str(obj["id"])
    return obj


@xframe_options_exempt
def timeline_view(request, secret_id):
    try:
        timeline = Timeline.objects.get(secret_id=secret_id)
    except Timeline.DoesNotExist:
        raise Http404
    if not timeline.viewable_by(request.user):
        return render(request, "timeline_permission_denied.html")

    slides = timeline.slides.viewable_by(request.user).distinct()

    eras = timeline.eras.viewable_by(request.user).distinct()
    title = slides.filter(is_title=True).first()
    events = slides.exclude(is_title=True)

    timeline_data = {
        "timeline": {
            "events": [
                _prep_slide(e)
                for e in serializers.SlideSerializer(events, many=True).data
            ],
            "eras": [
                _prep_slide(e) for e in serializers.EraSerializer(eras, many=True).data
            ],
            "scale": timeline.scale,
        }
    }
    if title:
        timeline_data["timeline"]["title"] = _prep_slide(
            serializers.SlideSerializer(title).data
        )
    timeline_data.update(serializers.CategorySerializer(timeline).data)

    return render(
        request,
        "timeline_view.html",
        {"timeline_data": timeline_data, "timeline": timeline},
    )


def timeline_export(request, secret_id, fmt):
    try:
        timeline = Timeline.objects.get(secret_id=secret_id)
    except Timeline.DoesNotExist:
        raise Http404
    if not timeline.viewable_by(request.user):
        return render(request, "timeline_permission_denied.html")

    slides = timeline.slides.viewable_by(request.user)
    eras = timeline.eras.viewable_by(request.user)

    header = [
        "Year",
        "Month",
        "Day",
        "Time",
        "End Year",
        "End Month",
        "End Day",
        "End Time",
        "Display Date",
        "Headline",
        "Text",
        "Media",
        "Media Credit",
        "Media Caption",
        "Media Thumbnail",
        "Type",
        "Group",
        "Background",
    ]
    rows = []

    for slide in slides:
        row = []
        row += utils.date_precision_to_list(slide.start_date, slide.precision)
        row += utils.date_precision_to_list(slide.end_date, slide.precision)
        row.append("")  # Display date
        row.append(slide.headline)
        row.append(slide.text)
        row.append(slide.media)
        row.append(slide.media_credit)
        row.append(slide.media_caption)
        row.append(slide.media_thumbnail)
        row.append("title" if slide.is_title else "")
        row.append(slide.group)
        row.append(slide.background_url or slide.background_color)
        rows.append(row)

    for era in eras:
        row = []
        row += utils.date_precision_to_list(era.start_date, era.precision)
        row += utils.date_precision_to_list(era.end_date, era.precision)
        row.append("")  # Display date
        row.append(era.headline)
        row.append("")  # Text
        row.append("")  # Media
        row.append("")  # Media Credit
        row.append("")  # Media Caption
        row.append("")  # Media Thumbnail
        row.append("era")  # Type
        row.append("")  # Group
        row.append("")  # Background
        rows.append(row)

    rows.sort(key=lambda r: [a or 0 for a in r[0:4]])

    if fmt == "csv":
        sio = StringIO()
        writer = csv.writer(sio)
        writer.writerow(header)
        for row in rows:
            writer.writerow(row)
        value = sio.getvalue()
        filename = "%s.csv" % slugify(timeline.name)
        content_type = "text/csv"

    elif fmt == "xlsx":
        wb = Workbook()
        ws = wb.active

        for i, row in enumerate([header] + rows):
            for letter, val in zip("ABCDEFGHIJKLMNOPQR", row):
                cell = "{}{}".format(letter, i + 1)
                ws[cell] = val

        value = save_virtual_workbook(wb)
        filename = "%s.xlsx" % slugify(timeline.name)
        content_type = "application/vnd.ms-excel"

    response = HttpResponse()
    response["Content-Type"] = content_type
    response["Content-Disposition"] = "attachment; filename={}".format(filename)
    response.write(value)
    return response


def timeline_upload(request):
    if not request.user.is_authenticated:
        return HttpResponseBadRequest("Login required.")
    if not request.method == "POST":
        return HttpResponseBadRequest("POST only")

    file_handle = request.FILES.get("file")
    file_type = request.POST.get("file_type")
    file_name = request.POST.get("file_name")
    if file_type == "text/csv":
        reader = csv.reader(TextIOWrapper(file_handle))
        cells = [r for r in reader]
    elif (
        file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    ):
        wb = load_workbook(filename=file_handle)
        ws = wb.active
        cells = []
        for row in ws.rows:
            value_row = [(cell.value or "") for cell in row]
            if any(value_row):
                cells.append(value_row)
    else:
        return HttpResponseBadRequest("Unrecognized file type %s" % file_type)

    try:
        slides, eras = utils.import_timeline_cells(cells)
    except utils.SpreadsheetFormatException as exp:
        return HttpResponseBadRequest(exp.args[0])
    try:
        with transaction.atomic():
            owner = Who.objects.canonical(user=request.user)
            timeline = Timeline(name=file_name)
            timeline.full_clean()
            timeline.save()
            timeline.owners.set([owner])
            for slide in slides:
                slide.save()
            timeline.slides.set(slides)
            for era in eras:
                era.save()
            timeline.eras.set(eras)
            return JsonResponse({"timeline_id": timeline.secret_id})
    except ValidationError as e:
        return HttpResponseBadRequest("Error: {}".format(e.message_dict))

    return HttpResponseBadRequest("Me swarmies")


@ensure_csrf_cookie
def index(request):
    help_texts = {h.key: h.text for h in HelpText.objects.all()}
    return render(
        request,
        "single_page_app.html",
        {"title": _("Timelines"), "initial_state": {"help": help_texts}},
    )
