# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-24 15:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("timelines", "0008_auto_20170228_1143")]

    operations = [
        migrations.AlterModelOptions(
            name="era", options={"ordering": ["start_date", "id"]}
        ),
        migrations.AlterModelOptions(
            name="slide", options={"ordering": ["start_date", "created", "id"]}
        ),
        migrations.RemoveField(model_name="timelinegroup", name="users"),
        migrations.AddField(
            model_name="timelinegroup",
            name="members",
            field=models.ManyToManyField(
                blank=True, related_name="groups", to="timelines.Who"
            ),
        ),
        migrations.AlterField(
            model_name="timelinegroup",
            name="owners",
            field=models.ManyToManyField(
                blank=True, related_name="groups_owned", to="timelines.Who"
            ),
        ),
    ]
