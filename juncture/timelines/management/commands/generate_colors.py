from django.core.management.base import BaseCommand
import colorsys


class Command(BaseCommand):
    ORIG = [
        "#FF6900",
        "#FCB900",
        "#7BDCB5",
        "#00D084",
        "#8ED1FC",
        "#0693E3",
        "#ABB8C3",
        "#EB144C",
        "#F78DA7",
        "#9900EF",
    ]

    def handle(self, *args, **options):
        new_colors = []
        for color in self.ORIG:
            r = int(color[1:3], 16) / 255
            g = int(color[3:5], 16) / 255
            b = int(color[5:7], 16) / 255

            h, s, v = colorsys.rgb_to_hsv(r, g, b)
            newr, newg, newb = colorsys.hsv_to_rgb(h, s / 2, v)

            new_colors.append(
                "#%02x%02x%02x" % tuple([int(a * 255) for a in (newr, newg, newb)])
            )
        print(new_colors)
