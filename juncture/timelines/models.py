from collections import OrderedDict
import datetime
import json
import re

from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import utc
from django.core.exceptions import ValidationError

from accounts.models import User
from richtext.models import RichTextField
from juncture.utils import server_now, secure_random_string


class WhoManager(models.Manager):
    def canonical(self, **kwargs):
        return self.get_or_create(expiration=Who.NO_EXPIRATION, **kwargs)[0]

    def current(self, now=None):
        now = now or server_now()
        return self.filter(expiration__gte=now)

    def associated_with(self, user, now=None):
        now = now or server_now()
        whos = user.who_set.current(now=now)
        return whos | self.filter(
            Q(group__owners__in=whos, expiration__gte=now)
            | Q(group__members__in=whos, expiration__gte=now)
        )

    def search(self, user, query):
        if not user.is_authenticated:
            return self.none()
        if not query:
            return self.none()

        tokens = query.split()
        token_regex = "(%s)" % "|".join(re.escape(t) for t in tokens)

        # Find the timelines, slides, and groups this user is associated with.
        # Consider "editable by" to be the threshold for association.

        # Start with a list of 'Who' objects that either represent this user, or a
        # group this user is in.
        associable = list(
            self.associated_with(user).order_by("id").distinct().values_list("id")
        )

        # Find all the timelines and slides that have an editor or owner with that
        # association, and groups that have a member that is that association.
        timelines = Timeline.objects.filter(
            Q(editors__in=associable) | Q(owners__in=associable)
        )
        slides = Slide.objects.filter(
            Q(editors__in=associable) | Q(owners__in=associable)
        )
        groups = TimelineGroup.objects.filter(
            Q(members__in=associable) | Q(owners__in=associable)
        )

        # Find all other who's that belong to those timelines, slides, and groups.
        # These are the "visible" who's.
        subqueries = [
            self.associated_with(user).values_list("id"),
            Timeline.owners.through.objects.filter(
                timeline_id__in=timelines
            ).values_list("who_id"),
            Timeline.editors.through.objects.filter(
                timeline_id__in=timelines
            ).values_list("who_id"),
            Timeline.viewers.through.objects.filter(
                timeline_id__in=timelines
            ).values_list("who_id"),
            Slide.owners.through.objects.filter(slide_id__in=slides).values_list(
                "who_id"
            ),
            Slide.editors.through.objects.filter(slide_id__in=slides).values_list(
                "who_id"
            ),
            Slide.viewers.through.objects.filter(slide_id__in=slides).values_list(
                "who_id"
            ),
            TimelineGroup.owners.through.objects.filter(
                timelinegroup_id__in=groups
            ).values_list("who_id"),
            TimelineGroup.members.through.objects.filter(
                timelinegroup_id__in=groups
            ).values_list("who_id"),
        ]
        visible_q = Q()
        for subquery in subqueries:
            visible_q = visible_q | Q(id__in=subquery)

        # Filter by search terms.
        search_q = Q()
        for field in [
            "user__username",
            "user__email",
            "user__display_name",
            "invitee",
            "group__name",
        ]:
            search_q |= Q(**{field + "__iregex": token_regex})

        # Retrieve the visible Who's filtered by the search terms. Uniqify to
        return self.current().filter(visible_q, search_q)


class Who(models.Model):
    # Arbitrary future date that indicates "no expiration". Duplicated in
    # frontend script.
    NO_EXPIRATION = datetime.datetime(2500, 1, 1, 0, 0, 0, 0, tzinfo=utc)

    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    group = models.ForeignKey(
        "TimelineGroup", blank=True, null=True, on_delete=models.CASCADE
    )
    public = models.BooleanField(default=False)

    invitee = models.CharField(
        max_length=255,
        blank=True,
        default="",
        help_text="Username or email for user without an account yet",
    )

    expiration = models.DateTimeField(default=NO_EXPIRATION)

    objects = WhoManager()

    def name(self):
        who = self.user or self.invitee or self.group
        if not who:
            who = "public" if self.public else "nobody"
        return str(who)

    def __str__(self):
        name = self.name()
        if self.expiration == self.NO_EXPIRATION:
            return "%s forever" % name
        if self.expiration > server_now():
            return "%s %s" % (name, self.expiration)
        return "%s expired" % name


class WhoableManager(models.Manager):
    def _can_q(self, user, accessor, via="ugo", now=None):
        now = now or server_now()

        def p(string):
            return string % accessor

        q = Q()
        if "u" in via:
            q = q | Q(**{p("%s__expiration__gt"): now, p("%s__user"): user})
        if "g" in via:
            q = q | Q(
                **{p("%s__expiration__gt"): now, p("%s__group__members__user"): user}
            )
            q = q | Q(
                **{p("%s__expiration__gt"): now, p("%s__group__owners__user"): user}
            )
        if "o" in via:
            q = q | Q(**{p("%s__expiration__gt"): now, p("%s__public"): True})
        return q

    def _via(self, user, implicit):
        if user is None or not user.is_authenticated:
            if implicit:
                return "o", None
            return "", self.none()
        elif user.is_superuser and implicit:
            return "", self.all()
        elif implicit:
            return "ugo", None
        return "ug", None

    def viewable_by(self, user, now=None, implicit=True):
        """
        Return objects viewable by the given user at the time `now` (defaults
        to current time). If `implicit` is set to False, exclude objects that
        are only viewable by virtue of being public or the user's superuser
        status.
        """
        via, qs = self._via(user, implicit)
        if qs:
            return qs
        now = now or server_now()

        q = self._can_q(user, "viewers", via, now)
        if self.model in (Era, Slide):
            q = q | (
                Q(viewers__isnull=True, editors__isnull=True)
                & self._can_q(user, "timeline__viewers", via, now)
            )

        return self.filter(q) | self.editable_by(user, now, implicit)

    def editable_by(self, user, now=None, implicit=True):
        """
        Return objects editable by the given user at the time `now` (defaults
        to current time). If `implicit` is set to False, exclude objects that
        are only editable by virtue of being public or the user's superuser
        status.
        """
        via, qs = self._via(user, implicit)
        if qs:
            return qs
        now = now or server_now()

        q = self._can_q(user, "editors", via, now)
        if self.model in (Era, Slide):
            q = q | (
                Q(viewers__isnull=True, editors__isnull=True)
                & self._can_q(user, "timeline__editors", via, now)
            )

        return self.filter(q) | self.owned_by(user, now, implicit)

    def owned_by(self, user, now=None, implicit=True):
        """
        Return objects owned by the given user at the time `now` (defaults
        to current time). If `implicit` is set to False, exclude objects that
        are only owned by virtue of being public or the user's superuser
        status.
        """
        via, qs = self._via(user, implicit)
        if qs:
            return qs
        now = now or server_now()

        q = self._can_q(user, "owners", via, now)
        if self.model in (Era, Slide):
            q = q | (
                Q(owners__isnull=True) & self._can_q(user, "timeline__owners", via, now)
            )

        return self.filter(q)


class WhoableMixin:
    """
    Instance methods for assessing 'Who'-based permissions. Assumes that the
    model is using WhoableManager as its default manager, and that the object
    has 'viewers', 'editors', and 'owners' as ManyToMany relations to Who.
    """

    def current_viewers(self):
        return self.viewers.current()

    def current_editors(self):
        return self.editors.current()

    def current_owners(self):
        return self.owners.current()

    def viewable_by(self, user, now=None, implicit=True):
        return (
            self.__class__.objects.viewable_by(user, now=now, implicit=implicit)
            .filter(pk=self.pk)
            .exists()
        )

    def editable_by(self, user, now=None, implicit=True):
        return (
            self.__class__.objects.editable_by(user, now=now, implicit=implicit)
            .filter(pk=self.pk)
            .exists()
        )

    def owned_by(self, user, now=None, implicit=True):
        return (
            self.__class__.objects.owned_by(user, now=now, implicit=implicit)
            .filter(pk=self.pk)
            .exists()
        )


class TimelineGroupManager(WhoableManager):
    def with_member(self, user, now=None):
        q = self._can_q(user, "members", via="u", now=now)
        return self.filter(q) | self.owned_by(user)

    def owned_by(self, user, now=None):
        q = self._can_q(user, "owners", via="u", now=now)
        return self.filter(q)


class TimelineGroup(models.Model):
    name = models.CharField(max_length=100)
    members = models.ManyToManyField(Who, blank=True, related_name="groups")
    owners = models.ManyToManyField(Who, blank=True, related_name="groups_owned")

    objects = TimelineGroupManager()

    def owned_by(self, user, now=None):
        return TimelineGroup.objects.owned_by(user, now=now).filter(pk=self.pk).exists()

    def __str__(self):
        return self.name


PRECISION_CHOICES = (
    ("year", _("Year")),
    ("month", _("Month")),
    ("day", _("Day")),
    ("hour", _("Hour")),
    ("minute", _("Minute")),
    ("second", _("Second")),
    ("millisecond", _("Millisecond")),
)


class Era(models.Model, WhoableMixin):
    headline = models.CharField(max_length=140)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    precision = models.CharField(
        max_length=20, choices=PRECISION_CHOICES, blank=True, default="day"
    )

    owners = models.ManyToManyField(Who, blank=True, related_name="eras_owned")
    viewers = models.ManyToManyField(Who, blank=True, related_name="eras_viewable")
    editors = models.ManyToManyField(Who, blank=True, related_name="eras_editable")

    objects = WhoableManager()

    def __str__(self):
        return "{}: {}...".format(
            self.start_date.strftime("%y-%m-%d"), self.headline[0:10]
        )

    class Meta:
        ordering = ["start_date", "id"]


class Slide(models.Model, WhoableMixin):
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    precision = models.CharField(
        max_length=20, choices=PRECISION_CHOICES, blank=True, default="day"
    )
    headline = RichTextField(max_length=140, blank=True, default="")
    text = RichTextField(blank=True, default="")
    media = models.TextField(blank=True, default="")
    media_credit = RichTextField(blank=True, default="")
    media_caption = RichTextField(blank=True, default="")
    media_thumbnail = models.TextField(blank=True, default="")
    is_title = models.BooleanField(default=False)
    group = models.CharField(max_length=140, blank=True, default="")

    background_color = models.TextField(blank=True, default="")
    background_url = models.TextField(blank=True, default="")

    created = models.DateTimeField(default=server_now)

    owners = models.ManyToManyField(Who, blank=True, related_name="slides_owned")
    viewers = models.ManyToManyField(Who, blank=True, related_name="slides_viewable")
    editors = models.ManyToManyField(Who, blank=True, related_name="slides_editable")

    objects = WhoableManager()

    def clean(self):
        super().clean()
        if self.start_date is None and self.end_date is not None:
            self.is_title = True
            self.precision = ""
        if self.end_date is not None and self.start_date is None:
            self.start_date = self.end_date
            self.end_date = None
        self._validate_model()

    def _validate_model(self):
        if not any((self.headline, self.text, self.media)):
            raise ValidationError(
                {"headline": _("One of 'headline', 'text', or 'media' " "is required")}
            )
        if not self.is_title and self.start_date is None:
            raise ValidationError(
                {"start_date": _("Start date required unless title is checked")}
            )

    def save(self, *args, **kwargs):
        self._validate_model()
        return super().save(*args, **kwargs)

    def __str__(self):
        date = self.start_date.strftime("%Y-%m-%d") if self.start_date else "title"
        return "{}: {}...".format(
            date, (self.headline or self.text or self.media)[0:10]
        )

    class Meta:
        ordering = ["start_date", "created", "id"]


class Timeline(models.Model, WhoableMixin):
    CATEGORY_COLORS = [
        "#ffb37f",
        "#fcda7e",
        "#abdcc8",
        "#68d0aa",
        "#c5e6fc",
        "#74bae3",
        "#b6bdc3",
        "#eb7f9b",
        "#f7c2cf",
        "#c477ef",
        "#f2f2f2",
        "#000000",
    ]
    name = models.CharField(max_length=140)
    slides = models.ManyToManyField(Slide, blank=True)
    eras = models.ManyToManyField(Era, blank=True)
    scale = models.CharField(
        max_length=len("cosmological"),
        choices=(("cosmological", _("Cosmological")), ("human", _("Human"))),
        default="human",
    )
    secret_id = models.CharField(
        max_length=32, default=secure_random_string, unique=True
    )

    categories = models.TextField(default="{}")

    created = models.DateTimeField(default=server_now)

    owners = models.ManyToManyField(Who, blank=True, related_name="timelines_owned")
    viewers = models.ManyToManyField(Who, blank=True, related_name="timelines_viewable")
    editors = models.ManyToManyField(Who, blank=True, related_name="timelines_editable")

    objects = WhoableManager()

    def set_categories(self, categories):
        self.categories = json.dumps(categories)

    def get_categories(self):
        implicit = self.slides.values_list("group", flat=True).distinct()
        categories = json.loads(self.categories or "{}")

        for i, category_name in enumerate(implicit):
            if category_name and category_name not in categories:
                categories[category_name] = Timeline.CATEGORY_COLORS[
                    i % len(Timeline.CATEGORY_COLORS)
                ]
        if "__default__" not in categories:
            categories["__default__"] = "#f2f2f2"

        ordered = OrderedDict(sorted(categories.items()))
        return ordered

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-created"]
