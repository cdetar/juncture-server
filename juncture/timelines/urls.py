from django.conf.urls import url
from timelines import views

urlpatterns = [
    url(r"^t/(?P<secret_id>\w+)", views.timeline_view, name="timeline-view"),
    url(
        r"^export/(?P<secret_id>\w+)\.(?P<fmt>\w{3,4})",
        views.timeline_export,
        name="timeline-export",
    ),
    url(r"^timelines/upload$", views.timeline_upload, name="timeline-upload"),
    url(r"", views.index),
]
