from rest_framework import serializers

from accounts.models import User
from timelines.models import Who, TimelineGroup, Slide, Era, Timeline
from juncture.utils import date_to_timelinejs


class DateHash(serializers.Field):
    def get_attribute(self, obj):
        return obj

    def to_representation(self, obj):
        date = getattr(obj, self.source)
        precision = obj.precision
        return date_to_timelinejs(date, precision)


class TextHash(serializers.Field):
    def get_attribute(self, obj):
        return obj

    def to_representation(self, obj):
        text = {}
        if obj.headline:
            text["headline"] = obj.headline
        if getattr(obj, "text", None):
            text["text"] = obj.text
        return text or None


class MediaHash(serializers.Field):
    def get_attribute(self, obj):
        return obj

    def to_representation(self, obj):
        media = {}
        if obj.media:
            media["url"] = obj.media
        if obj.media_credit:
            media["credit"] = obj.media_credit
        if obj.media_thumbnail:
            media["thumbnail"] = obj.media_thumbnail
        if obj.media_caption:
            media["caption"] = obj.media_caption
        return media or None


class BackgroundHash(serializers.Field):
    def get_attribute(self, obj):
        return obj

    def to_representation(self, obj):
        background = {}
        if obj.background_url:
            background["url"] = obj.background_url
        elif obj.background_color:
            background["color"] = obj.background_color
        return background or None


class CountRelated(serializers.Field):
    def to_representation(self, obj):
        return obj.count()


class SlideSerializer(serializers.ModelSerializer):
    start_date = DateHash(read_only=True)
    end_date = DateHash(read_only=True)
    text = TextHash(read_only=True)
    media = MediaHash(read_only=True)
    background = BackgroundHash(read_only=True)

    class Meta:
        model = Slide
        fields = (
            "id",
            "start_date",
            "end_date",
            "text",
            "media",
            "is_title",
            "group",
            "background",
            "created",
        )


class EraSerializer(serializers.ModelSerializer):
    start_date = DateHash(read_only=True)
    end_date = DateHash(read_only=True)
    text = TextHash(read_only=True)

    class Meta:
        model = Era
        fields = ("id", "start_date", "end_date", "text")


class CategoryField(serializers.DictField):
    child = serializers.CharField()


class CategorySerializer(serializers.ModelSerializer):
    categories = CategoryField(source="get_categories", read_only=True)

    class Meta:
        model = Timeline
        fields = ("categories",)


class TimelineSerializer(serializers.ModelSerializer):
    slides = serializers.PrimaryKeyRelatedField(queryset=Slide.objects.all(), many=True)
    eras = serializers.PrimaryKeyRelatedField(queryset=Era.objects.all(), many=True)
    categories = CategoryField(source="get_categories", read_only=True)

    class Meta:
        model = Timeline
        fields = ("id", "name", "slides", "eras", "categories", "scale", "secret_id")


class TimelineListSerializer(serializers.ModelSerializer):
    slides_count = CountRelated(source="slides", read_only=True)
    eras_count = CountRelated(source="eras", read_only=True)

    class Meta:
        model = Timeline
        fields = ("id", "name", "slides_count", "eras_count", "scale", "secret_id")


#
# Groups
#


class TimelineGroupUserSerializer(serializers.ModelSerializer):
    display_name = serializers.CharField(source="get_display_name", read_only=True)
    profile_image = serializers.CharField(source="get_profile_image", read_only=True)

    class Meta:
        model = User
        fields = ("id", "display_name", "profile_image")


class TimelineGroupWhoSerializer(serializers.ModelSerializer):
    user = TimelineGroupUserSerializer(read_only=True)

    class Meta:
        model = Who
        fields = ("id", "user", "invitee", "expiration")


class TimelineGroupSerializer(serializers.ModelSerializer):
    members = TimelineGroupWhoSerializer(many=True, read_only=True)
    owners = TimelineGroupWhoSerializer(many=True, read_only=True)

    class Meta:
        model = TimelineGroup
        fields = ("id", "name", "members", "owners")


#
# Permissions
#


class PermissionsUserSerializer(TimelineGroupUserSerializer):
    pass


class PermissionsGroupSerializer(TimelineGroupSerializer):
    pass


class PermissionsWhoSerializer(serializers.ModelSerializer):
    user = PermissionsUserSerializer(read_only=True)
    group = PermissionsGroupSerializer(read_only=True)

    class Meta:
        model = Who
        fields = ("id", "user", "invitee", "group", "public", "expiration")


class TimelinePermissionsSerializer(serializers.ModelSerializer):
    owners = PermissionsWhoSerializer(
        many=True, read_only=True, source="current_owners"
    )
    viewers = PermissionsWhoSerializer(
        many=True, read_only=True, source="current_viewers"
    )
    editors = PermissionsWhoSerializer(
        many=True, read_only=True, source="current_editors"
    )

    class Meta:
        model = Timeline
        fields = ("owners", "viewers", "editors")


#
# Search whos
#
class SearchWhoUserSerializer(serializers.ModelSerializer):
    display_name = serializers.CharField(source="get_display_name", read_only=True)
    profile_image = serializers.CharField(source="get_profile_image", read_only=True)

    class Meta:
        model = User
        fields = ("id", "display_name", "username", "email", "profile_image")


class SearchWhoSerializer(serializers.ModelSerializer):
    user = SearchWhoUserSerializer(read_only=True)
    group = PermissionsGroupSerializer(read_only=True)

    class Meta:
        model = Who
        fields = ("id", "user", "invitee", "group", "public")
