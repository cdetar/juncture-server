from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.utils import timezone

from timelines.models import Who, TimelineGroup, Timeline, Slide, Era


def who_qs_to_list(qs):
    parts = []
    for who in qs:
        parts.append(
            mark_safe(
                "<a href='{}'>{}</a>".format(
                    escape(reverse("admin:timelines_who_change", args=[who.id])),
                    escape(who.name()),
                )
            )
        )
    return mark_safe(", ".join(parts))


def who_inline(Model, accessor):
    class WhoInline(admin.TabularInline):
        model = getattr(Model, accessor).through
        raw_id_fields = ["who"]
        verbose_name_plural = accessor
        verbose_name = accessor
        extra = 0

    return WhoInline


class SlideInline(admin.TabularInline):
    model = Timeline.slides.through
    raw_id_fields = ["slide"]
    verbose_name = "slide"
    verbose_name_plural = "slides"
    extra = 0


class EraInline(admin.TabularInline):
    model = Timeline.eras.through
    raw_id_fields = ["era"]
    verbose_name = "era"
    verbose_name_plural = "eras"
    extra = 0


@admin.register(Timeline)
class TimelineAdmin(admin.ModelAdmin):
    list_display = ["name", "slides_count", "created", "owners_list"]
    filter_horizontal = ["slides", "eras"]
    date_hierarchy = "created"
    search_fields = ["name", "secret_id"]

    inlines = [
        who_inline(Timeline, "owners"),
        who_inline(Timeline, "editors"),
        who_inline(Timeline, "viewers"),
        SlideInline,
        EraInline,
    ]

    exclude = ["owners", "editors", "viewers", "slides", "eras"]

    def slides_count(self, obj):
        return obj.slides.all().count()

    slides_count.short_description = "slides"

    def owners_list(self, obj):
        return who_qs_to_list(obj.owners.all())

    owners_list.short_description = "owners"


@admin.register(Era)
class EraAdmin(admin.ModelAdmin):
    list_display = ["start_date", "end_date", "headline"]
    date_hierarchy = "start_date"
    search_fields = ["headline"]
    list_filter = ["timeline"]

    inlines = [
        who_inline(Era, "owners"),
        who_inline(Era, "editors"),
        who_inline(Era, "viewers"),
    ]
    exclude = ["owners", "editors", "viewers"]


@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
    list_display = ["start_date", "end_date", "headline", "text", "media"]
    date_hierarchy = "start_date"
    list_filter = ["is_title", "timeline", "group"]
    search_fields = ["text", "media"]

    inlines = [
        who_inline(Slide, "owners"),
        who_inline(Slide, "editors"),
        who_inline(Slide, "viewers"),
    ]
    exclude = ["owners", "editors", "viewers"]


@admin.register(Who)
class WhoAdmin(admin.ModelAdmin):
    list_display = ["name", "user", "invitee", "group", "public", "expiration_display"]
    search_fields = [
        "user__display_name",
        "user__username",
        "user__email",
        "group__name",
        "invitee",
    ]
    list_filter = ["public"]
    date_hierarchy = "expiration"

    def expiration_display(self, obj):
        if obj.expiration == Who.NO_EXPIRATION:
            return "never expires"
        elif obj.expiration < timezone.now():
            return "expired"
        return obj.expiration

    expiration_display.short_description = "expiration"


@admin.register(TimelineGroup)
class TimelineGroupAdmin(admin.ModelAdmin):
    list_display = ["name", "owners_list"]
    filter_horizontal = ["members", "owners"]

    def owners_list(self, obj):
        return who_qs_to_list(obj.owners.all())

    owners_list.short_description = "owners"
