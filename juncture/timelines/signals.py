from django.db.models.signals import post_save
from django.db.models import Q
from django.dispatch import receiver

from accounts.models import User
from timelines.models import Who


@receiver(post_save, sender=User)
def associate_who_invitees(sender, **kwargs):
    user = kwargs.get("instance")
    if user and kwargs.get("created"):
        for who in Who.objects.filter(
            Q(invitee__iexact=user.username) | Q(invitee__iexact=user.email),
            user__isnull=True,
        ):
            who.user = user
            who.invitee = ""
            who.save()
