import datetime
import functools
import json
import logging
from channels.generic.websocket import WebsocketConsumer
from channels_presence.models import Room, Presence
from channels_presence.decorators import touch_presence, remove_presence
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import Prefetch, Q

from juncture.channels_utils import (
    broadcast,
    send_channel,
    handle_error,
    require_payload_keys,
)
from juncture.utils import date_from_timelinejs, server_now
from timelines.models import Timeline, Who, Slide, Era, TimelineGroup
from timelines import utils
from timelines import serializers
from accounts.models import User


logger = logging.getLogger("django.channels.server")


class TimelinesConsumer(WebsocketConsumer):
    def connect(self):
        super().connect()
        self.user = self.scope["user"]

    @remove_presence
    def disconnect(self, close_code):
        pass

    @touch_presence
    def receive(self, text_data=None, bytes_data=None):
        logger.debug("IN %s", text_data)
        handle_receive(self, text_data)

    def forward_message(self, event):
        """
        Magic handler that responds to channel layer messages of type
        `forward.message`.  Simply forward the message on to the consumer.
        """
        self.send(event["message"])


def handle_receive(consumer, text_data):
    try:
        data = json.loads(text_data)
    except ValueError:
        handle_error(consumer, "Message body must be json")
        raise

    if "type" not in data:
        return handle_error(consumer, "Missing type")
    elif data["type"] == "FETCH_TIMELINE_LIST":
        return handle_list_timelines(consumer, data)
    elif data["type"] == "CREATE_TIMELINE":
        return handle_create_timeline(consumer, data)
    elif data["type"] == "IMPORT_TIMELINE":
        return handle_import_timeline(consumer, data)
    elif data["type"] == "FETCH_GROUPS":
        return handle_fetch_groups(consumer, data)
    elif data["type"] == "CHANGE_GROUP":
        return handle_change_group(consumer, data)
    elif data["type"] == "ADD_GROUP":
        return handle_add_group(consumer, data)
    elif data["type"] == "REMOVE_GROUP":
        return handle_remove_group(consumer, data)
    elif data["type"] == "LEAVE_GROUP":
        return handle_leave_group(consumer, data)
    elif data["type"] == "SEARCH_WHOS":
        return handle_search_whos(consumer, data)

    #
    # All following types require a `timeline_id` parameter
    #

    timeline_id = data.get("payload", {}).get("timeline_id")
    if not timeline_id:
        return handle_error(consumer, "Missing timeline_id", data["type"])
    try:
        timeline = Timeline.objects.get(
            secret_id=data.get("payload", {}).get("timeline_id")
        )
    except Timeline.DoesNotExist:
        return handle_error(consumer, "Timeline not found", data["type"])

    if data["type"] == "JOIN":
        return handle_join(consumer, data, timeline)
    elif data["type"] == "LEAVE":
        return handle_join(consumer, data, timeline)
    elif data["type"] == "FETCH_TIMELINE":
        return handle_fetch_timeline(consumer, data, timeline)
    elif data["type"] == "CHANGE_TIMELINE":
        return handle_change_timeline(consumer, data, timeline)
    elif data["type"] == "CHANGE_CATEGORIES":
        return handle_change_categories(consumer, data, timeline)
    elif data["type"] == "CHANGE_PERMISSIONS":
        return handle_change_permissions(consumer, data, timeline)
    elif data["type"] == "ADD_SLIDE":
        return handle_add_slide(consumer, data, timeline)
    elif data["type"] == "CHANGE_SLIDE":
        return handle_change_slide(consumer, data, timeline)
    elif data["type"] == "DELETE_SLIDE":
        return handle_delete_slide(consumer, data, timeline)
    elif data["type"] == "ADD_ERA":
        return handle_add_era(consumer, data, timeline)
    elif data["type"] == "CHANGE_ERA":
        return handle_change_era(consumer, data, timeline)
    elif data["type"] == "DELETE_ERA":
        return handle_delete_era(consumer, data, timeline)
    elif data["type"] == "DELETE_TIMELINE":
        return handle_delete_timeline(consumer, data, timeline)
    else:
        return handle_error(consumer, "Type not understood", data["type"])


def require_socket_login(fn):
    @functools.wraps(fn)
    def inner(consumer, data, *args, **kwargs):
        if not consumer.user.is_authenticated:
            return handle_error(consumer, "Login required", data["type"])
        return fn(consumer, data, *args, **kwargs)

    return inner


def handle_list_timelines(consumer, data):
    timelines = []
    if consumer.user.is_authenticated:
        qs = Timeline.objects.owned_by(consumer.user, implicit=False).order_by(
            "-created"
        )
        timelines = serializers.TimelineListSerializer(qs, many=True).data
    else:
        timelines = []
    send_channel(consumer, type="FETCH_TIMELINE_LIST", payload=timelines)
    send_channel(
        consumer,
        type="LOADING",
        payload={"type": "FETCH_TIMELINE_LIST", "loading": False},
    )


@require_payload_keys(["name"])
def handle_create_timeline(consumer, data):
    try:
        with transaction.atomic():
            who = Who.objects.canonical(user=consumer.user)
            timeline = Timeline(name=data["payload"]["name"])
            timeline.full_clean()
            timeline.save()
            timeline.owners.set([who])
    except ValidationError as e:
        return handle_error(
            consumer,
            "Error: {}".format("Error: {}".format(e.message_dict)),
            data["type"],
        )
    send_channel(
        consumer,
        type="REDIRECT",
        payload="/timelines/{}/edit".format(timeline.secret_id),
    )
    send_channel(
        consumer, type="LOADING", payload={"type": "CREATE_TIMELINE", "loading": False}
    )


@require_payload_keys(["spreadsheet"])
def handle_import_timeline(consumer, data):
    if not consumer.user.is_authenticated:
        return handle_error(
            consumer, "Must be signed in to import spreadsheets", data["type"]
        )
    spreadsheet = data["payload"]["spreadsheet"]
    ignore_errors = data["payload"].get("ignore_errors") is True
    title = spreadsheet.get("title")
    cells = spreadsheet.get("cells")
    if not isinstance(cells, list):
        return handle_error(
            consumer,
            "Malformed data. Expected list, got %s" % type(cells),
            data["type"],
        )

    try:
        (slides, eras) = utils.import_timeline_cells(cells, ignore_errors=ignore_errors)
    except utils.SpreadsheetFormatException as exp:
        return handle_error(consumer, exp.args[0], data["type"])

    try:
        with transaction.atomic():
            owner = Who.objects.canonical(user=consumer.user)
            timeline = Timeline(name=title)
            timeline.full_clean()
            timeline.save()
            timeline.owners.set([owner])
            for slide in slides:
                slide.save()
            timeline.slides.set(slides)
            for era in eras:
                era.save()
            timeline.eras.set(eras)
    except ValidationError as e:
        return handle_error(consumer, "Error: {}".format(e.message_dict), data["type"])

    send_channel(
        consumer,
        type="REDIRECT",
        payload="/timelines/{}/edit".format(timeline.secret_id),
    )
    send_channel(
        consumer, type="LOADING", payload={"type": "IMPORT_TIMELINE", "loading": False}
    )


@require_payload_keys(["timeline_id"])
def handle_delete_timeline(consumer, data, timeline):
    if not timeline.owned_by(consumer.user):
        return handle_error(
            consumer, "Must be a timeline owner to delete.", data["type"]
        )

    timeline.delete()
    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )
    send_channel(consumer, type="REDIRECT", payload="/")


#
# Groups
#


def handle_fetch_groups(consumer, data):
    if not consumer.user.is_authenticated:
        return

    owners_prefetch = Prefetch("owners", queryset=Who.objects.select_related("user"))
    members_prefetch = Prefetch("members", queryset=Who.objects.select_related("user"))
    groups = (
        TimelineGroup.objects.with_member(consumer.user)
        .distinct()
        .prefetch_related(members_prefetch, owners_prefetch)
    )

    send_channel(
        consumer, type="LOADING", payload={"type": "FETCH_GROUPS", "loading": False}
    )
    send_channel(
        consumer,
        type="SET_GROUPS",
        payload={
            "groups": {
                g["id"]: g
                for g in serializers.TimelineGroupSerializer(groups, many=True).data
            }
        },
    )


def ensure_group_owner(fn):
    """
    Decorator to fetch a single group and ensure that the requesting user is an
    owner of it.
    """

    @functools.wraps(fn)
    def inner(consumer, data):
        payload = data["payload"]
        try:
            group = TimelineGroup.objects.get(pk=payload["group"].get("id"))
        except TimelineGroup.DoesNotExist:
            return handle_error(consumer, "Not found", data["type"])
        if not group.owned_by(consumer.user):
            return handle_error(consumer, "Must be group owner", data["type"])
        return fn(consumer, data, group)

    return inner


def _set_groups_socket_response(consumer, data, group):
    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )
    send_channel(
        consumer,
        type="SET_GROUPS",
        payload={"groups": {group.id: serializers.TimelineGroupSerializer(group).data}},
    )


def _fetch_group_who(invitee):
    if invitee.get("id"):
        who = Who.objects.get(pk=invitee["id"])
    else:
        try:
            who_kwargs = {
                "user": User.objects.get(
                    Q(username__iexact=invitee["invitee"])
                    | Q(email__iexact=invitee["invitee"])
                )
            }
        except User.DoesNotExist:
            who_kwargs = {"invitee": invitee["invitee"]}
        try:
            expiration_days = int(invitee["expiration"])
        except ValueError:
            expiration_days = None
        if expiration_days == -1:
            who = Who.objects.canonical(**who_kwargs)
        else:
            if expiration_days is not None:
                who_kwargs["expiration"] = server_now() + datetime.timedelta(
                    days=expiration_days
                )
            else:
                who_kwargs["expiration"] = invitee["expiration"]
            who, created = Who.objects.get_or_create(**who_kwargs)
    return who


@require_payload_keys(["group"])
@require_socket_login
def handle_add_group(consumer, data):
    try:
        with transaction.atomic():
            pl = data["payload"]
            group = TimelineGroup.objects.create(name=pl["group"]["name"])
            group.owners.add(Who.objects.canonical(user=consumer.user))
            return _set_groups_socket_response(consumer, data, group)

    except User.DoesNotExist:
        return handle_error(consumer, "Referenced user not found: %s" % data["type"])


@require_payload_keys(["group"])
@require_socket_login
def handle_leave_group(consumer, data):
    try:
        group = TimelineGroup.objects.get(id=data["payload"]["group"].get("id"))
    except TimelineGroup.DoesNotExist:
        return handle_error(consumer, "Not found", data["type"])

    with transaction.atomic():
        group.owners.set(group.owners.exclude(user=consumer.user))
        group.members.set(group.members.exclude(user=consumer.user))

    return _set_groups_socket_response(consumer, data, group)


@require_payload_keys(["group"])
@require_socket_login
@ensure_group_owner
def handle_change_group(consumer, data, group):
    with transaction.atomic():
        pl = data["payload"]
        if pl["group"].get("name"):
            group.name = pl["group"]["name"]
            group.save()
        group.owners.set(
            [_fetch_group_who(inv) for inv in pl["group"].get("owners", [])]
        )
        group.members.set(
            [_fetch_group_who(inv) for inv in pl["group"].get("members", [])]
        )
    return _set_groups_socket_response(consumer, data, group)


@require_payload_keys(["group"])
@require_socket_login
@ensure_group_owner
def handle_remove_group(consumer, data, group):
    group.delete()
    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )
    send_channel(
        consumer,
        type="SET_GROUPS",
        payload={"groups": {data["payload"]["group"]["id"]: None}},
    )


@require_payload_keys(["q"])
def handle_search_whos(consumer, data):
    def respond(results):
        send_channel(
            consumer, type="LOADING", payload={"type": data["type"], "loading": False}
        )
        send_channel(consumer, type="SET_SEARCH_WHOS", payload={"whos": results})

    if not consumer.user.is_authenticated:
        return respond({})

    if len(data["payload"]["q"]) < 2:
        return respond({})

    whos = (
        Who.objects.search(consumer.user, data["payload"]["q"])
        .order_by("user_id", "group_id", "invitee")
        .distinct("user_id", "group_id", "invitee")
    )

    results = {
        w["id"]: w for w in serializers.SearchWhoSerializer(whos, many=True).data
    }
    return respond(results)


#
# Permissions
#


def handle_change_permissions(consumer, data, timeline):
    if not timeline.owned_by(consumer.user):
        return handle_error(consumer, "Not allowed", data["type"])

    payload = data["payload"]

    def get_who(kwargs):
        if kwargs.get("group") and kwargs["group"].get("id"):
            return Who.objects.get_or_create(
                group=TimelineGroup.objects.get(id=kwargs["group"]["id"]),
                expiration=kwargs["expiration"],
            )[0]
        elif kwargs.get("user") and kwargs["user"].get("id"):
            return Who.objects.get_or_create(
                user=User.objects.get(id=kwargs["user"]["id"]),
                expiration=kwargs["expiration"],
            )[0]
        elif kwargs.get("invitee"):
            return Who.objects.get_or_create(
                invitee=kwargs["invitee"], expiration=kwargs["expiration"]
            )[0]
        elif kwargs.get("public"):
            return Who.objects.get_or_create(
                public=True, expiration=kwargs["expiration"]
            )[0]

    with transaction.atomic():
        for role in ("editors", "owners", "viewers"):
            who_kwargs = payload.get(role)
            if who_kwargs is not None:
                # timeline.owners.set(...)
                # timeline.editors.set(...)
                # timeline.viewers.set(...)
                getattr(timeline, role).set([get_who(kwargs) for kwargs in who_kwargs])

    broadcast(
        timeline.secret_id,
        type="SET_PERMISSIONS",
        payload={
            "timelines": {
                timeline.secret_id: serializers.TimelinePermissionsSerializer(
                    timeline
                ).data
            }
        },
    )
    send_channel(
        consumer,
        type="LOADING",
        payload={"type": "CHANGE_PERMISSIONS", "loading": False},
    )


#
# Presence
#


def handle_join(consumer, data, timeline):
    if timeline.viewable_by(consumer.user):
        Room.objects.add(timeline.secret_id, consumer.channel_name, consumer.user)
    else:
        handle_error(consumer, "Not allowed", data["type"])


def handle_leave(consumer, data, timeline):
    Room.objects.remove(timeline.secret_id, consumer.channel_name)


#
# Timeline
#


def handle_fetch_timeline(consumer, data, timeline):
    if timeline.viewable_by(consumer.user):
        viewable_slides = timeline.slides.viewable_by(consumer.user)
        viewable_eras = timeline.eras.viewable_by(consumer.user)

        send_channel(
            consumer,
            type="SET_SLIDES",
            payload=[serializers.SlideSerializer(s).data for s in viewable_slides],
        )
        send_channel(
            consumer,
            type="SET_ERAS",
            payload=[serializers.EraSerializer(e).data for e in viewable_eras],
        )
        send_channel(
            consumer,
            type="SET_PERMISSIONS",
            payload={
                "timelines": {
                    timeline.secret_id: serializers.TimelinePermissionsSerializer(
                        timeline
                    ).data
                }
            },
        )
        send_channel(
            consumer,
            type="SET_TIMELINE",
            payload=serializers.TimelineSerializer(timeline).data,
        )
        send_channel(
            consumer,
            type="LOADING",
            payload={"type": "FETCH_TIMELINE", "loading": False},
        )
    else:
        handle_error(
            consumer,
            "This timeline is only viewable by some logged in users."
            " To view it, ask the timeline owner to grant you permission.",
            data["type"],
        )


def handle_change_timeline(consumer, data, timeline):
    for key in ("name",):
        if key in data["payload"]:
            setattr(timeline, key, data["payload"][key])
    timeline.save()
    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )
    broadcast(
        timeline.secret_id,
        type="SET_TIMELINE",
        payload=serializers.TimelineSerializer(timeline).data,
    )


@require_payload_keys(["category_update"])
def handle_change_categories(consumer, data, timeline):
    if not timeline.editable_by(consumer.user):
        return handle_error(consumer, "Permission denied", data["type"])

    # Include __default__ category by default
    category_update = data["payload"]["category_update"]
    categories = {}
    rename_categories = {}
    for update in category_update:
        if update["op"] == "change" and update["orig"] != update["name"]:
            rename_categories[update["orig"]] = update["name"]
        categories[update["name"]] = update["color"]

    if "__default__" not in categories:
        categories["__default__"] = "#f2f2f2"

    with transaction.atomic():
        changed_slides = []

        # Rename any renamed categories
        for old, new in rename_categories.items():
            qs = Slide.objects.filter(timeline=timeline, group=old)
            changed_slides += qs.values_list("id", flat=True)
            qs.update(group=new)

        # Remove category from any slides with no-longer-available category
        names = set(categories.keys())
        names.remove("__default__")
        names.add("")
        qs = Slide.objects.filter(timeline=timeline).exclude(group__in=names)
        changed_slides += qs.values_list("id", flat=True)
        qs.update(group="")

        timeline.set_categories(categories)
        timeline.save()

        # Stop loading
        send_channel(
            consumer, type="LOADING", payload={"type": data["type"], "loading": False}
        )

        # broadcast new timeline
        broadcast(
            timeline.secret_id,
            type="SET_TIMELINE",
            payload=serializers.TimelineSerializer(timeline).data,
        )

        # broadcast new slides
        slides_to_broadcast = list(Slide.objects.filter(id__in=changed_slides))
        if slides_to_broadcast:
            _broadcast_slides(timeline, *slides_to_broadcast)


def _broadcast_slides(timeline, *slides):
    presences = list(
        Presence.objects.select_related("user").filter(
            room__channel_name=timeline.secret_id
        )
    )
    payloads = {}
    for slide in slides:
        payloads[slide.id] = {
            "slide": slide,
            "payload": serializers.SlideSerializer(slide).data,
        }
    for presence in presences:
        user_slides = []
        for slide_id, payload in payloads.items():
            # if payload['slide'].viewable_by(presence.user): # FIXME
            user_slides.append(payload["payload"])
        send_channel(presence.channel_name, type="SET_SLIDES", payload=user_slides)


def _update_slide(consumer, data, timeline):
    if not timeline.editable_by(consumer.user):
        return handle_error(consumer, "Permission denied", data["type"])

    slide_data = data["payload"]["slide"]
    is_new = not slide_data.get("id")
    try:
        with transaction.atomic():
            if is_new:
                slide = Slide()
            else:
                try:
                    slide = Slide.objects.get(id=slide_data["id"])
                except Slide.DoesNotExist:
                    return handle_error(
                        consumer,
                        "Error: Slide {} not found".format(slide_data.get("id")),
                        data["type"],
                    )
                if not slide.editable_by(consumer.user):
                    return handle_error(consumer, "Permission denied", data["type"])

            start_date, start_precision = date_from_timelinejs(
                slide_data.get("start_date", {})
            )
            end_date, end_precision = date_from_timelinejs(
                slide_data.get("end_date", {})
            )

            media = slide_data.get("media") or {}
            text = slide_data.get("text") or {}
            background = slide_data.get("background") or {}

            slide.start_date = start_date
            slide.end_date = end_date
            slide.is_title = slide_data.get("is_title", False)
            slide.precision = start_precision
            slide.headline = text.get("headline", "")
            slide.text = text.get("text", "")
            slide.media = media.get("url", "")
            slide.media_caption = media.get("caption", "")
            slide.media_credit = media.get("credit", "")
            slide.media_thumbnail = media.get("thumbnail", "")
            slide.group = slide_data.get("group", "")
            slide.background_color = background.get("color", "")
            slide.background_url = background.get("url", "")

            slide.save()
            if is_new:
                timeline.slides.add(slide)
    except ValidationError as e:
        return handle_error(consumer, "Error: {}".format(e.message_dict), data["type"])

    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )

    _broadcast_slides(timeline, slide)


@require_payload_keys(["slide"])
def handle_add_slide(consumer, data, timeline):
    _update_slide(consumer, data, timeline)
    broadcast(
        timeline.secret_id,
        type="SET_TIMELINE",
        payload=serializers.TimelineSerializer(timeline).data,
    )


@require_payload_keys(["slide"])
def handle_change_slide(consumer, data, timeline):
    _update_slide(consumer, data, timeline)


@require_payload_keys(["slide_id"])
def handle_delete_slide(consumer, data, timeline):
    try:
        slide = Slide.objects.get(id=data["payload"]["slide_id"])
    except Slide.DoesNotExist:
        return handle_error(consumer, "Story not found", data["type"])

    if not slide.owned_by(consumer.user):
        return handle_error(consumer, "Must be story owner to delete.", data["type"])

    slide.delete()

    broadcast(
        timeline.secret_id,
        type="SET_TIMELINE",
        payload=serializers.TimelineSerializer(timeline).data,
    )


def _update_era(consumer, data, timeline):
    if not timeline.editable_by(consumer.user):
        return handle_error(consumer, "Permission denied", data["type"])

    era_data = data["payload"]["era"]
    is_new = not era_data.get("id")
    try:
        with transaction.atomic():
            if is_new:
                era = Era()
            else:
                try:
                    era = Era.objects.get(id=era_data["id"])
                except Era.DoesNotExist:
                    return handle_error(
                        consumer,
                        "Error: Era {} not found".format(era_data.get("id")),
                        data["type"],
                    )
                if not era.editable_by(consumer.user):
                    return handle_error(consumer, "Permission denied", data["type"])

            start_date, start_precision = date_from_timelinejs(
                era_data.get("start_date", {})
            )
            end_date, end_precision = date_from_timelinejs(era_data.get("end_date", {}))
            era.start_date = start_date
            era.end_date = end_date
            era.precision = start_precision
            era.headline = era_data.get("text", {}).get("headline")
            era.save()
            if is_new:
                timeline.eras.add(era)
                if consumer.user.is_authenticated:
                    era.owners.add(Who.objects.canonical(user=consumer.user))
    except ValidationError as e:
        return handle_error(consumer, "Error: {}".format(e.message_dict), data["type"])

    send_channel(
        consumer, type="LOADING", payload={"type": data["type"], "loading": False}
    )

    era_payload = [serializers.EraSerializer(era).data]
    for presence in Presence.objects.select_related("user").filter(
        room__channel_name=timeline.secret_id
    ):
        # if era.viewable_by(presence.user): # FIXME
        send_channel(presence.channel_name, type="SET_ERAS", payload=era_payload)


@require_payload_keys(["era"])
def handle_add_era(consumer, data, timeline):
    _update_era(consumer, data, timeline)
    broadcast(
        timeline.secret_id,
        type="SET_TIMELINE",
        payload=serializers.TimelineSerializer(timeline).data,
    )


@require_payload_keys(["era"])
def handle_change_era(consumer, data, timeline):
    _update_era(consumer, data, timeline)


@require_payload_keys(["era_id"])
def handle_delete_era(consumer, data, timeline):
    try:
        era = Era.objects.get(id=data["payload"]["era_id"])
    except Era.DoesNotExist:
        return handle_error(consumer, "Era not found", data["type"])

    if not era.owned_by(consumer.user):
        return handle_error(consumer, "Must be era owner to delete.", data["type"])

    era.delete()

    broadcast(
        timeline.secret_id,
        type="SET_TIMELINE",
        payload=serializers.TimelineSerializer(timeline).data,
    )
