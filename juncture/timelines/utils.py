import re
import datetime
import dateparser
from juncture.utils import settings_time_zone

from timelines.models import Slide, Era

precisions = ["year", "month", "day", "hour", "minute", "second", "millisecond"]


def parse_date_and_precision(obj):
    """
    Given a datestring in the format of "YYYY-MM-DD HH:MM:SS.ssssss" (or with
    less precision, e.g. "YYYY-MM" or "YYYY"), return a datetime object that
    represents the date and a string indicating the best precision.

    >>> parse_date_and_precision("2017-02-03")
    (datetime.datetime(2017, 2, 3, 0, 0, 0), "day")
    >>> parse_date_and_precision("2017")
    (datetime.datetime(2017, 1, 1, 0, 0, 0), "year")

    """
    if not obj:
        return None, None
    if isinstance(obj, (list, tuple)):
        clean = []
        for part in obj:
            if part:
                clean.append(int(part))
            else:
                break
        if not clean:
            return None, None
        obj = dict(zip(precisions, clean))

    if isinstance(obj, str):
        string = obj.strip()
        if not string:
            return None, None
        elif re.match(r"^\d+[/-]\d+[/-]\d+[\sT]+\d+:\d+:\d+.\d+", string):
            precision = "millisecond"
        elif re.match(r"^\d+[/-]\d+[/-]\d+[\sT]+\d+:\d+:\d+", string):
            precision = "second"
        elif re.match(r"^\d+[/-]\d+[/-]\d+[\sT]+\d+:\d+$", string):
            precision = "minute"
        elif re.match(r"^\d+[/-]\d+[/-]\d+[\sT]+\d+$", string):
            precision = "hour"
        elif re.match(r"^\d+[/-]\d+[/-]\d+", string):
            precision = "day"
        elif re.match(r"^\d+[/-]\d+$", string):
            precision = "month"
        elif re.match(r"^\d+$", string):
            precision = "year"
        else:
            # fallback
            precision = "day"
        date = dateparser.parse(string)
        settings_time_zone.localize(date)
        return (date, precision)

    if isinstance(obj, datetime.datetime):
        if obj.tzinfo is None or obj.tzinfo.utcoffset(obj) is None:
            settings_time_zone.localize(obj)
        return (obj, "second")

    if isinstance(obj, datetime.date):
        obj = datetime.datetime(
            obj.year, obj.month, obj.day, 12, 0, 0, tzinfo=settings_time_zone
        )
        return (obj, "day")

    if isinstance(obj, dict):
        precision = "year"
        for val in precisions:
            if obj.get(val, None) is None:
                break
            else:
                precision = val

        date = datetime.datetime(
            obj["year"],
            obj.get("month", 1),
            obj.get("day", 1),
            obj.get("hour", 12),
            obj.get("minute", 0),
            obj.get("second", 0),
            obj.get("millisecond", 0) * 1000,
            tzinfo=settings_time_zone,
        )
        return (date, precision)

    raise ValueError("Unrecognized type %s" % type(obj))


def date_precision_to_list(date, precision):
    """
    Given a datetime, and a string represencation of its precision, return a
    list of values in the format TimelineJS3 uses in spreadsheets.
    """
    if date is None:
        return ["", "", "", ""]

    parts = [date.year]
    if precision == "year":
        return parts + ["", "", ""]

    parts.append(date.month)
    if precision == "month":
        return parts + ["", ""]

    parts.append(date.day)
    if precision == "day":
        return parts + [""]

    if precision == "hour":
        time_fmt = "%H:00:00"
    elif precision == "minute":
        time_fmt = "%H:%M:00"
    else:
        time_fmt = "%H:%M:%S"

    time = date.strftime(time_fmt)
    if precision == "millisecond":
        time = "%s.%s" % (time, int(date.microsecond * 1000))

    parts.append(time)
    return time


class SpreadsheetFormatException(Exception):
    pass


def import_timeline_cells(cells, ignore_errors=False):
    """
    Given a 2d array of cells coming from a timelinejs2 or timelinejs3
    spreadsheet (with header), create lists of unsaved but clean slide and era
    models. Returns a tuple of (slides, eras).
    """
    header = cells[0]

    slides = []
    eras = []

    def call_or_row_error(func, row_index):
        try:
            return func(), None
        except Exception as e:
            if ignore_errors:
                return None, e
            else:
                raise SpreadsheetFormatException(
                    "Error, row {}: {}".format(
                        str(row_index + 2), getattr(e, "message_dict", str(e))
                    )
                )

    if len(header) == 9:
        # Old style TimelineJS2 spreadsheet
        columns = (
            "startdate",
            "enddate",
            "headline",
            "text",
            "media",
            "mediacredit",
            "mediathumbnail",
            "type",
            "tag",
        )
        for row_index, row in enumerate(cells[1:]):
            values = dict(zip(columns, row))
            if values.get("enddate") and not values.get("startdate"):
                values["startdate"] = values["enddate"]
                values["enddate"] = None

            # Parse start date
            res, err = call_or_row_error(
                lambda: parse_date_and_precision(values.get("startdate")), row_index
            )
            if err:
                continue
            start_date, start_precision = res

            # Parse end date
            res, err = call_or_row_error(
                lambda: parse_date_and_precision(values.get("enddate")), row_index
            )
            if err:
                continue
            end_date, end_precision = res

            # All other fields
            slide = Slide(
                start_date=start_date,
                end_date=end_date,
                precision=start_precision or "day",
                headline=values.get("headline", ""),
                text=values.get("text", ""),
                media=values.get("media", ""),
                media_credit=values.get("mediacredit", ""),
                media_thumbnail=values.get("medathumbnail", ""),
                is_title=(values.get("type") == "title") or (start_date is None),
                group=values.get("tag", ""),
            )
            res, err = call_or_row_error(slide.full_clean, row_index)
            if err:
                continue
            slides.append(slide)

    elif len(header) == 18:
        # New stile TimelineJS3 spreadsheet
        columns = (
            "year",
            "month",
            "day",
            "time",
            "end_year",
            "end_month",
            "end_day",
            "end_time",
            "display_date",
            "headline",
            "text",
            "media",
            "media_credit",
            "media_caption",
            "media_thumbnail",
            "type",
            "group",
            "background",
        )

        for row_index, row in enumerate(cells[1:]):
            values = dict(zip(columns, row))

            # Parse start date into datetime
            time = [a for a in re.split(r"[^\d]+", values.get("time", "")) if a]
            res, err = call_or_row_error(
                lambda: parse_date_and_precision(
                    [
                        values.get("year", ""),
                        values.get("month", ""),
                        values.get("day", ""),
                    ]
                    + time
                ),
                row_index,
            )
            if err:
                continue
            start_date, start_precision = res

            # Parse end date into datetime
            end_time = [
                int(a) for a in re.split(r"[^\d]+", values.get("end_time", "")) if a
            ]
            res, err = call_or_row_error(
                lambda: parse_date_and_precision(
                    [
                        values.get("end_year", ""),
                        values.get("end_month", ""),
                        values.get("end_day", ""),
                    ]
                    + end_time
                ),
                row_index,
            )
            if err:
                continue
            end_date, end_precision = res

            if values.get("type", "").strip() == "era":
                era = Era(
                    start_date=start_date,
                    end_date=end_date,
                    headline=values.get("headline", ""),
                )
                res, err = call_or_row_error(era.full_clean, row_index)
                if err:
                    continue
                eras.append(era)
            else:
                if "http" in (values.get("background") or ""):
                    background_url = values["background"]
                    background_color = ""
                else:
                    background_url = ""
                    background_color = values.get("background") or ""

                slide = Slide(
                    start_date=start_date,
                    end_date=end_date,
                    precision=start_precision or "day",
                    headline=values.get("headline", ""),
                    text=values.get("text", ""),
                    media=values.get("media", ""),
                    media_caption=values.get("media_caption", ""),
                    media_thumbnail=values.get("media_thumbnail", ""),
                    is_title=(values.get("type") == "title") or (start_date is None),
                    group=values.get("group", ""),
                    background_url=background_url,
                    background_color=background_color,
                )
                res, err = call_or_row_error(slide.full_clean, row_index)
                if err:
                    continue
                slides.append(slide)
    # else:
    #    return handle_error(
    #        message, "Spreadsheet format not recognized.", data['type']
    #    )

    return (slides, eras)
