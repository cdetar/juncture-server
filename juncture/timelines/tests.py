import pytest
import datetime

from timelines.models import Timeline, TimelineGroup, Slide, Era, Who
from accounts.models import User
from juncture.utils import server_now


@pytest.fixture
@pytest.mark.django_db
def perms_timeline_set():
    # Timeline 1
    ret = {}
    for name in ("tl1", "tl2"):
        timeline = Timeline.objects.create(name=name)
        owner_user = User.objects.create(username="%s_owner_user" % name)
        timeline.owners.add(Who.objects.canonical(user=owner_user))
        owner_group = TimelineGroup.objects.create(name="%s_owner_group" % name)
        owner_group.members.set(
            [
                Who.objects.canonical(
                    user=User.objects.create(username="%s_owner_group_1" % name)
                ),
                Who.objects.canonical(
                    user=User.objects.create(username="%s_owner_group_2" % name)
                ),
            ]
        )
        timeline.owners.add(Who.objects.canonical(group=owner_group))

        editor_user = User.objects.create(username="%s_editor_user" % name)
        timeline.editors.add(Who.objects.canonical(user=editor_user))
        editor_group = TimelineGroup.objects.create(name="%s_editor_group" % name)
        editor_group.members.set(
            [
                Who.objects.canonical(
                    user=User.objects.create(username="%s_editor_group_1" % name)
                ),
                Who.objects.canonical(
                    user=User.objects.create(username="%s_editor_group_2" % name)
                ),
            ]
        )
        timeline.editors.add(Who.objects.canonical(group=editor_group))
        viewer_user = User.objects.create(username="%s_viewer_user" % name)
        timeline.viewers.add(Who.objects.canonical(user=viewer_user))
        viewer_group = TimelineGroup.objects.create(name="%s_viewer_group" % name)
        viewer_group.members.set(
            [
                Who.objects.canonical(
                    user=User.objects.create(username="%s_viewer_group_1" % name)
                ),
                Who.objects.canonical(
                    user=User.objects.create(username="%s_viewer_group_2" % name)
                ),
            ]
        )
        timeline.viewers.add(Who.objects.canonical(group=viewer_group))

        slide_inherit = Slide.objects.create(
            headline="Inherit", start_date=server_now()
        )
        era_inherit = Era.objects.create(
            headline="Inherit", start_date=server_now(), end_date=server_now()
        )

        slide_owned = Slide.objects.create(
            headline="%s Owned" % name, start_date=server_now()
        )
        slide_owned.owners.add(Who.objects.canonical(user=owner_user))
        slide_owned.owners.add(Who.objects.canonical(group=owner_group))
        era_owned = Era.objects.create(
            headline="%s Owned" % name, start_date=server_now(), end_date=server_now()
        )
        era_owned.owners.add(Who.objects.canonical(user=owner_user))
        era_owned.owners.add(Who.objects.canonical(group=owner_group))

        alt_owner = User.objects.create(username="alt_owner_%s" % name)
        alt_owner_who = Who.objects.canonical(user=alt_owner)

        slide_editor = Slide.objects.create(
            headline="%s Editor" % name, start_date=server_now()
        )
        slide_editor.owners.add(alt_owner_who)
        slide_editor.editors.add(Who.objects.canonical(user=editor_user))
        slide_editor.editors.add(Who.objects.canonical(group=editor_group))
        era_editor = Era.objects.create(
            headline="%s Editor" % name, start_date=server_now(), end_date=server_now()
        )
        era_editor.owners.add(alt_owner_who)
        era_editor.editors.add(Who.objects.canonical(user=editor_user))
        era_editor.editors.add(Who.objects.canonical(group=editor_group))

        slide_viewer = Slide.objects.create(
            headline="%s Viewer" % name, start_date=server_now()
        )
        slide_viewer.owners.add(alt_owner_who)
        slide_viewer.viewers.add(Who.objects.canonical(user=viewer_user))
        slide_viewer.viewers.add(Who.objects.canonical(group=viewer_group))
        era_viewer = Era.objects.create(
            headline="%s Viewer" % name, start_date=server_now(), end_date=server_now()
        )
        era_viewer.owners.add(alt_owner_who)
        era_viewer.viewers.add(Who.objects.canonical(user=viewer_user))
        era_viewer.viewers.add(Who.objects.canonical(group=viewer_group))

        slide_public_view = Slide.objects.create(
            headline="Public view", start_date=server_now()
        )
        slide_public_view.owners.add(alt_owner_who)
        slide_public_view.viewers.add(Who.objects.canonical(public=True))
        era_public_view = Era.objects.create(
            headline="%s Public view" % name,
            start_date=server_now(),
            end_date=server_now(),
        )
        era_public_view.owners.add(alt_owner_who)
        era_public_view.viewers.add(Who.objects.canonical(public=True))

        slide_public_edit = Slide.objects.create(
            headline="%s Public edit" % name, start_date=server_now()
        )
        slide_public_edit.editors.add(Who.objects.canonical(public=True))
        slide_public_edit.owners.add(alt_owner_who)
        era_public_edit = Era.objects.create(
            headline="%s Public edit" % name,
            start_date=server_now(),
            end_date=server_now(),
        )
        era_public_edit.owners.add(alt_owner_who)
        era_public_edit.editors.add(Who.objects.canonical(public=True))

        ret[name] = {
            "timeline": timeline,
            "owner_user": owner_user,
            "owner_group": owner_group,
            "editor_user": editor_user,
            "editor_group": editor_group,
            "viewer_user": viewer_user,
            "viewer_group": viewer_group,
            "slides": {
                "inherit": slide_inherit,
                "owned": slide_owned,
                "editor": slide_editor,
                "viewer": slide_viewer,
                "public_view": slide_public_view,
                "public_edit": slide_public_edit,
            },
            "eras": {
                "inherit": era_inherit,
                "owned": era_owned,
                "editor": era_editor,
                "viewer": era_viewer,
                "public_view": era_public_view,
                "public_edit": era_public_edit,
            },
        }
        timeline.slides.set(list(ret[name]["slides"].values()))
        timeline.eras.set(list(ret[name]["eras"].values()))

    public_view = Timeline.objects.create(name="public view")
    public_view_owner_user = User.objects.create(username="public_view_owner_user")
    public_view.owners.add(Who.objects.canonical(user=public_view_owner_user))
    public_view.viewers.add(Who.objects.canonical(public=True))
    ret["public_view"] = {"timeline": public_view, "owner_user": public_view_owner_user}
    public_edit = Timeline.objects.create(name="public edit")
    public_edit_owner_user = User.objects.create(username="public_edit_owner_user")
    public_edit.owners.add(Who.objects.canonical(user=public_edit_owner_user))
    public_edit.editors.add(Who.objects.canonical(public=True))
    ret["public_edit"] = {"timeline": public_edit, "owner_user": public_edit_owner_user}
    ret["rando"] = User.objects.create(username="rando")
    return ret


@pytest.mark.django_db
def test_timeline_permissions(perms_timeline_set):
    superuser = User.objects.create(username="superuser", is_superuser=True)

    timeline = perms_timeline_set["tl1"]["timeline"]
    timeline2 = perms_timeline_set["tl2"]["timeline"]
    public_edit_timeline = perms_timeline_set["public_edit"]["timeline"]
    public_view_timeline = perms_timeline_set["public_view"]["timeline"]

    owner_user = perms_timeline_set["tl1"]["owner_user"]
    owner_group = perms_timeline_set["tl1"]["owner_group"]
    owner_group_user = owner_group.members.first().user
    editor_user = perms_timeline_set["tl1"]["editor_user"]
    editor_group = perms_timeline_set["tl1"]["editor_group"]
    editor_group_user = editor_group.members.first().user
    viewer_user = perms_timeline_set["tl1"]["viewer_user"]
    viewer_group = perms_timeline_set["tl1"]["viewer_group"]
    viewer_group_user = viewer_group.members.first().user
    rando = perms_timeline_set["rando"]

    # Timeline Ownership
    for user in [owner_user, owner_group_user]:
        assert set(Timeline.objects.owned_by(user)) == set([timeline])
        assert timeline.owned_by(user) is True
    for user in [
        editor_user,
        editor_group_user,
        viewer_user,
        viewer_group_user,
        rando,
        None,
    ]:
        assert set(Timeline.objects.owned_by(user)) == set([])
        assert timeline.owned_by(user) is False
        assert public_edit_timeline.owned_by(user) is False
        assert public_view_timeline.owned_by(user) is False

    assert set(Timeline.objects.owned_by(superuser)) == set(
        [timeline, public_edit_timeline, public_view_timeline, timeline2]
    )
    assert set(Timeline.objects.owned_by(superuser, implicit=False)) == set()
    assert timeline.owned_by(superuser) is True
    assert timeline.owned_by(superuser, implicit=False) is False

    # Timeline Editorship
    for user in [owner_user, owner_group_user, editor_user, editor_group_user]:
        assert set(Timeline.objects.editable_by(user)) == set(
            [timeline, public_edit_timeline]
        )
        assert timeline.editable_by(user) is True
        assert public_edit_timeline.editable_by(user) is True
        assert public_view_timeline.editable_by(user) is False
    for user in [viewer_user, viewer_group_user, rando, None]:
        assert set(Timeline.objects.editable_by(user)) == set([public_edit_timeline])
        assert timeline.editable_by(user) is False
        assert public_edit_timeline.editable_by(user) is True
        assert public_view_timeline.editable_by(user) is False

    assert set(Timeline.objects.editable_by(superuser)) == set(
        [timeline, public_edit_timeline, public_view_timeline, timeline2]
    )
    assert set(Timeline.objects.editable_by(superuser, implicit=False)) == set()
    assert timeline.editable_by(superuser) is True
    assert timeline.editable_by(superuser, implicit=False) is False

    # Tiemline Viewership
    for user in [
        owner_user,
        owner_group_user,
        editor_user,
        editor_group_user,
        viewer_user,
        viewer_group_user,
    ]:
        assert set(Timeline.objects.viewable_by(user)) == set(
            [timeline, public_edit_timeline, public_view_timeline]
        ), user
        assert timeline.viewable_by(user) is True
        assert public_edit_timeline.viewable_by(user) is True
        assert public_view_timeline.viewable_by(user) is True
    for user in [rando, None]:
        assert set(Timeline.objects.viewable_by(user)) == set(
            [public_edit_timeline, public_view_timeline]
        )
        assert timeline.viewable_by(user) is False
        assert public_edit_timeline.viewable_by(user) is True
        assert public_view_timeline.viewable_by(user) is True

    assert set(Timeline.objects.viewable_by(superuser)) == set(
        [timeline, public_edit_timeline, public_view_timeline, timeline2]
    )
    assert set(Timeline.objects.viewable_by(superuser, implicit=False)) == set()
    assert timeline.viewable_by(superuser) is True
    assert timeline.viewable_by(superuser, implicit=False) is False

    #
    # Slides
    #
    slides = perms_timeline_set["tl1"]["slides"]
    slides2 = perms_timeline_set["tl2"]["slides"]
    eras = perms_timeline_set["tl1"]["eras"]
    eras2 = perms_timeline_set["tl2"]["eras"]

    # Slide ownership
    for user in [owner_user, owner_group_user]:
        assert set(Slide.objects.owned_by(user)) == set(
            [slides["inherit"], slides["owned"]]
        )
        assert set(Era.objects.owned_by(user)) == set([eras["inherit"], eras["owned"]])
    for user in [
        editor_user,
        editor_group_user,
        viewer_user,
        viewer_group_user,
        rando,
        None,
    ]:
        assert set(Slide.objects.owned_by(user)) == set([])
        assert set(Era.objects.owned_by(user)) == set([])

    # Slide editorship
    for user in [owner_user, owner_group_user]:
        assert set(Slide.objects.editable_by(user)) == set(
            [
                # inherited from timeline
                slides["inherit"],
                # explicit owner
                slides["owned"],
                # public
                slides["public_edit"],
                slides2["public_edit"],
            ]
        ), user
        assert set(Era.objects.editable_by(user)) == set(
            [
                # inherited from timeline
                eras["inherit"],
                # owned
                eras["owned"],
                # public
                eras["public_edit"],
                eras2["public_edit"],
            ]
        )
    for user in [editor_user, editor_group_user]:
        assert set(Slide.objects.editable_by(user)) == set(
            [
                # inherited from timeline
                slides["inherit"],
                slides["owned"],
                # explicit editor
                slides["editor"],
                # public
                slides["public_edit"],
                slides2["public_edit"],
            ]
        ), user
        assert set(Era.objects.editable_by(user)) == set(
            [
                # inherited from timeline
                eras["inherit"],
                eras["owned"],
                # explicit editor
                eras["editor"],
                # public
                eras["public_edit"],
                eras2["public_edit"],
            ]
        ), user
    for user in [viewer_user, viewer_group_user, rando, None]:
        assert set(Slide.objects.editable_by(user)) == set(
            [slides["public_edit"], slides2["public_edit"]]
        )
        assert set(Era.objects.editable_by(user)) == set(
            [eras["public_edit"], eras2["public_edit"]]
        )

    # Slide viewership
    for user in [owner_user, owner_group_user]:
        assert set(Slide.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                slides["inherit"],
                # explicit owner
                slides["owned"],
                # public
                slides["public_edit"],
                slides2["public_edit"],
                slides["public_view"],
                slides2["public_view"],
            ]
        ), user
        assert set(Era.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                eras["inherit"],
                # owned
                eras["owned"],
                # public
                eras["public_edit"],
                eras2["public_edit"],
                eras["public_view"],
                eras2["public_view"],
            ]
        )
    for user in [editor_user, editor_group_user]:
        assert set(Slide.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                slides["inherit"],
                slides["owned"],
                # explicit editor
                slides["editor"],
                # public
                slides["public_edit"],
                slides2["public_edit"],
                slides["public_view"],
                slides2["public_view"],
            ]
        ), user
        assert set(Era.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                eras["inherit"],
                eras["owned"],
                # explicit editor
                eras["editor"],
                # public
                eras["public_edit"],
                eras2["public_edit"],
                eras["public_view"],
                eras2["public_view"],
            ]
        ), user
    for user in [viewer_user, viewer_group_user]:
        assert set(Slide.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                slides["inherit"],
                slides["owned"],
                # explicit viewer
                slides["viewer"],
                # public
                slides["public_edit"],
                slides2["public_edit"],
                slides["public_view"],
                slides2["public_view"],
            ]
        ), user
        assert set(Era.objects.viewable_by(user)) == set(
            [
                # inherited from timeline
                eras["inherit"],
                eras["owned"],
                # explicit viewer
                eras["viewer"],
                # public
                eras["public_edit"],
                eras2["public_edit"],
                eras["public_view"],
                eras2["public_view"],
            ]
        ), user
    for user in [rando, None]:
        assert set(Slide.objects.viewable_by(user)) == set(
            [
                slides["public_edit"],
                slides2["public_edit"],
                slides["public_view"],
                slides2["public_view"],
            ]
        )
        assert set(Era.objects.viewable_by(user)) == set(
            [
                eras["public_edit"],
                eras2["public_edit"],
                eras["public_view"],
                eras2["public_view"],
            ]
        )


@pytest.mark.django_db
def test_who_association():
    # Associate matched username
    who = Who.objects.create(invitee="someusername")
    user = User.objects.create(username="someusername")
    who = Who.objects.get(pk=who.pk)
    assert who.user == user
    assert who.invitee == ""

    # Associate matched email
    who = Who.objects.create(invitee="test@example.com")
    user = User.objects.create(email="test@example.com", username="foo")
    who = Who.objects.get(pk=who.pk)
    assert who.user == user
    assert who.invitee == ""

    # No association if no match
    who = Who.objects.create(invitee="someone else")
    user = User.objects.create(username="not someone else")
    who = Who.objects.get(pk=who.pk)
    assert who.user is None
    assert who.invitee == "someone else"


@pytest.mark.django_db
def test_search_whos():
    john_username = User.objects.create(username="john")
    john_displayname = User.objects.create(display_name="Johnny", username="sdfg")
    john_email = User.objects.create(email="john@example.com", username="asdf")

    whos_canonical = {}
    whos_future = {}
    whos_expired = {}
    for u in User.objects.all():
        whos_canonical[u] = Who.objects.canonical(user=u)
        whos_future[u] = Who.objects.create(
            user=u, expiration=server_now() + datetime.timedelta(days=1)
        )
        whos_expired[u] = Who.objects.create(
            user=u, expiration=server_now() - datetime.timedelta(days=1)
        )

    the_johns = TimelineGroup.objects.create(name="Johns")
    the_johns.owners.add(whos_canonical[john_username])
    the_johns.members.set([whos_canonical[john_displayname], whos_expired[john_email]])
    for g in TimelineGroup.objects.all():
        whos_canonical[g] = Who.objects.canonical(group=g)
        whos_future[g] = Who.objects.create(
            group=g, expiration=server_now() + datetime.timedelta(days=1)
        )
        whos_expired[g] = Who.objects.create(
            group=g, expiration=server_now() - datetime.timedelta(days=1)
        )

    assert set(Who.objects.associated_with(john_username)) == {
        whos_canonical[john_username],
        whos_future[john_username],
        whos_canonical[the_johns],
        whos_future[the_johns],
    }

    # Will show whos that are associated with john_username is in, which
    # respond to 'john'.
    assert set(Who.objects.search(john_username, "john")) == {
        whos_canonical[john_username],
        whos_future[john_username],
        whos_canonical[john_displayname],
        whos_canonical[the_johns],
        whos_future[the_johns],
    }
    assert set(Who.objects.search(john_username, "johnny")) == {
        whos_canonical[john_displayname]
    }
    assert set(Who.objects.search(john_username, "johns")) == {
        whos_canonical[the_johns],
        whos_future[the_johns],
    }
    assert set(Who.objects.search(john_email, "john")) == {
        whos_canonical[john_email],
        whos_future[john_email],
    }
    assert set(Who.objects.search(john_displayname, "john")) == {
        whos_canonical[john_displayname],
        whos_future[john_displayname],
        whos_canonical[john_username],
        whos_canonical[the_johns],
        whos_future[the_johns],
    }

    assert set(Who.objects.search(john_username, "john@example.com")) == set()
    tl = Timeline.objects.create()
    tl.owners.add(whos_canonical[john_username])
    tl.owners.add(whos_canonical[john_email])
    assert set(Who.objects.search(john_username, "john@example.com")) == {
        whos_canonical[john_email]
    }
