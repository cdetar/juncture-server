"use strict";

var path = require("path")
var webpack = require("webpack")
var BundleTracker = require("webpack-bundle-tracker")
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var autoprefixer = require('autoprefixer')

var root = path.join(__dirname, '..')

module.exports = {
  devtool: 'eval',
  entry: {
    'main': [
      path.join(root, 'static', 'scss', 'index.scss'),
      path.join(root, 'static', 'js', 'index.js'),
    ],
    'readonly': [
      path.join(root, 'static', 'scss', 'readonly.scss'),
      path.join(root, 'static', 'js', 'readonly.js'),
    ],
    "editor": [
      path.join(root, 'richtext', 'static', 'richtext', 'editor.scss'),
      path.join(root, 'richtext', 'static', 'richtext', 'editor.js'),
    ],
  },
  output: {
    path: path.join(root, 'static', 'tmp'),
    filename: '[name]-[hash].js',
    publicPath: '/static/dist/'
  },
  plugins: [
    new BundleTracker({
      path: path.join(root, 'static', 'tmp'),
      filename: 'webpack-stats.json'
    }),
    //new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new ExtractTextPlugin('[name]-[hash].css'),
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader!sass-loader')
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
      },
      {test: /\.woff2?(\?v=.*)?$/, loader: "url?limit=10000&mimetype=application/font-woff" },
      {test: /\.ttf(\?v=.*)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
      {test: /\.eot(\?v=.*)?$/, loader: "file" },
      {test: /\.svg(\?v=.*)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },
      {test: /\.otf(\?v=.*)?$/, loader: "url?limit=10000&mimetype=application/font-sfnt" },
    ]
  },
  postcss: function() {
    return [autoprefixer];
  },
  resolve: {
    // The path wherein we expect modules directories (e.g. node_modules) to reside.
    root: root
  }
};

