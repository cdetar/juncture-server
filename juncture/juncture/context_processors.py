from django.conf import settings
from django.contrib.sites.models import Site

from accounts.utils import serialize_auth_state
from timelines.models import Timeline


def public_settings(request):
    site = Site.objects.get_current()
    return {
        "public_settings": {
            "PUBLIC_API_KEYS": settings.PUBLIC_API_KEYS,
            "MEDIA_URL": settings.MEDIA_URL,
            "SITE": {"name": site.name, "domain": site.domain},
            "CATEGORY_COLORS": Timeline.CATEGORY_COLORS,
        },
        "auth": serialize_auth_state(request.user),
    }
