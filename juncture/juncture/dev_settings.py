# flake8: noqa
from .default_settings import *

DEBUG = THUMBNAIL_DEBUG = True

# Allow insecure passwords in dev.
AUTH_PASSWORD_VALIDATORS = []

WEBPACK_LOADER = {
    "DEFAULT": {
        "CACHE": False,
        "BUNDLE_DIR_NAME": "dev/",
        "STATS_FILE": os.path.join(BASE_DIR, "static", "dev", "webpack-stats.json"),
        "POLL_INTERVAL": 0.1,
        "IGNORE": [".+\.hot-update.js", ".+\.map"],
    }
}
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
DEBUG_TOOLBAR_CONFIG = {"JQUERY_URL": ""}
