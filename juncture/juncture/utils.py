import datetime
from django.utils import timezone
from django.conf import settings
import pytz
from random import SystemRandom

system_random = SystemRandom()
settings_time_zone = pytz.timezone(settings.TIME_ZONE)

DATE_PARTS = ["year", "month", "day", "hour", "minute", "second", "millisecond"]


def server_now():
    return timezone.now().astimezone(settings_time_zone)


def secure_random_string(length=32, alphabet=None):
    alphabet = alphabet or "abcdefghijklmnopqrstuvwxyz0123456789"
    return "".join(system_random.choice(alphabet) for i in range(length))


def date_to_timelinejs(date, precision):
    tjs_date = {}
    if date is None:
        return tjs_date

    date = date.astimezone(settings_time_zone)
    for i, target in enumerate(DATE_PARTS):
        if precision in DATE_PARTS[i:]:
            tjs_date[target] = getattr(date, target)
    return tjs_date


def date_from_timelinejs(tjs_date):
    args = []
    precision = None
    for part in DATE_PARTS:
        if tjs_date.get(part):
            args.append(tjs_date[part])
            precision = part
        else:
            break
    if precision is None:
        return None, "day"

    defaults = [2017, 1, 1, 12, 0, 0, 0]
    if len(args) < len(defaults):
        args += defaults[len(args) :]
    return datetime.datetime(*args, tzinfo=settings_time_zone), precision
