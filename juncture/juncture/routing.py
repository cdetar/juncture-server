from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from django.conf.urls import url
from timelines.channels import TimelinesConsumer

application = ProtocolTypeRouter(
    {"websocket": AuthMiddlewareStack(URLRouter([url(r"", TimelinesConsumer)]))}
)
