import functools
import json
import logging

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.serializers.json import DjangoJSONEncoder

channel_layer = get_channel_layer()
logger = logging.getLogger("django.channels.server")


def json_dumps(obj):
    # Be sure to escape this output in templates using `escapejson`.
    return json.dumps(obj, cls=DjangoJSONEncoder)


def prepare_message(payload=None, type=None):
    obj = {}
    if payload is not None:
        obj["payload"] = payload
    if type:
        obj["type"] = type
    return json_dumps(obj)


def layer_message(msg_str):
    """
    Pack the message into a dict as required for channel layer message passing.
    https://channels.readthedocs.io/en/latest/topics/channel_layers.html The
    "type" will be magic-munged to find a handler method on the consumers who
    receive it.  Assume a "forward_message" handler that will simply pass the
    message on to the client.
    """
    return {"type": "forward.message", "message": msg_str}


def broadcast(group_name, **kwargs):
    """
    Send the payload 'kwargs' to all clients in the group with the given name.
    """
    msg_str = prepare_message(**kwargs)
    async_to_sync(channel_layer.group_send)(group_name, layer_message(msg_str))


def send_channel(consumer_or_channel_name, **kwargs):
    msg_str = prepare_message(**kwargs)
    logger.debug("channel_utils.OUT %s", msg_str)
    if isinstance(consumer_or_channel_name, str):
        async_to_sync(channel_layer.send)(
            consumer_or_channel_name, layer_message(msg_str)
        )
    else:
        consumer_or_channel_name.send(msg_str)


def handle_error(consumer, error, actionType=""):
    send_channel(consumer, type="ERROR", payload={"type": actionType, "error": error})


def require_payload_keys(keylist):
    """
    Decorator to enforce that a message contains a 'payload' key with the given
    keylist as subkeys.
    """

    def decorator(fn):
        @functools.wraps(fn)
        def inner(consumer, data, *args, **kwargs):
            if "payload" not in data:
                return handle_error(consumer, "Requires 'payload' key", data["type"])
            if not isinstance(data["payload"], dict):
                return handle_error(consumer, "'payload' must be a dict", data["type"])
            for key in keylist:
                if key not in data["payload"]:
                    return handle_error(
                        consumer, "Missing '%s' payload key." % key, data["type"]
                    )
            return fn(consumer, data, *args, **kwargs)

        return inner

    return decorator
