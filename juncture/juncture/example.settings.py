# flake8: noqa
from .dev_settings import *

# Add client id's and secrets for social account providers. Changes only take
# effect when you sync them to the database with `./manage.py sync_auth_providers`.
ALLAUTH_APP_KEYS["twitter"]["client_id"] = ""
ALLAUTH_APP_KEYS["twitter"]["secret"] = ""
ALLAUTH_APP_KEYS["facebook"]["client_id"] = ""
ALLAUTH_APP_KEYS["facebook"]["secret"] = ""
ALLAUTH_APP_KEYS["google"]["client_id"] = ""
ALLAUTH_APP_KEYS["google"]["secret"] = ""

# Add INSTALLED_APPS for any in-use social account providers.
for name, keys in ALLAUTH_APP_KEYS.items():
    if keys["client_id"]:
        INSTALLED_APPS.append("allauth.socialaccount.providers.%s" % name)
