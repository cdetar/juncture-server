from django.contrib import admin
from .models import RichTextField
from .forms import RichTextWidget, RichTextFormField


class RichTextMixin:
    formfield_overrides = {
        RichTextField: {"widget": RichTextWidget, "form_class": RichTextFormField}
    }


class RichTextAdmin(RichTextMixin, admin.ModelAdmin):
    pass
