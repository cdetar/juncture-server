import pytest
from richtext.utils import sanitize


@pytest.mark.unit
def test_sanitize(settings):
    assert sanitize(
        "<p>Yes</p><script type='text/javascript'>alert('no');</script>"
    ) == (
        """<p>Yes</p>&lt;script type="text/javascript"&gt;alert('no');"""
        """&lt;/script&gt;"""
    )

    # target blank, noreferrer for external links
    assert sanitize("<a href='http://google.com/'>OK</a>") == (
        '<a href="http://google.com/" rel="noopener noreferrer" target="_blank">'
        "OK</a>"
    )

    # No modification for local links
    assert sanitize("<a href='/local/link'>OK</a>") == '<a href="/local/link">OK</a>'

    # linkify
    assert sanitize("http://google.com") == (
        '<a href="http://google.com" rel="noopener noreferrer" target="_blank">'
        "http://google.com"
        "</a>"
    )

    assert sanitize("<p><br></p>") == ""
    assert sanitize("<p>&nbsp;</p>") == ""
    assert sanitize("<p>&nbsp;</p>\n<p><br></p>") == ""
    assert sanitize("<p>&nbsp;</p><p>OK</p>") == "<p>\xa0</p><p>OK</p>"
