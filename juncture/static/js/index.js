import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { configureStore } from "./store";
import { connectSocket } from "./store/transport";
import "./utils/jquery";

const store = configureStore(window.__INITIAL_STATE__);
connectSocket(store);

/*
 * Full app.
 */
// Weird syntax for allowing re-importing for hot reloading.
function bootApp(store) {
  const _boot = function() {
    if (indexEl) {
      ReactDOM.render(
        <Provider store={store}>
          <Index />
        </Provider>,
        indexEl
      );
    }
  };

  module.hot &&
    module.hot.accept("./components/Index.js", () => {
      if (store) {
        Index = require("./components/Index.js")["default"];
        _boot();
      }
    });
  _boot(store);
}
let Index = require("./components/Index.js")["default"];
let indexEl = document.getElementById("app");
if (indexEl) {
  bootApp(store);
}

/*
 * Navbar only, for non-single-page-app pages.
 */
// Weird import syntax for allowing re-importing for hot reloading.
function bootNavbar(store) {
  const _boot = function() {
    if (navbarEl) {
      ReactDOM.render(
        <Provider store={store}>
          <Navbar />
        </Provider>,
        navbarEl
      );
    }
  };

  module.hot &&
    module.hot.accept("./components/Navbar.js", () => {
      if (store) {
        Navbar = require("./components/Navbar.js")["default"];
        _boot();
      }
    });
  _boot(store);
}
let Navbar = require("./components/Navbar.js")["default"];
let navbarEl = document.getElementById("navbar");
if (navbarEl) {
  bootNavbar(store);
}
