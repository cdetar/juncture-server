import * as A from "./actions";

let _socketClient = null;

export const connectSocket = store => {
  if (_socketClient) {
    try {
      _socketClient.close();
    } catch (e) {
      console.error(e);
    }
  }
  _socketClient = new SocketClient(store);
};

export class SocketClient {
  constructor(store) {
    this.store = store;
    this.debug = true;
    // eslint-disable-next-line no-console
    this.log = (...params) => this.debug && console.log.apply(console, params);
    this.connect();
  }

  connect() {
    this.log("SocketClient.connect");
    this.url = window.location.href.replace(/^http/, "ws").split("#")[0];
    this.socket = new WebSocket(this.url);
    this.socket.onmessage = e => this.onMessage(e);
    this.socket.onopen = e => this.onOpen(e);
    this.socket.onclose = e => this.onClose(e);
    this.socket.onerror = e => this.onError(e);
    this.store.dispatch(A.socketConnecting({ url: this.url }));
  }

  close() {
    try {
      this.socket.close();
    } catch (e) {
      console.error(e);
    }
    if (this.heartbeat) {
      clearInterval(this.heartbeat);
      this.heartbeat = null;
    }
    this.store.dispatch(A.socketClosing({ url: this.url }));
  }

  onMessage(event) {
    let data;
    try {
      data = JSON.parse(event.data);
    } catch (e) {
      console.error("Transport.onMessage: invalid JSON", e);
      return;
    }
    this.log("Transport.onMessage", data);
    switch (data.type) {
      case A.SET_PRESENCE_TYPE:
        this.store.dispatch(A.setPresence(data.payload));
        break;
      case A.SET_AUTH_TYPE:
        this.store.dispatch(A.setAuth(data.payload));
        break;
      case A.SET_TIMELINE_TYPE:
        this.store.dispatch(A.setTimeline(data.payload));
        break;
      case A.SET_SLIDES_TYPE:
        this.store.dispatch(A.setSlides(data.payload));
        break;
      case A.SET_ERAS_TYPE:
        this.store.dispatch(A.setEras(data.payload));
        break;
      case A.SET_PERMISSIONS_TYPE:
        this.store.dispatch(A.setPermissions(data.payload));
        break;
      case A.SET_LOADING_TYPE:
        this.store.dispatch(A.setLoading(data.payload));
        break;
      case A.SET_ERROR_TYPE:
        console.error(data);
        if (data.payload.type) {
          this.store.dispatch(A.setError(data.payload));
        } else {
          alert(`Server error: ${data.payload.error}`);
        }
        break;
      case A.SET_GROUPS_TYPE:
        this.store.dispatch(A.setGroups(data.payload));
        break;
      case A.SET_SEARCH_WHOS_TYPE:
        this.store.dispatch(A.setSearchWhos(data.payload));
        break;
      case A.FETCH_TIMELINE_TYPE:
        this.store.dispatch(A.handleFetchedTimeline(data.payload));
        break;
      case A.FETCH_TIMELINE_LIST_TYPE:
        this.store.dispatch(A.handleFetchedTimelineList(data.payload));
        break;
      case A.REDIRECT_TYPE:
        this.store.dispatch(A.setRedirect(data.payload));
        break;
      default:
        console.error("transport.js: Unhandled message:", data);
        break;
    }
  }

  onOpen(event) {
    this.log("SocketClient.onOpen");
    setTimeout(() => {
      this.heartbeat = setInterval(() => {
        this.sendMessage("heartbeat");
      }, 30000);
    }, Math.random() * 30000);
    this.store.dispatch(A.socketOpen({ url: this.url }));
    if (this._connectInterval) {
      clearTimeout(this._connectInterval);
      this._connectInterval = null;
    }
  }

  onClose(event) {
    this.log("SocketClient.onClose");
    this.store.dispatch(A.socketClosed({ url: this.url }));
    if (!this._connectInterval) {
      this._connectInterval = setInterval(() => {
        if (this.store.getState().socket.state === WebSocket.CLOSED) {
          this.connect();
        }
      }, 1000);
    }
  }

  onError(event) {
    this.log("SocketClient.onError");
    this.store.dispatch(A.socketError({ error: "error" }));
  }

  sendMessage(msg) {
    let string = JSON.stringify(msg);
    let waitForSend = resolve => {
      if (this.socket.bufferedAmount > 0) {
        setTimeout(() => waitForSend(resolve), 10);
      } else {
        resolve();
      }
    };
    return new Promise((resolve, reject) => {
      try {
        this.socket.send(string);
      } catch (e) {
        return reject(e);
      }
      waitForSend(resolve);
    });
  }
}

export const sendSocketMessage = function(message, dispatch, startTime) {
  startTime = startTime || Date.now();

  const handleError = err => {
    if (dispatch && message.type) {
      // If it looks like we have the means to dispatch this error, do so.
      return dispatch(A.setError({ type: message.type, error: err }));
    } else {
      // Otherwise, propagate the error.
      return Promise.reject(err);
    }
  };

  if (!(_socketClient && _socketClient.socket.readyState === WebSocket.OPEN)) {
    // poll until socket is ready.
    return new Promise((resolve, reject) => {
      if (Date.now() - startTime > 30000) {
        handleError("Timed out waiting for WebSocket to connect.").catch(err =>
          reject(err)
        );
      }
      setTimeout(() => {
        sendSocketMessage(message, dispatch, startTime)
          .then(resolve)
          .catch(reject);
      }, 100);
    });
  }
  return _socketClient.sendMessage(message).catch(handleError);
};
