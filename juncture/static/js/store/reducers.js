import { csrftoken as theCsrfToken } from "./csrf";
import * as A from "./actions";
import _ from "lodash";

export const csrftoken = (state, action) => theCsrfToken;
export const settings = (state, action) => window.__SETTINGS__ || {};
export const auth = (state, action) => window.__AUTH__ || {};

//
// Reducers for timeline data.
//
export const timelineList = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.FETCH_TIMELINE_LIST_TYPE:
      state = {};
      action.payload.forEach(
        timeline => (state[timeline.secret_id] = timeline)
      );
      return state;
  }
  return state;
};

export const timelines = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_TIMELINE_TYPE:
      return { ...state, [action.payload.secret_id]: action.payload };
  }
  return state;
};

export const slides = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_SLIDES_TYPE:
      state = { ...state };
      action.payload.forEach(slide => (state[slide.id] = slide));
      return state;
  }
  return state;
};

export const eras = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_ERAS_TYPE:
      state = { ...state };
      action.payload.forEach(era => (state[era.id] = era));
      return state;
  }
  return state;
};

export const permissions = (state = null, action) => {
  state = state || { timelines: {}, eras: {}, slides: {} };
  switch (action.type) {
    case A.SET_PERMISSIONS_TYPE:
      state = { ...state };
      _.forEach(action.payload, (idmap, type) => {
        _.forEach(idmap, (permmap, id) => {
          state[type][id] = {
            ...state[type][id],
            ...permmap
          };
        });
      });
      return state;
  }
  return state;
};

export const groups = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_GROUPS_TYPE:
      state = { ...state };
      _.each(action.payload.groups, (group, id) => {
        if (group === null) {
          delete state[id];
        } else {
          state[id] = group;
        }
      });
      break;
  }
  return state;
};

export const whoSearch = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_SEARCH_WHOS_TYPE:
      state = { ...state };
      _.each(action.payload.whos, (who, id) => {
        state[id] = who;
      });
      break;
  }
  return state;
};

//
// Reducer for the "socket" namespace. Holds state for connection status and
// message error status for the socket.
//
export const socket = (state = null, action) => {
  state = state || { state: WebSocket.CLOSED };
  switch (action.type) {
    case A.SOCKET_CONNECTING_TYPE:
      return { state: WebSocket.CONNECTING, url: action.payload.url };
    case A.SOCKET_OPEN_TYPE:
      return { state: WebSocket.OPEN, url: action.payload.url };
    case A.SOCKET_CLOSING_TYPE:
      return { state: WebSocket.CLOSING, url: action.payload.url };
    case A.SOCKET_CLOSED_TYPE:
      return { state: WebSocket.CLOSED, url: action.payload.url };
    case A.SOCKET_ERROR_TYPE:
      return { state: "ERROR", error: action.payload.error, ...state };
  }
  return state;
};

//
// Reducer for generic loading states.
//
export const loading = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.SET_LOADING_TYPE:
      return {
        ...state,
        [action.payload.type]: action.payload.loading
      };
  }
  return state;
};
export const error = (state = null, action) => {
  state = state || {};
  if (action.type === A.SET_ERROR_TYPE) {
    let str = action.payload.error.toString();
    if (str === "[object Object]") {
      str = action.payload.error.body;
    }
    if (!str) {
      str = "Server error";
    }
    console.error(action.payload.error);
    state = {
      ...state,
      [action.payload.type]: str
    };
    return state;
  }
  return state;
};

//
// Reducer for presence indication.
//
export const presence = (state = null, action) => {
  state = state || { path: null, members: [] };
  switch (action.type) {
    case A.SET_PRESENCE_TYPE:
      return action.payload;
  }
  return state;
};

//
// Google API
//
export const goog = (state = null, action) => {
  state = state || {
    loaded: false,
    authed: false
  };
  switch (action.type) {
    case A.SET_GAPI_LOADED_TYPE:
      return {
        ...state,
        loaded: action.payload
      };
    case A.SET_GAPI_AUTH_TYPE:
      return {
        ...state,
        authed: action.payload
      };
  }
  return state;
};

//
// Server-pushed redirect
//
export const redirect = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.REDIRECT_TYPE:
      return {
        path: action.payload
      };
  }
  return state;
};

//
// Help text
//
export const help = (state = null, action) => {
  state = state || {};
  switch (action.type) {
    case A.HELP_TYPE:
      state = {
        ...state,
        ...action.payload
      };
  }
  return state;
};
