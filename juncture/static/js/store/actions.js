import { sendSocketMessage } from "./transport.js";
import * as goog from "../utils/goog.js";
import { csrftoken } from "../utils/csrf.js";

// Sockets
export const SOCKET_CONNECTING_TYPE = "SOCKET_CONNECTING";
export const socketConnecting = payload => ({
  type: SOCKET_CONNECTING_TYPE,
  payload
});
export const SOCKET_OPEN_TYPE = "SOCKET_OPEN";
export const socketOpen = payload => ({ type: SOCKET_OPEN_TYPE, payload });
export const SOCKET_CLOSING_TYPE = "SOCKET_CLOSING";
export const socketClosing = payload => ({
  type: SOCKET_CLOSING_TYPE,
  payload
});
export const SOCKET_CLOSED_TYPE = "SOCKET_CLOSED";
export const socketClosed = payload => ({ type: SOCKET_CLOSED_TYPE, payload });
export const SOCKET_ERROR_TYPE = "SOCKET_ERROR";
export const socketError = payload => ({ type: SOCKET_ERROR_TYPE, payload });

// Progress
export const SET_LOADING_TYPE = "LOADING";
export const setLoading = payload => ({ type: SET_LOADING_TYPE, payload });
export const SET_ERROR_TYPE = "ERROR";
export const setError = payload => {
  return dispatch => {
    dispatch({ type: SET_ERROR_TYPE, payload });
    // error implies no longer loading.
    dispatch(setLoading({ type: payload.type, loading: false }));
  };
};
export const REDIRECT_TYPE = "REDIRECT";
export const setRedirect = payload => ({ type: REDIRECT_TYPE, payload });

// Presence
export const SET_PRESENCE_TYPE = "PRESENCE";
export const setPresence = payload => ({ type: SET_PRESENCE_TYPE, payload });
export const SET_AUTH_TYPE = "AUTH";
export const setAuth = payload => ({ type: SET_AUTH_TYPE, payload });

// Groups
export const FETCH_GROUPS_TYPE = "FETCH_GROUPS";
export const fetchGroups = payload => dispatch => {
  dispatch(setLoading({ type: FETCH_GROUPS_TYPE, payload }));
  sendSocketMessage({ type: FETCH_GROUPS_TYPE, payload }, dispatch);
};
export const SET_GROUPS_TYPE = "SET_GROUPS";
export const setGroups = payload => ({ type: SET_GROUPS_TYPE, payload });
export const CHANGE_GROUP_TYPE = "CHANGE_GROUP";
export const changeGroup = payload => dispatch => {
  dispatch(setLoading({ type: CHANGE_GROUP_TYPE, payload }));
  sendSocketMessage({ type: CHANGE_GROUP_TYPE, payload }, dispatch);
};
export const ADD_GROUP_TYPE = "ADD_GROUP";
export const addGroup = payload => dispatch => {
  dispatch(setLoading({ type: ADD_GROUP_TYPE, payload }));
  sendSocketMessage({ type: ADD_GROUP_TYPE, payload }, dispatch);
};
export const REMOVE_GROUP_TYPE = "REMOVE_GROUP";
export const removeGroup = payload => dispatch => {
  dispatch(setLoading({ type: REMOVE_GROUP_TYPE, payload }));
  sendSocketMessage({ type: REMOVE_GROUP_TYPE, payload }, dispatch);
};
export const LEAVE_GROUP_TYPE = "LEAVE_GROUP";
export const leaveGroup = payload => dispatch => {
  dispatch(setLoading({ type: LEAVE_GROUP_TYPE, payload }));
  sendSocketMessage({ type: LEAVE_GROUP_TYPE, payload }, dispatch);
};

// Whos
export const SET_SEARCH_WHOS_TYPE = "SET_SEARCH_WHOS";
export const SEARCH_WHOS_TYPE = "SEARCH_WHOS";
export const setSearchWhos = payload => ({
  type: SET_SEARCH_WHOS_TYPE,
  payload
});
export const searchWhos = payload => dispatch => {
  dispatch(setLoading({ type: SEARCH_WHOS_TYPE, payload }));
  sendSocketMessage({ type: SEARCH_WHOS_TYPE, payload }, dispatch);
};

// Permissions
export const SET_PERMISSIONS_TYPE = "SET_PERMISSIONS";
export const setPermissions = payload => ({
  type: SET_PERMISSIONS_TYPE,
  payload
});
export const CHANGE_PERMISSIONS_TYPE = "CHANGE_PERMISSIONS";
export const changePermissions = payload => dispatch => {
  dispatch(setLoading({ type: CHANGE_PERMISSIONS_TYPE, loading: true }));
  sendSocketMessage({ type: CHANGE_PERMISSIONS_TYPE, payload }, dispatch);
};

// Timelines
export const SET_TIMELINE_TYPE = "SET_TIMELINE";
export const setTimeline = payload => ({ type: SET_TIMELINE_TYPE, payload });
export const FETCH_TIMELINE_TYPE = "FETCH_TIMELINE";
export const handleFetchedTimeline = payload => ({
  type: FETCH_TIMELINE_TYPE,
  payload
});
export const fetchTimeline = timelineId => dispatch => {
  dispatch(
    setLoading({ type: FETCH_TIMELINE_TYPE, loading: "Loading timeline" })
  );
  sendSocketMessage(
    { type: FETCH_TIMELINE_TYPE, payload: { timeline_id: timelineId } },
    dispatch
  );
};
export const CHANGE_TIMELINE_TYPE = "CHANGE_TIMELINE";
export const changeTimeline = payload => dispatch => {
  dispatch(
    setLoading({ type: CHANGE_TIMELINE_TYPE, loading: "Updating timeline" })
  );
  sendSocketMessage({ type: CHANGE_TIMELINE_TYPE, payload: payload });
};
export const IMPORT_TIMELINE_TYPE = "IMPORT_TIMELINE";
export const importTimeline = payload => dispatch => {
  dispatch(
    setLoading({ type: IMPORT_TIMELINE_TYPE, loading: "Importing timeline" })
  );
  goog
    .fetchSpreadsheet(payload.id)
    .catch(err =>
      dispatch(setError({ type: IMPORT_TIMELINE_TYPE, error: err }))
    )
    .then(spreadsheet =>
      sendSocketMessage(
        {
          type: IMPORT_TIMELINE_TYPE,
          payload: {
            spreadsheet: spreadsheet,
            ignore_errors: payload.ignore_errors
          }
        },
        dispatch
      )
    );
};

export const UPLOAD_TIMELINE_TYPE = "UPLOAD_TIMELINE";
export const uploadTimeline = payload => dispatch => {
  dispatch(
    setLoading({ type: UPLOAD_TIMELINE_TYPE, loading: "Uploading timeline" })
  );
  const headers = new Headers();
  headers.append("X-CSRFToken", csrftoken);
  fetch("/timelines/upload", {
    method: "POST",
    body: payload,
    headers: headers,
    credentials: "include"
  })
    .then(res => (res.ok ? res.json() : Promise.reject(res)))
    .then(json => {
      dispatch(setLoading({ type: UPLOAD_TIMELINE_TYPE, loading: false }));
      dispatch({
        type: REDIRECT_TYPE,
        payload: `/timelines/${json.timeline_id}/edit`
      });
    })
    .catch(res => {
      res.text().then(text => {
        dispatch(setLoading({ type: UPLOAD_TIMELINE_TYPE, loading: false }));
        dispatch({
          type: SET_ERROR_TYPE,
          payload: {
            type: UPLOAD_TIMELINE_TYPE,
            error: `${res.statusText}: ${text}`
          }
        });
      });
    });
};

export const CREATE_TIMELINE_TYPE = "CREATE_TIMELINE";
export const createTimeline = payload => dispatch => {
  dispatch(
    setLoading({ type: CREATE_TIMELINE_TYPE, loading: "Creating timeline" })
  );
  sendSocketMessage({ type: CREATE_TIMELINE_TYPE, payload }, dispatch);
};
export const DELETE_TIMELINE_TYPE = "DELETE_TIMELINE";
export const deleteTimeline = payload => dispatch => {
  dispatch(
    setLoading({ type: DELETE_TIMELINE_TYPE, loading: "Deleting timeline" })
  );
  sendSocketMessage({ type: DELETE_TIMELINE_TYPE, payload }, dispatch);
};
export const FETCH_TIMELINE_LIST_TYPE = "FETCH_TIMELINE_LIST";
export const fetchTimelineList = payload => dispatch => {
  dispatch(setLoading({ type: FETCH_TIMELINE_LIST_TYPE, loading: true }));
  sendSocketMessage({ type: FETCH_TIMELINE_LIST_TYPE, payload }, dispatch);
};
export const handleFetchedTimelineList = payload => ({
  type: FETCH_TIMELINE_LIST_TYPE,
  payload
});
export const LEAVE_TYPE = "LEAVE";
export const leave = payload => dispatch => {
  sendSocketMessage({ type: LEAVE_TYPE, payload }, dispatch);
};
export const JOIN_TYPE = "JOIN";
export const join = payload => dispatch => {
  sendSocketMessage({ type: JOIN_TYPE, payload }, dispatch);
};

export const CHANGE_CATEGORIES_TYPE = "CHANGE_CATEGORIES";
export const changeCategories = payload => dispatch => {
  sendSocketMessage({ type: CHANGE_CATEGORIES_TYPE, payload }, dispatch);
};

// Eras
export const SET_ERAS_TYPE = "SET_ERAS";
export const setEras = payload => ({ type: SET_ERAS_TYPE, payload });
export const ADD_ERA_TYPE = "ADD_ERA";
export const addEra = payload => dispatch => {
  dispatch(setLoading({ type: ADD_ERA_TYPE, loading: true }));
  sendSocketMessage({ type: ADD_ERA_TYPE, payload }, dispatch);
};
export const CHANGE_ERA_TYPE = "CHANGE_ERA";
export const changeEra = payload => dispatch => {
  dispatch(setLoading({ type: CHANGE_ERA_TYPE, loading: true }));
  sendSocketMessage({ type: CHANGE_ERA_TYPE, payload }, dispatch);
};
export const DELETE_ERA_TYPE = "DELETE_ERA";
export const deleteEra = payload => dispatch => {
  dispatch(setLoading({ type: DELETE_ERA_TYPE, loading: true }));
  sendSocketMessage({ type: DELETE_ERA_TYPE, payload }, dispatch);
};

// Slides
export const SET_SLIDES_TYPE = "SET_SLIDES";
export const setSlides = payload => ({ type: SET_SLIDES_TYPE, payload });
export const ADD_SLIDE_TYPE = "ADD_SLIDE";
export const addSlide = payload => dispatch => {
  dispatch(setLoading({ type: ADD_SLIDE_TYPE, loading: true }));
  sendSocketMessage({ type: ADD_SLIDE_TYPE, payload }, dispatch);
};
export const CHANGE_SLIDE_TYPE = "CHANGE_SLIDE";
export const changeSlide = payload => dispatch => {
  dispatch(setLoading({ type: CHANGE_SLIDE_TYPE, loading: true }));
  sendSocketMessage({ type: CHANGE_SLIDE_TYPE, payload }, dispatch);
};
export const DELETE_SLIDE_TYPE = "DELETE_SLIDE";
export const deleteSlide = payload => dispatch => {
  dispatch(setLoading({ type: DELETE_SLIDE_TYPE, loading: true }));
  sendSocketMessage({ type: DELETE_SLIDE_TYPE, payload }, dispatch);
};

// Google API
export const SET_GAPI_AUTH_TYPE = "GAPI_AUTH";
export const SET_GAPI_LOADED_TYPE = "GAPI_LOADED";
export const loadGapi = clientId => dispatch => {
  const updateSigninStatus = status => {
    dispatch({ type: SET_GAPI_AUTH_TYPE, payload: status });
  };
  // Status "loading" for spinners
  dispatch(setLoading({ type: SET_GAPI_LOADED_TYPE, loading: true }));
  goog
    .load(clientId, updateSigninStatus)
    .then(() => {
      // goog client is now loaded.
      dispatch({ type: SET_GAPI_LOADED_TYPE, payload: true });
      // Unset "loading" for spinners
      dispatch(setLoading({ type: SET_GAPI_LOADED_TYPE, loading: false }));
    })
    .catch(err =>
      dispatch(setError({ type: SET_GAPI_LOADED_TYPE, error: err }))
    );
};
export const gapiAuth = event => dispatch => {
  goog.handleAuthClick(event).catch(err => {
    dispatch(setError({ type: SET_GAPI_AUTH_TYPE, error: err }));
  });
};
export const gapiSignout = event => dispatch => {
  goog.handleSignoutClick(event).catch(err => {
    dispatch(setError({ type: SET_GAPI_AUTH_TYPE, error: err }));
  });
};
// Help
export const HELP_TYPE = "HELP";
