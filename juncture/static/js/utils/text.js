export const quoteRe = pattern =>
  pattern.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");

export const textContrastColor = bgHex => {
  if (/#\d{6}/.test(bgHex)) {
    const r = parseInt(bgHex.substring(1, 3), 16);
    const g = parseInt(bgHex.substring(3, 5), 16);
    const b = parseInt(bgHex.substring(5, 7), 16);

    // https://en.wikipedia.org/wiki/YIQ
    const yiq = (r * 299 + g * 587 + b * 114) / 1000;
    return yiq >= 128 ? "#000000" : "#ffffff";
  }
  return "#000000";
};
