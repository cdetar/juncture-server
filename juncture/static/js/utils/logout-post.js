import { csrftoken } from "./csrf";

$(".js-logout").on("click", function(e) {
  e.preventDefault();
  var form = document.createElement("form");
  form.method = "post";
  form.action = "/accounts/logout/";
  var input = document.createElement("input");
  input.type = "hidden";
  input.name = "csrfmiddlewaretoken";
  input.value = csrftoken;
  form.appendChild(input);
  $(this).after(form);
  form.submit();
});
