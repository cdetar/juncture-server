/*
 * Utilities for working with Timeline3 data and objects.
 */
import $ from "jquery";
import _ from "lodash";
import { textContrastColor } from "./text.js";

export const addCategoryColors = (tl, categories) => {
  const isFirstLoad = !tl._junctureLoadedCallback;
  if (!isFirstLoad) {
    tl.removeEventListener("loaded", tl._junctureLoadedCallback);
  }
  return new Promise((resolve, reject) => {
    tl._junctureLoadedCallback = () => {
      const messageEls = {};
      try {
        // search for category containers with appropriate textual label.
        $(".tl-timegroup-message").each((i, msgEl) => {
          messageEls[
            $(msgEl)
              .text()
              .trim()
          ] = $(msgEl);
        });
        if (_.size(messageEls) === 0 && categories["__default__"]) {
          // No time slider categories. Just set the default category color.
          $(".tl-timenav").css("background-color", categories["__default__"]);
        } else {
          $(".tl-timenav").css("background-color", "#f2f2f2");
          // Assign the background-color to each category
          _.each(categories, (color, cat) => {
            let el =
              cat === "__default__" ? messageEls[""] : messageEls[cat.trim()];
            if (el) {
              const contrast = textContrastColor(color);
              const invContrast = textContrastColor(contrast);
              el.closest(".tl-timegroup").css("background-color", color);
              el.css("color", contrast);
              el.css("text-shadow", `0px 2px 2px ${invContrast}`);
            }
          });
        }
      } catch (e) {
        if (reject) {
          return reject(e);
        } else {
          throw e;
        }
      }
      return resolve();
    };
    tl.addEventListener("loaded", tl._junctureLoadedCallback);

    // Hack: Poll for initial load, as the 'loaded' and 'dataloaded' events
    // don't seem to reliably fire on first load.
    if (isFirstLoad) {
      const pollFirstLoadInterval = setInterval(() => {
        let isLoaded = !!tl.getCurrentSlide();
        if (isLoaded) {
          clearInterval(pollFirstLoadInterval);
          tl._junctureLoadedCallback();
        }
      });
    }
  });
};

// NOTE: Assumes that the TimelineJS3 API (window.TL) is already loaded and
// present.
const applyMediaType = slide => {
  const media = slide && slide.media;
  if (media && media.url) {
    const mediaType = window.TL.MediaType(media);
    if (!mediaType || mediaType["type"] == "imageblank") {
      // Media type not recognized. Throw it into an iframe.
      const a = document.createElement("a");
      a.href = media.url;
      // Don't allow recursing, and sanity check that the URL isn't some
      // non-URL thing.
      if (a.host && a.host !== document.location.host) {
        const clean = [
          a.protocol,
          "//",
          a.host,
          a.pathname,
          a.search,
          a.hash
        ].join("");
        return {
          ...slide,
          media: {
            ...media,
            url: `<iframe src="${clean}" style='border: none;'></iframe>`
          }
        };
      }
    }
  }
  return slide;
};

export const applyMediaTypes = tlData => {
  return {
    ...tlData,
    title: applyMediaType(tlData.title),
    events: tlData.events.map(applyMediaType)
  };
};
