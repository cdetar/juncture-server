import _ from "lodash";
import moment from "moment";

const can = (perms, auth, accessor) => {
  return _.some(
    perms[accessor],
    who =>
      // is public
      who["public"] ||
      // or user is listed
      (who.user && who.user.id === auth.id) ||
      // or user is a group owner
      (who.group &&
        _.some(
          who.group.owners,
          owho => owho.user && owho.user.id === auth.id
        )) ||
      // or user is a group member
      (who.group &&
        _.some(
          who.group.members,
          mwho => mwho.user && mwho.user.id === auth.id
        ))
  );
};

export const isOwner = (perms, auth) => can(perms, auth, "owners");
export const isEditor = (perms, auth) =>
  can(perms, auth, "owners") || can(perms, auth, "editors");
export const isViewer = (perms, auth) =>
  can(perms, auth, "owners") ||
  can(perms, auth, "editors") ||
  can(perms, auth, "viewers");

export const publicIsOwner = perms =>
  _.some(perms.owners, who => who["public"]);

export const publicIsEditor = perms =>
  publicIsOwner(perms) || _.some(perms.editors, who => who["public"]);

export const publicIsViewer = perms =>
  publicIsEditor(perms) || _.some(perms.viewers, who => who["public"]);

// Duplicated in timelines/models.py
export const NO_EXPIRATION = "2500-01-01T00:00:00Z";

export const isForever = date => {
  return moment(date).year() > 2100;
};

export const capabilities = {
  timelines: {
    owners: ["change the title", "change permissions", "delete this timeline"],
    editors: ["add and remove stories, eras, and categories"],
    viewers: ["view this timeline"]
  }
};
