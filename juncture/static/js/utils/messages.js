import jQuery from "jquery";

const $ = jQuery;

$(() =>
  $(".messages .alert").each((i, message) => {
    setTimeout(() => $(message).alert("close"), 3000 + i * 1000);
  })
);
