/* globals gapi */
import jQuery from "jquery";

// Convert Google Promises/A+ style to native promise style. Google / A+
// promises use `.then(resolve, reject)` to catch errors, native uses
// `.then(resolve).catch(reject)`
const GPromise = fn =>
  new Promise((resolve, reject) => fn().then(resolve, reject));

export const load = (clientId, updateSigninStatus) => {
  return new Promise((resolve, reject) => {
    jQuery
      .ajax({
        url: "https://apis.google.com/js/api.js",
        dataType: "script",
        cache: true
      })
      .done((script, textStatus) => {
        gapi.load("client:auth2", () => {
          initGapi(clientId, updateSigninStatus)
            .then(resolve)
            .catch(reject);
        });
      })
      .fail((jqxhr, settings, exception) => {
        reject(exception);
      });
  });
};

export const initGapi = (clientId, updateSigninStatus) => {
  return GPromise(() => {
    return gapi.client
      .init({
        discoveryDocs: [
          "https://sheets.googleapis.com/$discovery/rest?version=v4"
        ],
        clientId: clientId,
        scope: "https://www.googleapis.com/auth/spreadsheets.readonly"
      })
      .then(() => {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      });
  });
};

export const handleAuthClick = event => {
  return GPromise(() => gapi.auth2.getAuthInstance().signIn());
};

export const handleSignoutClick = event => {
  return GPromise(() => gapi.auth2.getAuthInstance().signOut());
};

export const fetchSpreadsheet = spreadsheetId => {
  return Promise.all([
    // Get spreadsheet metadata (title, etc)
    GPromise(() =>
      gapi.client.sheets.spreadsheets.get({ spreadsheetId: spreadsheetId })
    ),
    // Get spreadsheet cell range.
    GPromise(() =>
      gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: spreadsheetId,
        range: "A1:Z250"
      })
    )
  ]).then(([sheet, cells]) => {
    return {
      ...sheet.result.properties,
      cells: cells.result.values
    };
  });
};
