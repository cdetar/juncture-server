import _ from "lodash";
import moment from "moment";

export const dateFmt = (dateStr, precision = "day", type = "simple") => {
  // Assumes dateStr is a simple ISO type.
  let date = moment(dateStr);
  switch (precision) {
    case "year":
      return date.format("YYYY");
    case "month":
      return date.format("MMM YYYY");
    case "hour":
      return date.format("MMM DD, YYYY ha");
    case "minute":
      return date.format("MMM DD, YYYY h:mma");
    case "second":
      return date.format("MMM DD, YYYY h:mm:ssa");
    case "millisecond":
      return date.format("MMM DD, YYYY h:mm:ss.SSSa");
    case "day":
    default:
      return date.format("MMM DD, YYYY");
  }
};

export const timelineDateToJsDate = dateHash => {
  // Construct a date object. We can't just use the usual date constructor,
  // as year values between 0 and 99 are interpreted as 1900 - 1999.
  const date = new Date();
  const hasOwnProperty = Object.prototype.hasOwnProperty;
  date.setFullYear(dateHash.year);
  date.setMonth(
    hasOwnProperty.call(dateHash, "month") ? dateHash.month - 1 : 0
  );
  date.setDate(hasOwnProperty.call(dateHash, "day") ? dateHash.day : 1);
  date.setHours(hasOwnProperty.call(dateHash, "hour") ? dateHash.hour : 12);
  date.setMinutes(
    hasOwnProperty.call(dateHash, "minute") ? dateHash.minute : 0
  );
  date.setSeconds(
    hasOwnProperty.call(dateHash, "second") ? dateHash.second : 0
  );
  date.setMilliseconds(
    hasOwnProperty.call(dateHash, "millisecond") ? dateHash.millisecond : 0
  );
  return date;
};

export const displayTimelineDate = dateHash => {
  const date = timelineDateToJsDate(dateHash);
  let precision;
  _.each(TIME_PARTS, part => {
    if (!Object.prototype.hasOwnProperty.call(dateHash, part)) {
      return false;
    }
    precision = part;
  });
  return dateFmt(date, precision);
};

export const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

export const TIME_PARTS = [
  "year",
  "month",
  "day",
  "hour",
  "minute",
  "second",
  "millisecond"
];
