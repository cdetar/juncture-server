import React from "react";
import { connect } from "react-redux";
import * as A from "../store/actions";

class RedirectReceiver extends React.Component {
  componentWillMount() {
    this.checkRedirect(this.props);
  }
  componentWillReceiveProps(newProps) {
    this.checkRedirect(newProps);
  }
  checkRedirect(props) {
    if (props.redirect.path) {
      let path = props.redirect.path;
      props.setRedirect(null);
      props.router.push(path);
    }
  }
  render() {
    return null;
  }
}
RedirectReceiver.propTypes = {
  router: React.PropTypes.shape({
    push: React.PropTypes.func.isRequired
  }).isRequired
};

export default connect(
  state => ({ redirect: state.redirect }),
  dispatch => ({
    setRedirect: payload => dispatch(A.setRedirect(payload))
  })
)(RedirectReceiver);
