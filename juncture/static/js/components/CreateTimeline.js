import React from "react";
import { connect } from "react-redux";
import LoginRequired from "./LoginRequired";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import Navbar from "./Navbar";
import RedirectReceiver from "./RedirectReceiver";
import * as A from "../store/actions";

class CreateTimeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ""
    };
  }

  componentWillMount() {
    document.title = "Create timeline";
  }

  onSubmit(event) {
    event && event.preventDefault();
    if (this.state.name) {
      this.props.createTimeline({ name: this.state.name });
    }
  }

  render() {
    return (
      <div className="create-timeline">
        <Navbar />
        <div className="container">
          <LoginRequired />
          <RedirectReceiver router={this.props.router} />
          <h1>Create a Timeline</h1>
          <Loading id={A.CREATE_TIMELINE_TYPE}>
            <form onSubmit={this.onSubmit.bind(this)}>
              <div className="form-group">
                <label className="form-control-label">Name</label>
                <input
                  className="form-control"
                  type="text"
                  placeholder="Timeline name"
                  onChange={e => this.setState({ name: e.target.value })}
                  value={this.state.name}
                />
              </div>
              <ErrorState id={A.CREATE_TIMELINE_TYPE} />
              <button
                type="submit"
                className="btn btn-primary"
                disabled={!this.state.name}
              >
                Create
              </button>
            </form>
          </Loading>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    createTimeline: payload => dispatch(A.createTimeline(payload))
  })
)(CreateTimeline);
