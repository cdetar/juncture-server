import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import { TwitterPicker } from "react-color";
import _ from "lodash";
import { FormSlideEditor } from "./SlideEditor";
import ErrorState from "./ErrorState";
import Help from "./Help";
import InPlaceEditor from "./InPlaceEditor";
import SearchWithinTimeline from "./SearchWithinTimeline";
import {
  Permissions,
  PermissionsCompact,
  getPermissionStats
} from "./Permissions";
import Loading from "./Loading";
import jQuery from "jquery";
import * as A from "../store/actions";
import * as permutils from "../utils/permutils.js";

const ModalTrigger = props => (
  <a
    href="#"
    className={props.className}
    data-toggle="modal"
    data-target={`#${props.id}`}
  >
    {props.children}
  </a>
);

const Modal = props => {
  const Wrapper = props.onSubmit ? "form" : "div";
  const wrapperProps = props.onSubmit ? { onSubmit: props.onSubmit } : {};
  let modal = (
    <div className={"modal fade"} id={props.id}>
      <div className="modal-dialog" role="document">
        <Wrapper className="modal-content" {...wrapperProps}>
          {props.title ? (
            <div className="modal-header">
              <h5 className="modal-title">{props.title}</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ) : (
            ""
          )}
          <div className="modal-body">{props.children}</div>
          {props.footer ? (
            <div className="modal-footer">{props.footer}</div>
          ) : (
            ""
          )}
        </Wrapper>
      </div>
    </div>
  );
  if (props.trigger) {
    return (
      <span>
        <ModalTrigger id={props.id} className={props.className}>
          {props.trigger}
        </ModalTrigger>
        {modal}
      </span>
    );
  }
  return modal;
};

class SlideEditorModal extends React.Component {
  onChange(slide) {
    jQuery("#modal-slide-editor").modal("hide");
    this.props.onChange(slide);
  }
  onDelete(slide) {
    jQuery("#modal-slide-editor").modal("hide");
    this.props.onDelete(slide);
  }
  render() {
    let slide = this.props.slides && this.props.slides[this.props.curSlideId];
    if (slide) {
      return (
        <Modal
          id="modal-slide-editor"
          className="btn btn-outline-primary nav-item"
          trigger={<Loading id={A.CHANGE_SLIDE_TYPE}>Edit this</Loading>}
        >
          <ErrorState id={A.CHANGE_SLIDE_TYPE} />
          <FormSlideEditor
            slide={slide}
            onChange={this.onChange.bind(this)}
            onDelete={this.onDelete.bind(this)}
            timeline={this.props.timeline}
          />
        </Modal>
      );
    }
    return null;
  }
}

class SlideAdderModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { slide: {} };
  }
  onChange(slide) {
    jQuery("#modal-slide-adder").modal("hide");
    this.props.onChange(slide);
    this.setState({ slide: {} });
  }
  render() {
    return (
      <Modal
        id="modal-slide-adder"
        className="btn btn-primary nav-item"
        trigger={<Loading id={A.ADD_SLIDE_TYPE}>Add My Story</Loading>}
      >
        <ErrorState id={A.ADD_SLIDE_TYPE} />
        <FormSlideEditor
          slide={this.state.slide}
          onChange={this.onChange.bind(this)}
          timeline={this.props.timeline}
        />
      </Modal>
    );
  }
}

class DeleteTimelineModal extends React.Component {
  onDelete() {
    this.props.deleteTimeline({
      timeline_id: this.props.timeline.secret_id
    });
  }
  componentWillUnmount() {
    jQuery("#delete-timeline").modal("hide");
  }
  render() {
    return (
      <Modal id="delete-timeline" title={`Delete ${this.props.timeline.name}`}>
        <ErrorState id={A.DELETE_TIMELINE_TYPE} />
        <p>
          Are you sure you want to delete {this.props.timeline.name} along with
          all of its stories?
        </p>
        <p>
          {this.props.timeline.slides.length} stories and{" "}
          {this.props.timeline.eras.length} eras will be deleted permanently.
          This can’t be undone.
        </p>
        <button className="btn btn-danger" onClick={this.onDelete.bind(this)}>
          <Loading id={A.DELETE_TIMELINE_TYPE}>
            Destroy {this.props.timeline.name}
          </Loading>
        </button>
      </Modal>
    );
  }
}
DeleteTimelineModal.propTypes = {
  timeline: React.PropTypes.object.isRequired,
  deleteTimeline: React.PropTypes.func.isRequired
};

class CategoryRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showPicker: false };
    this._collapseOnClickout = event => {
      const $el = jQuery(event.target);
      const hide =
        (this.state.showPicker && !$el.closest(this.rowEl).length) ||
        (!$el.is("swatch") && !$el.closest(".color-picker").length);
      if (hide) {
        this.setState({ showPicker: false });
      }
    };
  }
  componentWillMount() {
    jQuery(document).on("click", this._collapseOnClickout);
  }
  componentWillUnmount() {
    jQuery(document).off("click", this._collapseOnClickout);
  }
  render() {
    let showDelete =
      this.props.category !== "__default__" &&
      (!!this.props.category || !!this.props.color);
    return (
      <div className="row category-color-editor" ref={el => (this.rowEl = el)}>
        <div className="col-8">
          {this.props.category === "__default__" ? (
            <input
              className="form-control"
              readOnly
              value="Default color (no category)"
            />
          ) : (
            <input
              className="form-control"
              type="text"
              placeholder="Category name"
              value={this.props.category}
              onChange={e =>
                this.props.onChange(e.target.value, this.props.color)
              }
            />
          )}
        </div>
        <div className="col-2">
          <span
            className="swatch"
            style={{
              backgroundColor: this.props.color
            }}
            onClick={e => this.setState({ showPicker: !this.state.showPicker })}
          />
          <div className="color-picker-holder">
            <div className="color-picker">
              {this.state.showPicker ? (
                <TwitterPicker
                  colors={this.props.settings.CATEGORY_COLORS}
                  color={this.props.color}
                  triangle="top-right"
                  onChangeComplete={color => {
                    this.setState({ showPicker: false });
                    this.props.onChange(this.props.category, color.hex);
                  }}
                />
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
        <div className="col-2">
          {showDelete ? (
            <a
              href="#"
              className="btn btn-outline-secondary"
              title="Remove category"
              onClick={this.props.onRemove.bind(this)}
            >
              <i className="fa fa-trash" />
            </a>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

class CategoryEditorModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.propsToState(props);
  }
  componentWillReceiveProps(newProps) {
    this.setState(this.propsToState(newProps));
  }
  propsToState(props) {
    let rows = _.sortBy(
      _.map(props.timeline.categories, (color, name) => ({
        name: name,
        color: color,
        orig: name
      })),
      c => c.name.toLowerCase()
    );
    return { rows: rows };
  }
  updateRow(name, color, index) {
    this.setState({
      rows: [
        ...this.state.rows.slice(0, index),
        {
          name: name,
          color: color,
          orig: this.state.rows[index] && this.state.rows[index].orig
        },
        ...this.state.rows.slice(index + 1)
      ]
    });
  }
  removeRow(i, event) {
    event && event.preventDefault();
    this.setState({
      rows: this.state.rows.slice(0, i).concat(this.state.rows.slice(i + 1))
    });
  }
  onSubmit(event) {
    event && event.preventDefault();
    let update = [];
    this.state.rows.forEach(({ name, color, orig }) => {
      if (orig) {
        update.push({ op: "change", orig: orig, name: name, color: color });
      } else {
        update.push({ op: "add", name: name, color: color });
      }
    });
    this.props.onChange({
      timeline_id: this.props.timeline.secret_id,
      category_update: update
    });
    jQuery(`#${this.props.id}`).modal("hide");
  }
  render() {
    let rows = [...this.state.rows];
    // New empty category
    if (
      rows.length === 0 ||
      !(
        rows[rows.length - 1].name === "" &&
        rows[rows.length - 1].color === "" &&
        rows[rows.length - 1].orig === undefined
      )
    ) {
      rows.push({ name: "", color: "" });
    }
    return (
      <Modal
        id={this.props.id}
        title="Categories"
        onSubmit={this.onSubmit.bind(this)}
        footer={
          <button type="submit" className="btn btn-primary">
            Set Categories
          </button>
        }
      >
        <ErrorState id={A.CHANGE_CATEGORIES_TYPE} />
        <div className="row">
          <div className="col-8">Name</div>
          <div className="col-2">Color</div>
        </div>
        {rows.map((cat, i) => (
          <CategoryRow
            category={cat.name}
            color={cat.color}
            key={`cat-${i}`}
            settings={this.props.settings}
            onChange={(name, color) => this.updateRow(name, color, i)}
            onRemove={e => this.removeRow(i, e)}
          />
        ))}
      </Modal>
    );
  }
}

class EmbedCodeModal extends React.Component {
  render() {
    let url = `${document.location.protocol}//${document.location.host}/t/${this.props.timeline.secret_id}`;
    let embedCode = `<iframe src='${url}' style='width: 100%; height: 400px;'></iframe>`;
    let stats = this.props.permissionStats;
    return (
      <Modal id={this.props.id}>
        {!_.some([
          stats.publicIsViewer,
          stats.publicIsEditor,
          stats.publicIsOwner
        ]) ? (
          stats.isEditor || stats.isOwner ? (
            <div className="alert alert-danger">
              <Help id="embed-not-public-owner" />
            </div>
          ) : (
            <div className="alert alert-info">
              <Help id="embed-not-public-editor" />
            </div>
          )
        ) : null}
        <p>
          <Help id="embed-paste-code" />
        </p>
        <textarea
          className="form-control"
          rows={4}
          readOnly
          value={embedCode}
        />
      </Modal>
    );
  }
}

class PermissionsModal extends React.Component {
  render() {
    return (
      <Modal
        id="timeline-permissions-modal"
        title={`${this.props.timeline.name} permissions`}
      >
        <Permissions type="timelines" id={this.props.id} />
      </Modal>
    );
  }
}
PermissionsModal.propTypes = {
  id: React.PropTypes.string.isRequired,
  timeline: React.PropTypes.object.isRequired
};

class TimelineEditBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showNav: false };
    this._collapseOnClickout = event => {
      if (this.state.showNav) {
        if (
          !jQuery(event.target).is(".timeline-edit-nav-toggler, .brand") &&
          !jQuery(event.target).closest("#timeline-bar-nav-drawer").length
        ) {
          this.setState({ showNav: false });
        }
      }
    };
  }
  changeSlide(slide) {
    this.props.changeSlide({
      timeline_id: this.props.timeline.secret_id,
      slide: slide
    });
  }
  deleteSlide(slide) {
    this.props.deleteSlide({
      timeline_id: this.props.timeline.secret_id,
      slide_id: slide.id
    });
  }
  componentWillMount() {
    document.title = this.props.timeline.name;
    jQuery(document).on("click", this._collapseOnClickout);
  }
  componentWillUnmount() {
    jQuery(document).off("click", this._collapseOnClickout);
  }
  componentWillReceiveProps(newProps) {
    document.title = newProps.timeline.name;
  }

  addSlide(slide) {
    this.props.addSlide({
      timeline_id: this.props.timeline.secret_id,
      slide: slide
    });
  }

  modifyName(name) {
    this.props.changeTimeline({
      timeline_id: this.props.timeline.secret_id,
      name: name
    });
    document.title = name;
  }

  togglePublicView(event) {
    event && event.preventDefault();
    let timelineId = this.props.timeline.secret_id;
    let perms = this.props.permissions.timelines[timelineId];
    let update = {
      timeline_id: timelineId,
      public_can_view: !permutils.publicIsViewer(perms)
    };
    if (!update.public_can_view) {
      update.public_can_edit = false;
    }
    this.props.changePermissions(update);
  }
  togglePublicEdit(event) {
    event && event.preventDefault();
    let timelineId = this.props.timeline.secret_id;
    let perms = this.props.permissions.timelines[timelineId];
    let update = {
      timeline_id: timelineId,
      public_can_edit: !permutils.publicIsEditor(perms)
    };
    if (update.public_can_edit) {
      update.public_can_view = true;
    }
    this.props.changePermissions(update);
  }

  toggleNav(event) {
    event && event.preventDefault();
    this.setState({ showNav: !this.state.showNav });
  }

  toggleMode(event) {
    event && event.preventDefault();
    this.props.onChangeMode(
      this.props.mode === "spreadsheet" ? "viewer" : "spreadsheet"
    );
    this.setState({ showNav: false });
  }

  render() {
    let perms = this.props.permissions.timelines[this.props.timeline.secret_id];
    let isOwner = permutils.isOwner(perms, this.props.auth);
    let canEdit = permutils.isEditor(perms, this.props.auth);
    return (
      <div className={this.props.className}>
        <nav className="flex-navbar">
          <div className="subnav">
            <button
              className="timeline-edit-nav-toggler"
              type="button"
              aria-controls="editor-nav"
              aria-expanded={this.state.showNav ? "true" : "false"}
              aria-label="Toggle navigation"
              onClick={this.toggleNav.bind(this)}
            ></button>
            <InPlaceEditor
              editable={isOwner}
              allowEmpty={false}
              placeholder="Timeline name"
              value={this.props.timeline.name}
              onChange={this.modifyName.bind(this)}
            >
              <Loading id={A.CHANGE_TIMELINE_TYPE}>
                <h1
                  className="brand"
                  title={this.props.timeline.name}
                  onClick={isOwner ? undefined : this.toggleNav.bind(this)}
                >
                  {this.props.timeline.name}
                </h1>
              </Loading>
            </InPlaceEditor>
            <SearchWithinTimeline
              timeline={this.props.timeline}
              slides={this.props.slides}
              onGoTo={this.props.onGoTo.bind(this)}
            />
          </div>
          <div className="subnav">
            {canEdit && this.props.mode === "viewer" ? (
              <SlideAdderModal
                onChange={this.addSlide.bind(this)}
                timeline={this.props.timeline}
              />
            ) : (
              ""
            )}

            {canEdit && this.props.mode === "viewer" ? (
              <SlideEditorModal
                curSlideId={this.props.curSlideId}
                timeline={this.props.timeline}
                onChange={this.changeSlide.bind(this)}
                onDelete={this.deleteSlide.bind(this)}
                slides={this.props.slides}
              />
            ) : (
              ""
            )}
          </div>
        </nav>

        <div
          className={`nav-drawer${this.state.showNav ? " in" : ""}`}
          id="timeline-bar-nav-drawer"
        >
          <div className="header">
            <button
              className="timeline-edit-nav-toggler"
              type="button"
              aria-controls="editor-nav"
              aria-expanded={this.state.showNav ? "true" : "false"}
              aria-label="Toggle navigation"
              onClick={this.toggleNav.bind(this)}
            ></button>
            <h1 className="brand" onClick={this.toggleNav.bind(this)}>
              {this.props.timeline.name.replace(/^Timeline: /, "")}
            </h1>
          </div>
          <div className="body">
            <ModalTrigger id="timeline-permissions-modal" className="nav-link">
              <PermissionsCompact
                type="timelines"
                id={this.props.timeline.secret_id}
              />
            </ModalTrigger>
            {canEdit ? (
              <a
                className="nav-link"
                href="#"
                onClick={this.toggleMode.bind(this)}
              >
                {this.props.mode === "spreadsheet" ? (
                  <span>
                    <i className="fa fa-clock-o" /> Show as Timeline
                  </span>
                ) : (
                  <span>
                    <i className="fa fa-table" /> Show as Spreadsheet
                  </span>
                )}
              </a>
            ) : null}

            {canEdit ? (
              <ModalTrigger id="category-editor" className="nav-link">
                <Loading id={A.CHANGE_CATEGORIES_TYPE}>
                  <i className="fa fa-tags" /> Edit Categories
                </Loading>
              </ModalTrigger>
            ) : null}

            <a
              className="nav-link"
              href={`/t/${this.props.timeline.secret_id}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="fa fa-share-alt" /> Share
            </a>
            <ModalTrigger id="embed-code" className="nav-link">
              <span>
                <i className="fa fa-code" /> Embed
              </span>
            </ModalTrigger>

            <div className="nav-link">
              <i className="fa fa-download" />
              Download:{" "}
              <a href={`/export/${this.props.timeline.secret_id}.csv`} download>
                csv
              </a>
              ,{" "}
              <a
                href={`/export/${this.props.timeline.secret_id}.xlsx`}
                download
              >
                excel
              </a>
            </div>

            {isOwner ? (
              <ModalTrigger id="delete-timeline" className="nav-link">
                <span>
                  <i className="fa fa-trash" /> Delete timeline
                </span>
              </ModalTrigger>
            ) : null}

            <hr />

            <Link className="nav-link" to="/">
              <i className="fa fa-home" /> Home
            </Link>

            {this.props.auth.is_authenticated ? (
              <a className="nav-link js-logout" href="/accounts/logout/">
                Logout
              </a>
            ) : (
              ""
            )}
            {!this.props.auth.is_authenticated ? (
              <a
                className="nav-link"
                href={`/accounts/login/?next=${encodeURIComponent(
                  document.location.pathname
                )}`}
              >
                Login
              </a>
            ) : (
              ""
            )}
            {!this.props.auth.is_authenticated ? (
              <a
                className="nav-link"
                href={`/accounts/signup/?next=${encodeURIComponent(
                  document.location.pathname
                )}`}
              >
                Register
              </a>
            ) : (
              ""
            )}
          </div>
        </div>
        <EmbedCodeModal
          id="embed-code"
          timeline={this.props.timeline}
          permissionStats={getPermissionStats(perms, this.props.auth)}
        />
        <PermissionsModal
          id={this.props.timeline.secret_id}
          timeline={this.props.timeline}
        />
        <CategoryEditorModal
          timeline={this.props.timeline}
          settings={this.props.settings}
          onChange={this.props.changeCategories.bind(this)}
          id="category-editor"
        />
        <DeleteTimelineModal
          timeline={this.props.timeline}
          deleteTimeline={this.props.deleteTimeline}
          router={this.props.router}
        />
      </div>
    );
  }
}
TimelineEditBar.propTypes = {
  // connect
  addSlide: React.PropTypes.func.isRequired,
  changeSlide: React.PropTypes.func.isRequired,
  deleteSlide: React.PropTypes.func.isRequired,
  changePermissions: React.PropTypes.func.isRequired,
  changeCategories: React.PropTypes.func.isRequired,
  changeTimeline: React.PropTypes.func.isRequired,
  deleteTimeline: React.PropTypes.func.isRequired,
  settings: React.PropTypes.object,
  router: React.PropTypes.object,

  // TimelineProvider
  auth: React.PropTypes.object.isRequired,
  eras: React.PropTypes.object.isRequired,
  permissions: React.PropTypes.object.isRequired,
  slides: React.PropTypes.object.isRequired,
  timeline: React.PropTypes.object.isRequired,

  // caller
  className: React.PropTypes.string.isRequired,
  curSlideId: React.PropTypes.string,
  onGoTo: React.PropTypes.func.isRequired,
  mode: React.PropTypes.oneOf(["spreadsheet", "viewer"]).isRequired,
  onChangeMode: React.PropTypes.func.isRequired
};
export default connect(
  state => ({
    settings: state.settings
  }),
  dispatch => ({
    addSlide: payload => dispatch(A.addSlide(payload)),
    changeSlide: payload => dispatch(A.changeSlide(payload)),
    deleteSlide: payload => dispatch(A.deleteSlide(payload)),
    changePermissions: payload => dispatch(A.changePermissions(payload)),
    changeCategories: payload => dispatch(A.changeCategories(payload)),
    changeTimeline: payload => dispatch(A.changeTimeline(payload)),
    deleteTimeline: payload => dispatch(A.deleteTimeline(payload))
  })
)(TimelineEditBar);
