import React from "react";
import { connect } from "react-redux";

class LoginRequired extends React.Component {
  componentWillReceiveProps(newProps) {
    this.requireAuth(newProps);
  }
  componentWillMount() {
    this.requireAuth(this.props);
  }
  requireAuth(props) {
    if (!props.auth.is_authenticated) {
      document.location.href = `/accounts/login/?next=${encodeURIComponent(
        document.location.pathname
      )}`;
    }
  }
  render() {
    return <div>{this.props.children}</div>;
  }
}

export default connect(state => ({ auth: state.auth }))(LoginRequired);
