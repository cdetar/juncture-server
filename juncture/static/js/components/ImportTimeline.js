import React from "react";
import { connect } from "react-redux";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import LoginRequired from "./LoginRequired";
import Navbar from "./Navbar";
import RedirectReceiver from "./RedirectReceiver";
import * as A from "../store/actions";

const TIMELINE_URL_PATTERNS = [
  /docs\.google\.com\/spreadsheet\/.*\?.*key=([^&#]+)/i,
  /docs\.google\.com\/spreadsheets?\/d\/([^/]+)\/edit/i,
  /drive\.google\.com\/previewtemplate\/?\?.*id=([^&#]+)/i,
  /yourcelf\.github\.io\/TimelineJS-editor\/?\?.*timeline=([^&#]+)/i,
  /^([A-Za-z0-9-]{24,})$/
];

class ImportTimeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      error: "",
      id: ""
    };
  }

  componentWillMount() {
    if (
      !this.props.goog.loaded &&
      !this.props.loading[A.SET_GAPI_LOADED_TYPE]
    ) {
      this.props.loadGapi(this.props.settings.PUBLIC_API_KEYS.GOOGLE_CLIENT_ID);
    }
    document.title = "Import timeline";
  }

  onChange(event) {
    let val = event.target.value;
    for (let i = 0; i < TIMELINE_URL_PATTERNS.length; i++) {
      let match = TIMELINE_URL_PATTERNS[i].exec(val);
      if (match && match[1]) {
        this.setState({
          url: val,
          id: match[1],
          error: ""
        });
        return;
      }
    }
    this.setState({
      url: val,
      error: "Timeline URL not recognized."
    });
  }

  onSubmitGoogleSpreadsheet(event) {
    event && event.preventDefault();
    if (this.state.id) {
      this.props.importTimeline({
        id: this.state.id,
        ignore_errors: !!this.state.ignoreErrors
      });
    }
  }

  onFileUpload(event) {
    let input = event.target;
    let data = new FormData();
    data.append("file", input.files[0]);
    data.append("file_name", input.files[0].name);
    data.append("file_type", input.files[0].type);

    this.props.uploadTimeline(data);
  }

  render() {
    return (
      <LoginRequired>
        <RedirectReceiver router={this.props.router} />
        <Navbar />
        <div className="import-timeline container">
          <h1>Import a timeline</h1>

          <h3>Google Spreadsheet</h3>
          <div>
            <Loading id={A.SET_GAPI_LOADED_TYPE}>
              <Loading id={A.SET_GAPI_AUTH_TYPE}>
                <Loading id={A.IMPORT_TIMELINE_TYPE}>
                  <p>
                    Import a TimelineJS-compatible timeline from a Google
                    Spreadsheet or the old TimelineJS-editor.
                  </p>
                  {this.props.goog.authed ? (
                    <div>
                      <form
                        onSubmit={this.onSubmitGoogleSpreadsheet.bind(this)}
                      >
                        <div
                          className={
                            "form-group" +
                            (this.state.error ? " has-danger" : "")
                          }
                        >
                          <label className="form-control-label">
                            Timeline to import
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            value={this.state.url}
                            onChange={this.onChange.bind(this)}
                            placeholder="Spreadsheet or timeline URL"
                          />
                          {this.state.error ? (
                            <div className="form-control-feedback">
                              {this.state.error}
                            </div>
                          ) : (
                            ""
                          )}
                          <div className="form-text text-muted">
                            <small>
                              e.g.
                              https://docs.google.com/spreadsheet/ccc?key=1BBVcQmmNoxsPoEHrJZJOomorOIdaSDN-09nTZtEknyk&mode=public
                            </small>
                          </div>
                        </div>
                        <div className="form-check">
                          <label className="form-check-label">
                            <input
                              type="checkbox"
                              className="form-check-input"
                              checked={this.state.ignoreErrors || false}
                              onChange={e =>
                                this.setState({
                                  ignoreErrors: e.target.checked
                                })
                              }
                            />{" "}
                            Skip any rows that have errors
                          </label>
                        </div>
                        <ErrorState id={A.IMPORT_TIMELINE_TYPE} />
                        <button
                          type="submit"
                          className="btn btn-primary"
                          disabled={!this.state.id || !!this.state.error}
                        >
                          Import
                        </button>
                      </form>
                      <button
                        className="btn btn-default pull-right"
                        onClick={e => this.props.gapiSignout(e)}
                      >
                        Unauthorize Google
                      </button>
                    </div>
                  ) : (
                    <div>
                      <ErrorState id={A.SET_GAPI_LOADED_TYPE} />
                      <ErrorState id={A.SET_GAPI_AUTH_TYPE} />
                      <p>
                        Please authorize Google to import google spreadsheets.
                      </p>
                      <button
                        className="btn btn-primary"
                        onClick={e => this.props.gapiAuth(e)}
                      >
                        Authorize Google
                      </button>
                    </div>
                  )}
                </Loading>
              </Loading>
            </Loading>
          </div>

          <h3 className="mt-5">Upload File</h3>
          <div>
            <p>Upload a csv or xlsx file.</p>
            <ErrorState id={A.UPLOAD_TIMELINE_TYPE} />
            <Loading id={A.UPLOAD_TIMELINE_TYPE}>
              <form>
                <input
                  type="file"
                  accept="text/csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                  onChange={this.onFileUpload.bind(this)}
                />
              </form>
            </Loading>
          </div>
        </div>
      </LoginRequired>
    );
  }
}
export default connect(
  state => ({
    settings: state.settings,
    goog: state.goog,
    loading: state.loading
  }),
  dispatch => ({
    importTimeline: payload => dispatch(A.importTimeline(payload)),
    uploadTimeline: payload => dispatch(A.uploadTimeline(payload)),
    loadGapi: payload => dispatch(A.loadGapi(payload)),
    gapiAuth: payload => dispatch(A.gapiAuth(payload)),
    gapiSignout: payload => dispatch(A.gapiSignout(payload))
  })
)(ImportTimeline);
