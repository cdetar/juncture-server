import React from "react";
import { connect } from "react-redux";

const cssClassMap = {
  [WebSocket.CONNECTING]: "conecting",
  [WebSocket.OPEN]: "open",
  [WebSocket.CLOSING]: "closing",
  [WebSocket.CLOSED]: "closed"
};

class ConnectionStatus extends React.Component {
  render() {
    let classes = ["connection"];
    if (cssClassMap[this.props.socket.state]) {
      classes.push(cssClassMap[this.props.socket.state]);
    }
    return (
      <div className={classes.join(" ")}>
        <div className="message">
          {this.props.socket.state === WebSocket.CONNECTING
            ? this.renderConnecting()
            : this.props.socket.state === WebSocket.CLOSING
            ? this.renderClosing()
            : this.props.socket.state === WebSocket.CLOSED
            ? this.renderClosed()
            : this.props.socket.state === WebSocket.OPEN
            ? this.renderOpen()
            : ""}
        </div>
      </div>
    );
  }

  renderOpen() {
    return <span>Connected!</span>;
  }

  renderConnecting() {
    return <span>Connecting...</span>;
  }

  renderClosing() {
    return <span>Connection lost...</span>;
  }
  renderClosed() {
    return <span>Trying to connect...</span>;
  }
}

export default connect(
  state => ({ socket: state.socket }),
  (dispatch, ownProps) => ({})
)(ConnectionStatus);
