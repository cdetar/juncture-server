import React from "react";
import Linkify from "react-linkify";
import { connect } from "react-redux";

const defaults = {
  // embeds
  "embed-not-public-owner": `This timeline will only be viewable by some logged in users. To ensure anyone can view, set the permissions to add "Public" as a viewer.`,
  "embed-not-public-editor": `This timeline is only viewable by some logged in users. Ask the timeline owner to add "Public" as a viewer if you want regular visitors of your website or blog to be able to see it.`,
  "embed-paste-code":
    "Paste this code into your website or blog to embed a timeline.",
  // url or image upload
  "not-image-file": "That doesn't seem to be an image file.",
  "not-image-url": "Hmmm, that doesn't look like an image.",
  "not-valid-url":
    "Hmmm, that doesn't look like a URL. It should start with 'http:' or 'https:'.",
  "not-valid-url-or-image":
    "Hmmm, that doesn't look like a valid URL or image.",
  "images-https-only": "Images must use 'https:' links to work correctly.",

  // Slide editor
  "media-credit-help": "Credits for the content in Media URL. (optional)",
  "media-caption-help": "Caption to display under media. (optional)",
  "media-thumbnail-help":
    "Enter image URL to add a thumbnail image for the story marker. If no image is added, an automatic icon will be used."
};

class Help extends React.Component {
  render() {
    let adminLink;
    let text;
    if (this.props.help[this.props.id]) {
      text = this.props.help[this.props.id];
      adminLink = `/admin/helpdocs/helptext/${this.props.id}/change/`;
    } else {
      text = defaults[this.props.id] || "";
      adminLink = `/admin/helpdocs/helptext/add/?key=${encodeURIComponent(
        this.props.id
      )}&text=${encodeURIComponent(text)}`;
    }
    return (
      <span className={`helpdoc${this.props.auth.is_staff ? " editable" : ""}`}>
        <Linkify properties={{ target: "_blank", rel: "noopener noreferrer" }}>
          {text}
        </Linkify>
        {this.props.auth.is_staff ? (
          <a
            className="admin-edit"
            href={adminLink}
            title={this.props.id}
            target="_blank"
            rel="noopener noreferrer"
          >
            <i className="fa fa-edit" />
          </a>
        ) : null}
      </span>
    );
  }
}
Help.propTypes = {
  help: React.PropTypes.object.isRequired,
  id: React.PropTypes.string.isRequired,
  auth: React.PropTypes.object.isRequired
};

export default connect(state => ({
  help: state.help,
  auth: state.auth
}))(Help);
