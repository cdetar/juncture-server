import React from "react";
import _ from "lodash";
import $ from "jquery";
import moment from "moment";
import Loading from "./Loading";
import WhoSelector from "./WhoSelector";
import { whoKey, Who } from "./Who";
import { connect } from "react-redux";
import * as permutils from "../utils/permutils";
import * as A from "../store/actions";

/**
 * Return a map of various stats gleaned from the permissions for an object and
 * the auth state, including whether the authenticated user is an owner,
 * editor, or viewer, and how many of those various roles the object has.
 */
export const getPermissionStats = (perms, auth) => {
  const stats = {
    publicIsOwner: permutils.publicIsOwner(perms),
    publicIsViewer: permutils.publicIsViewer(perms),
    publicIsEditor: permutils.publicIsEditor(perms),
    isOwner: permutils.isOwner(perms, auth),
    isEditor: permutils.isEditor(perms, auth),
    isViewer: permutils.isViewer(perms, auth),
    owningUsers: _.filter(perms.owners, who => !!who.user),
    owningGroups: _.filter(perms.owners, who => !!who.group),
    editingUsers: _.filter(perms.editors, who => !!who.user),
    editingGroups: _.filter(perms.editors, who => !!who.group),
    viewingUsers: _.filter(perms.viewers, who => !!who.user),
    viewingGroups: _.filter(perms.viewers, who => !!who.group)
  };
  const countGroupMembers = who =>
    who.group.owners.length + who.group.members.length;
  stats.numOwners =
    stats.owningUsers.length +
    _.sum(_.map(stats.owningGroups, countGroupMembers));
  stats.numEditors =
    stats.numOwners +
    stats.editingUsers.length +
    _.sum(_.map(stats.editingGroups, countGroupMembers));
  stats.numViewers =
    stats.numEditors +
    stats.viewingUsers.length +
    _.sum(_.map(stats.viewingGroups, countGroupMembers));
  return stats;
};

/**
 * Interface for editing the full permissions profile for an object. Who is in
 * which role, for how long.
 */
class WhoEditor extends React.Component {
  onRemove(event) {
    event && event.preventDefault();
    this.props.onRemove(this.props.who);
  }
  render() {
    const exp = moment(this.props.who.expiration);
    const isForever = permutils.isForever(exp);
    const dateChoices = [
      ["1 day", moment().add(1, "days")],
      ["7 days", moment().add(7, "days")],
      ["2 weeks", moment().add(14, "days")],
      ["3 months", moment().add(90, "days")]
    ];
    return (
      <div className="who-edit-row form-inline">
        <WhoSelector
          who={this.props.who}
          onChange={who =>
            this.props.onChange({
              who: { ...who, expiration: this.props.who.expiration },
              role: this.props.role
            })
          }
        />
        <select
          className="form-control"
          value={this.props.role}
          onChange={event =>
            this.props.onChange({
              who: { ...this.props.who },
              role: event.target.value
            })
          }
        >
          <option value="owners">owns this</option>
          <option value="editors">can edit this</option>
          <option value="viewers">can view this</option>
        </select>
        <div> until </div>
        <select
          className="form-control who-expiration"
          value={this.props.who.expiration}
          onChange={event =>
            this.props.onChange({
              who: { ...this.props.who, expiration: event.target.value },
              role: this.props.role
            })
          }
        >
          <option value={this.props.who.expiration}>
            {isForever ? "Forever" : exp.format("lll")}
          </option>
          {!isForever ? (
            <option value={permutils.NO_EXPIRATION}>Forever</option>
          ) : (
            ""
          )}
          {dateChoices.map(([label, date]) => (
            <option value={date.format()} key={label}>
              {label} ({date.format("lll")})
            </option>
          ))}
        </select>
        <a
          href="#"
          onClick={this.onRemove.bind(this)}
          className="btn btn-outline-danger"
        >
          Remove
        </a>
      </div>
    );
  }
}
WhoEditor.propTypes = {
  who: React.PropTypes.object.isRequired,
  role: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
  onRemove: React.PropTypes.func.isRequired
};

class WhoReader extends React.Component {
  render() {
    return (
      <div className="who-reader-row">
        <Who who={this.props.who} />
        <span>
          {this.props.role === "owners"
            ? "owner"
            : this.props.role === "editors"
            ? "editor"
            : "viewer"}
        </span>
        <span>
          {permutils.isForever(this.props.who.expiration)
            ? "forever"
            : `${moment(this.props.who.expiration).format("lll")}`}
        </span>
      </div>
    );
  }
}
WhoReader.propTypes = {
  who: React.PropTypes.object.isRequired,
  role: React.PropTypes.string.isRequired
};

const permissionsMapStateToProps = (state, ownProps) => {
  const perms = _.get(state, ["permissions", ownProps.type, ownProps.id]);
  return {
    perms: perms,
    auth: state.auth,
    stats: perms && getPermissionStats(perms, state.auth)
  };
};

/**
 * Compact view of permissions for an object suitable for display in menus,
 * etc.
 */
class RawPermissionsCompact extends React.Component {
  renderCapabilitiesPopover(role, text) {
    const caplist = permutils.capabilities[this.props.type][role]
      .map(cap => `<li>${cap}</li>`)
      .join("\n");
    const capabilitiesContent = `<div>${_.capitalize(
      role
    )} can: <ul>${caplist}</ul></div>`;
    return (
      <span
        className="popover-underline"
        tabIndex="0"
        data-toggle="popover"
        data-trigger="hover"
        data-placement="right"
        data-html="true"
        data-content={capabilitiesContent}
      >
        {text}
      </span>
    );
  }

  componentDidUpdate() {
    $('[data-toggle="popover"]').popover();
  }

  render() {
    const { stats, perms, onClick } = this.props;
    if (perms === undefined) {
      return <Loading />;
    }
    return (
      <div className="permissions compact" onClick={onClick}>
        <div className="headline">
          {stats.publicIsViewer ? (
            <span>
              <i className="fa fa-globe" />
            </span>
          ) : stats.numViewers > 1 ? (
            <span>
              <i className="fa fa-users" />
            </span>
          ) : stats.numViewers === 1 ? (
            <span>
              <i className="fa fa-user" />
            </span>
          ) : (
            <span>
              <i className="fa fa-user-times" />
              {stats.numViewers}
            </span>
          )}{" "}
          {stats.publicIsOwner
            ? "No restrictions"
            : stats.publicIsEditor || stats.publicIsViewer
            ? "Public"
            : "Private"}
        </div>
        <div className="subheadline">
          {stats.publicIsOwner ? (
            <div>
              <i className="fa fa-globe" /> Public{" "}
              {this.renderCapabilitiesPopover("owners", "owns")},{" "}
              {this.renderCapabilitiesPopover("editors", "can edit")}, and{" "}
              {this.renderCapabilitiesPopover("viewers", "can view")} this.
            </div>
          ) : (
            <div>
              <i className="fa fa-certificate" /> {stats.numOwners}{" "}
              {this.renderCapabilitiesPopover(
                "owners",
                `own${stats.numOwners === 1 ? "s" : ""}`
              )}{" "}
              this.
            </div>
          )}
          {!stats.publicIsOwner ? (
            stats.publicIsEditor ? (
              <div>
                <i className="fa fa-pencil" /> Anyone{" "}
                {this.renderCapabilitiesPopover("editors", "can edit")} or{" "}
                {this.renderCapabilitiesPopover("viewers", "view")}.
              </div>
            ) : (
              <div>
                <i className="fa fa-pencil" /> {stats.numEditors}{" "}
                {this.renderCapabilitiesPopover("editors", "can edit")}.
              </div>
            )
          ) : null}
          {!stats.publicIsOwner && !stats.publicIsEditor ? (
            stats.publicIsViewer ? (
              <div>
                <i className="fa fa-eye" /> Anyone{" "}
                {this.renderCapabilitiesPopover("viewers", "can view")}.
              </div>
            ) : (
              <div>
                <i className="fa fa-eye" /> {stats.numViewers}{" "}
                {this.renderCapabilitiesPopover("viewers", "can view")}.
              </div>
            )
          ) : null}
          <div className="you">
            {!stats.publicIsOwner ? (
              <span>
                You{" "}
                {stats.isOwner
                  ? "own"
                  : stats.isEditor
                  ? "can edit"
                  : stats.isViewer
                  ? "can view"
                  : "can't view"}{" "}
                this.
              </span>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
RawPermissionsCompact.propTypes = {
  // connect
  auth: React.PropTypes.object.isRequired,
  stats: React.PropTypes.object.isRequired,
  perms: React.PropTypes.object,

  // caller
  type: React.PropTypes.string.isRequired,
  id: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]),
  onClick: React.PropTypes.func
};
export const PermissionsCompact = connect(permissionsMapStateToProps)(
  RawPermissionsCompact
);

/**
 * Editor for full permissions.
 */
class RawPermissions extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.propsToState(props);
  }
  propsToState(props) {
    return {
      whoRoles: [
        ...props.perms.owners.map(who => ({ who, role: "owners" })),
        ...props.perms.editors.map(who => ({ who, role: "editors" })),
        ...props.perms.viewers.map(who => ({ who, role: "viewers" }))
      ],
      frozen: true
    };
  }

  componentWillReceiveProps(newProps) {
    if (!this.state.frozen) {
      this.setState(this.propsToState(newProps));
    }
  }

  componentWillMount() {
    this.props.fetchGroups();
  }

  componentDidUpdate() {
    $('[data-toggle="popover"]').popover();
  }

  onAddEmpty(event) {
    event && event.preventDefault();
    this.setState({
      whoRoles: [
        ...this.state.whoRoles,
        {
          who: { invitee: "", expiration: permutils.NO_EXPIRATION },
          role: "viewers"
        }
      ]
    });
  }

  onChangeWho(who, role, index) {
    this.setState({
      whoRoles: [
        ...this.state.whoRoles.slice(0, index),
        { who: who, role: role },
        ...this.state.whoRoles.slice(index + 1)
      ]
    });
  }

  onRemoveWho(index) {
    this.setState({
      whoRoles: [
        ...this.state.whoRoles.slice(0, index),
        ...this.state.whoRoles.slice(index + 1)
      ]
    });
  }

  onSave(event) {
    event && event.preventDefault();
    let update = {
      owners: [],
      editors: [],
      viewers: [],
      timeline_id: this.props.id
    };
    this.state.whoRoles.forEach(({ role, who }) => {
      if (_.some([who.user, who.group, who.invitee, who["public"]])) {
        update[role].push(who);
      }
    });
    this.setState({ frozen: false });
    this.props.changePermissions(update);
  }

  render() {
    if (this.props.perms === undefined) {
      return <Loading />;
    }
    return (
      <div className="permissions full">
        <PermissionsCompact {...this.props} />
        <hr />
        <h4>
          {this.props.stats.isOwner ? "Change permissions" : "Permissions"}
        </h4>
        <form
          onSubmit={this.onSave.bind(this)}
          className={`roles ${
            this.props.stats.isOwner ? "editable" : "readonly"
          }`}
        >
          {this.props.stats.isOwner ? null : (
            <div className="who-reader-row">
              <div>
                <b>Who</b>
              </div>
              <div>
                <b>Role</b>
              </div>
              <div>
                <b>Until</b>
              </div>
            </div>
          )}
          {this.state.whoRoles.map(({ who, role }, index) =>
            this.props.stats.isOwner ? (
              <WhoEditor
                who={who}
                role={role}
                key={whoKey(who) + index}
                onChange={({ who, role }) => this.onChangeWho(who, role, index)}
                onRemove={() => this.onRemoveWho(index)}
              />
            ) : (
              <WhoReader who={who} role={role} key={whoKey(who) + index} />
            )
          )}
          {this.props.stats.isOwner ? (
            <div>
              <a
                href="#"
                className="btn btn-default"
                onClick={this.onAddEmpty.bind(this)}
              >
                Add user or group
              </a>
            </div>
          ) : null}
          {this.props.stats.isOwner ? (
            <div>
              <button type="submit" className="btn btn-primary">
                <Loading id={A.CHANGE_PERMISSIONS_TYPE}>
                  <span>Save</span>
                </Loading>
              </button>
            </div>
          ) : null}
        </form>
      </div>
    );
  }
}
RawPermissions.propTypes = {
  // connect
  auth: React.PropTypes.object.isRequired,
  stats: React.PropTypes.object.isRequired,
  perms: React.PropTypes.object,
  fetchGroups: React.PropTypes.func,
  changePermissions: React.PropTypes.func,

  // caller
  type: React.PropTypes.string.isRequired,
  id: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]),
  onClick: React.PropTypes.func
};
export const Permissions = connect(permissionsMapStateToProps, dispatch => ({
  fetchGroups: payload => dispatch(A.fetchGroups(payload)),
  changePermissions: payload => dispatch(A.changePermissions(payload))
}))(RawPermissions);
