import React from "react";
import { connect } from "react-redux";
import { BlockPicker } from "react-color";
import _ from "lodash";
import jQuery from "jquery";
import Help from "./Help";
import UrlOrImageUpload from "./UrlOrImageUpload";
import VariablePrecisionDate from "./VariablePrecisionDate";
import { timelineDateToJsDate } from "../utils/dates";

const FormGroup = props => (
  <div className="form-group row">
    <label className="form-control-label col-sm-3">{props.label}</label>
    <div className={`col-sm-9${props.error ? " has-danger" : ""}`}>
      {props.error ? (
        <div className="form-control-feedback">{props.error}</div>
      ) : (
        ""
      )}
      {props.children}
    </div>
  </div>
);

const CategoryInput = props => {
  let choices;
  if (props.choices) {
    choices = _.sortBy(props.choices, c => c.toLowerCase()).map(choice => {
      if (choice === "__default__") {
        return { name: "----", value: "" };
      } else {
        return { name: choice, value: choice };
      }
    });
  }
  return choices ? (
    <select
      className={props.className}
      value={props.value}
      onChange={props.onChange}
    >
      {choices.map(choice => (
        <option value={choice.value} key={choice.value}>
          {choice.name}
        </option>
      ))}
    </select>
  ) : (
    <input
      type="text"
      className={props.className}
      value={props.value}
      onChange={props.onChange}
      placeholder={props.placeholder}
    />
  );
};

class BackgroundInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.stateFromProps(props);
  }

  componentWillReceiveProps(newProps) {
    this.setState(this.stateFromProps(newProps));
  }

  stateFromProps(props) {
    const val = props.value || {};
    let mode = val.url ? "image" : val.color ? "color" : "clear";
    return { mode: mode };
  }

  changeMode(mode) {
    this.setState({ mode });
    if (mode === "clear") {
      this.update("clear", "", "");
    }
  }

  update(key, value, error) {
    let update = { url: "", color: "" };
    if (key === "image") {
      if (value === "") {
        return;
      }
      update.url = value;
    } else if (key === "color") {
      update.color = value;
    }
    this.props.onChange({ value: update, error: error });
  }

  render() {
    let classes = ["background-chooser"];
    if (this.props.error) {
      classes.push("has-danger");
    }
    return (
      <div className={classes.join(" ")}>
        <div className="form-check form-check-inline">
          {[
            ["clear", "Clear"],
            ["image", "Image"],
            ["color", "Color"]
          ].map(([key, name]) => (
            <label className="form-check-label mr-2" key={key}>
              <input
                className="form-check-input"
                type="radio"
                name="backgroundType"
                value={key}
                checked={this.state.mode === key}
                onChange={e => this.changeMode(e.target.value)}
              />{" "}
              {name}
            </label>
          ))}
        </div>
        {this.state.mode === "image" ? (
          <UrlOrImageUpload
            imgurApiKey={this.props.settings.PUBLIC_API_KEYS.IMGUR_API_KEY}
            className="form-control"
            placeholder="Background image"
            showHelp={false}
            showPreview={true}
            imageOnly={true}
            value={_.get(this.props.value, "url", "")}
            error={this.props.error}
            onChange={({ url, error }) => this.update("image", url, error)}
          />
        ) : this.state.mode === "color" ? (
          <BlockPicker
            triangle="hide"
            colors={this.props.settings.CATEGORY_COLORS}
            color={_.get(this.props.value, "color") || "#fff"}
            onChangeComplete={color => this.update("color", color.hex, "")}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}
BackgroundInput.propTypes = {
  settings: React.PropTypes.object.isRequired,
  onChange: React.PropTypes.func.isRequired,
  value: React.PropTypes.object.isRequired,
  error: React.PropTypes.string
};

class BackgroundInputCell extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this._collapseOnClickout = event => {
      if (this.state.open) {
        if (
          !jQuery(event.target).is(".toggle-background-input") &&
          !jQuery(event.target).closest(".background-input-cell").length
        ) {
          this.setState({ open: false });
        }
      }
    };
  }
  componentWillMount() {
    jQuery(document).on("click", this._collapseOnClickout);
  }
  componentWillUnmount() {
    jQuery(document).off("click", this._collapseOnClickout);
  }

  render() {
    let val = this.props.value.url || this.props.value.color || "";
    return (
      <div className="background-input-cell">
        <input
          readOnly
          className="form-control toggle-background-input"
          value={val}
          onChange={() => null}
          onClick={e => this.setState({ open: !this.state.open })}
        />
        <div className={`background-input${this.state.open ? "" : " hide"}`}>
          <div className="arrow-up" />
          <BackgroundInput
            settings={this.props.settings}
            value={this.props.value}
            error={this.props.error}
            onChange={this.props.onChange}
          />
        </div>
      </div>
    );
  }
}
BackgroundInputCell.propTypes = BackgroundInput.propTypes;

/**
 * A base react component for editing a complex object (like a slide or era).
 * Copies the object props into state, validates on change, but only fires
 * props.onChange if there are no errors.
 *
 * Properties required:
 *    this.objName - the name of the object (slide, era) given as props
 *    this.objNoun - a human readable name for the object
 *    this.fields  - an array of all field keys to represent on the object
 */
export class BaseObjEditor extends React.Component {
  getInitialState(props) {
    return {
      [this.objName]: { ...props[this.objName] },
      errors: {}
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps[this.objName].id !== this.props[this.objName].id) {
      return this.setState({ [this.objName]: { ...newProps[this.objName] } });
    }
    // If we're set with an empty object, clear state. We must be an "adder".
    if (_.isEmpty(newProps[this.objName])) {
      return this.setState({ [this.objName]: {} });
    }
    // Merge in any properties of newProps that we haven't changed in 'state'.
    const newObj = {};
    this.fields.forEach(key => {
      let newPropVal = _.get(newProps[this.objName], key);
      let oldPropVal = _.get(this.props[this.objName], key);
      let oldStateVal = _.get(this.state[this.objName], key);
      if (oldPropVal === oldStateVal) {
        if (newPropVal !== undefined) {
          _.set(newObj, key, newPropVal);
        }
      } else if (oldStateVal !== undefined) {
        _.set(newObj, key, oldStateVal);
      }
    });
    this.setState({ [this.objName]: newObj });
  }

  updateObj(...params) {
    let obj = { ...this.state[this.objName] };
    let errors = { ...this.state.errors };
    _.chunk(params, 3).forEach(([statePath, val, error]) => {
      _.set(obj, statePath, val);
      _.set(errors, statePath, error);
    });
    return new Promise((resolve, reject) => {
      this.setState({ [this.objName]: obj, errors: errors }, resolve);
    });
  }

  updateAndGo(...params) {
    this.updateObj(...params).then(this.onSubmit.bind(this));
  }

  confirmDelete(event) {
    event && event.preventDefault();
    if (confirm(`Delete this ${this.objNoun}?`)) {
      this.props.onDelete(this.props[this.objName]);
    }
  }

  checkError(errors, err, errorKey, msg) {
    if (!errors[errorKey] && err) {
      errors[errorKey] = msg;
    } else if (errors[errorKey] === msg && !err) {
      errors[errorKey] = "";
    }
  }

  onSubmit(event) {
    event && event.preventDefault();
    let update = this.clean();
    this.setState({
      [this.objName]: update[this.objName],
      errors: update.errors
    });
    if (!update.hasError) {
      this.props.onChange(update[this.objName]);
    }
  }

  clean() {
    let obj = { ...this.state[this.objName] };
    let errors = { ...this.state.errors };

    this.errorChecks(obj, errors);

    return {
      [this.objName]: obj,
      errors: errors,
      hasError: this.findDeepTruthy(errors)
    };
  }

  findDeepTruthy(obj) {
    if (_.isObject(obj)) {
      let truthy = false;
      _.forEach(obj, (prop, key) => {
        if (this.findDeepTruthy(prop)) {
          truthy = true;
          return false; // cancel iteration; truthy is true
        }
      });
      return truthy;
    } else {
      return !!obj;
    }
  }
}

/**
 * Implementation of BaseObjEditor for slides.
 */
class BaseSlideEditor extends BaseObjEditor {
  constructor(props) {
    super(props);
    this.objName = "slide";
    this.objNoun = "Story";
    this.fields = [
      "id",
      "start_date.year",
      "start_date.month",
      "start_date.day",
      "end_date.year",
      "end_date.month",
      "end_date.day",
      "is_title",
      "text.headline",
      "text.text",
      "media.url",
      "media.credit",
      "media.caption",
      "media.thumbnail",
      "group",
      "background"
    ];
    this.state = this.getInitialState(props);
  }

  errorChecks(slide, errors) {
    const hasEnd = this.findDeepTruthy(slide.end_date);
    const hasStart = this.findDeepTruthy(slide.start_date);
    this.checkError(
      errors,
      hasEnd && !hasStart,
      "start_date",
      "Start date is required if end date is specified."
    );
    this.checkError(
      errors,
      hasStart && hasEnd && slide.end_date.month && !slide.start_date.month,
      "end_date",
      "Start date must have a month if end date has a month."
    );
    this.checkError(
      errors,
      hasStart && hasEnd && slide.end_date.day && !slide.end_date.month,
      "end_date",
      "Start date must have a day if end date has a day."
    );
    this.checkError(
      errors,
      hasStart &&
        hasEnd &&
        timelineDateToJsDate(slide.end_date) <
          timelineDateToJsDate(slide.start_date),
      "end_date",
      "End date must come after start date."
    );
    this.checkError(
      errors,
      !hasStart && !slide.is_title,
      "start_date",
      "Start date is required unless 'title' is checked."
    );
    this.checkError(
      errors,
      !_.get(slide, "text.headline") &&
        !_.get(slide, "text.text") &&
        !_.get(slide, "media.url"),
      "text.headline",
      "At least one of headline, text, or media url required"
    );
  }

  updateSlide(...params) {
    return this.updateObj(...params);
  }
}

/**
 * Implementation of BaseSlideEditor for modal forms.
 */
class FormSlideEditor extends BaseSlideEditor {
  inputProps(path, placeholder, defaultValue = "") {
    return {
      className: "form-control",
      placeholder: placeholder,
      value: _.get(this.state.slide, path, defaultValue),
      onChange: e => this.updateSlide(path, e.target.value)
    };
  }

  render() {
    return (
      <form
        className="form-horizontal form-slide-editor"
        onSubmit={this.onSubmit.bind(this)}
      >
        <div>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <FormGroup label="Dates">
          {this.state.slide.is_title ? (
            ""
          ) : (
            <VariablePrecisionDate
              label="Start"
              value={this.state.slide.start_date || {}}
              error={this.state.errors.start_date || ""}
              onChange={({ date, error }) =>
                this.updateSlide("start_date", date, error)
              }
            />
          )}
          {this.state.slide.is_title ? (
            ""
          ) : (
            <VariablePrecisionDate
              label="End"
              value={this.state.slide.end_date || {}}
              error={this.state.errors.end_date || ""}
              onChange={({ date, error }) =>
                this.updateSlide("end_date", date, error)
              }
            />
          )}
          <div className="row">
            {this.state.slide.is_title ? (
              ""
            ) : (
              <label className="form-control-label col-1">or</label>
            )}
            <div className="form-check col-11">
              <label className="form-check-label ">
                <input
                  className="form-check-input"
                  type="checkbox"
                  checked={this.state.slide.is_title || false}
                  onChange={e => {
                    // clear dates
                    this.updateSlide(
                      "start_date",
                      {},
                      null,
                      "end_date",
                      {},
                      null,
                      "is_title",
                      e.target.checked,
                      null
                    );
                  }}
                />{" "}
                Use this as as this timeline’s title story
              </label>
            </div>
          </div>
        </FormGroup>

        <FormGroup
          label="Headline"
          error={_.get(this.state.errors, "text.headline")}
        >
          <input {...this.inputProps("text.headline", "Headline")} />
        </FormGroup>
        <FormGroup label="Text" error={_.get(this.state.errors, "text.text")}>
          <textarea {...this.inputProps("text.text", "More details")} />
        </FormGroup>

        <FormGroup label="Media URL">
          <div className="form-text">
            Any link to:
            <i className="fa fa-fw fa-image" title="Images, gifs, pngs, jpgs" />
            <i className="fa fa-fw fa-flickr" title="Flickr" />
            <i className="fa fa-fw fa-instagram" title="Instagram" />
            <i className="fa fa-fw fa-twitter" title="Twitter" />
            <i className="fa fa-fw fa-vine" title="Vine" />
            <i className="fa fa-fw fa-youtube" title="Youtube" />
            <i className="fa fa-fw fa-vimeo-square" title="Vimeo" />
            <i className="fa fa-fw fa-map-marker" title="Google Maps" />
            <i className="fa fa-fw fa-google-plus" title="Google+" />
            <i className="fa fa-fw fa-file-excel-o" title="Google Docs" />
            <i className="fa fa-fw fa-soundcloud" title="Soundcloud" />
            <i className="fa fa-fw fa-globe" title="The Internets" />
          </div>
          <UrlOrImageUpload
            imgurApiKey={this.props.settings.PUBLIC_API_KEYS.IMGUR_API_KEY}
            className="form-control"
            placeholder="Media URL"
            value={_.get(this.state.slide, "media.url", "")}
            error={_.get(this.state.errors, "media.url", "")}
            onChange={({ url, error }) =>
              this.updateSlide("media.url", url, error)
            }
          />
        </FormGroup>
        <FormGroup label="Media credit">
          <input {...this.inputProps("media.credit", "Media Credit")} />
          <small className="form-text text-muted">
            <Help id="media-credit-help" />
          </small>
        </FormGroup>
        <FormGroup label="Caption">
          <input {...this.inputProps("media.caption", "Caption")} />
          <small className="form-text text-muted">
            <Help id="media-caption-help" />
          </small>
        </FormGroup>

        {this.state.slide.is_title ? (
          ""
        ) : (
          <FormGroup label="Thumbnail URL">
            <UrlOrImageUpload
              imgurApiKey={this.props.settings.PUBLIC_API_KEYS.IMGUR_API_KEY}
              className="form-control"
              placeholder="Thumbnail URL"
              showHelp={false}
              showPreview={true}
              imageOnly={true}
              value={_.get(this.state.slide, "media.thumbnail", "")}
              error={_.get(this.state.errors, "media.thumbnail", "")}
              onChange={({ url, error }) =>
                this.updateSlide("media.thumbnail", url, error)
              }
            />
            <small className="form-text text-muted">
              <Help id="media-thumbnail-help" />
            </small>
          </FormGroup>
        )}

        <FormGroup label="Background">
          <BackgroundInput
            settings={this.props.settings}
            value={_.get(this.state.slide, "background") || {}}
            error={_.get(this.state.errors, "background", "")}
            onChange={({ value, error }) =>
              this.updateSlide("background", value, error)
            }
          />
        </FormGroup>

        <FormGroup label="Category">
          <CategoryInput
            {...this.inputProps("group", "Tags")}
            choices={
              this.props.timeline
                ? Object.keys(this.props.timeline.categories)
                : undefined
            }
          />
        </FormGroup>

        <FormGroup
          label={
            this.props.onDelete ? (
              <a
                href="#"
                className="btn btn-xs btn-outline-danger"
                onClick={this.confirmDelete.bind(this)}
              >
                <i className="fa fa-trash" /> Delete story
              </a>
            ) : (
              ""
            )
          }
        >
          <button type="submit" className="btn btn-primary pull-right">
            Save
          </button>
        </FormGroup>
      </form>
    );
  }
}
FormSlideEditor.propTypes = {
  timeline: React.PropTypes.object,
  slide: React.PropTypes.object.isRequired,
  onChange: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func
};
const ConnectedFormSlideEditor = connect(state => ({
  settings: state.settings
}))(FormSlideEditor);
export { ConnectedFormSlideEditor as FormSlideEditor };

export const Cell = props => {
  let classes = ["cell"];
  if (props.error) {
    classes.push("has-danger");
  }
  if (props.className) {
    classes.push(props.className);
  }
  return (
    <div className={classes.join(" ")}>
      {props.children}
      {props.error ? (
        <div className="cell-error">
          <div className="arrow-up" />
          <div className="form-control-feedback">{props.error}</div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

/**
 * Implementation of BaseSlideEditor for spreadsheet rows.
 */
class RowSlideEditor extends BaseSlideEditor {
  render() {
    return (
      <div className="row-slide-editor" id={`row-${this.props.slide.id}`}>
        <div className="cell index">{this.props.index + 1}</div>
        <Cell error={this.state.errors.start_date}>
          {/* Start date */}
          <VariablePrecisionDate
            disabled={this.state.slide.is_title}
            value={this.state.slide.start_date || {}}
            onChange={({ date, error }) =>
              this.updateAndGo(
                "start_date",
                date,
                error,
                "is_title",
                false,
                null
              )
            }
          />
        </Cell>
        <Cell error={this.state.errors.end_date}>
          {/* End date */}
          <VariablePrecisionDate
            disabled={this.state.slide.is_title}
            value={this.state.slide.end_date || {}}
            onChange={({ date, error }) =>
              this.updateAndGo("end_date", date, error, "is_title", false, null)
            }
          />
        </Cell>
        <Cell error={this.state.errors.is_title}>
          {/* Title status */}
          <label className="icon-checkbox bigcheck">
            <input
              type="checkbox"
              checked={this.state.slide.is_title || false}
              onChange={e => {
                this.updateAndGo(
                  "start_date",
                  {},
                  null,
                  "end_date",
                  this.props.slide.end_date || {},
                  null,
                  "is_title",
                  e.target.checked,
                  null
                );
              }}
            />
            <i className="fa fa-check-square-o checked-icon" />
            <i className="fa fa-square-o unchecked-icon" />
          </label>
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "text.headline")}
        >
          {/* Heading */}
          <input
            type="text"
            className="form-control"
            value={_.get(this.state.slide, "text.headline", "")}
            placeholder="Headline"
            onChange={e =>
              this.updateAndGo("text.headline", e.target.value, null)
            }
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "text.text")}
        >
          {/* Text */}
          <textarea
            value={_.get(this.state.slide, "text.text", "")}
            className="form-control"
            placeholder="Text"
            onChange={e => this.updateAndGo("text.text", e.target.value, null)}
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "media.url")}
        >
          {/* Media URL */}
          <UrlOrImageUpload
            imgurApiKey={this.props.settings.PUBLIC_API_KEYS.IMGUR_API_KEY}
            showHelp={false}
            showPreview={false}
            className="form-control"
            placeholder="Media URL"
            value={_.get(this.state.slide, "media.url", "")}
            error={_.get(this.state.errors, "media.url", "")}
            onChange={({ url, error }) =>
              this.updateAndGo("media.url", url, error)
            }
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "media.credit", "")}
        >
          {/* Media credit */}
          <input
            type="text"
            className="form-control"
            value={_.get(this.state.slide, "media.credit", "")}
            placeholder="Credits"
            onChange={e =>
              this.updateAndGo("media.credit", e.target.value, null)
            }
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "media.caption", "")}
        >
          {/* Media caption */}
          <input
            type="text"
            className="form-control"
            value={_.get(this.state.slide, "media.caption", "")}
            placeholder="captions"
            onChange={e =>
              this.updateAndGo("media.caption", e.target.value, null)
            }
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "media.thumbnail")}
        >
          {/* Media Thumbnail */}
          <UrlOrImageUpload
            imgurApiKey={this.props.settings.PUBLIC_API_KEYS.IMGUR_API_KEY}
            showHelp={false}
            showPreview={false}
            className="form-control"
            placeholder="Thumbnail"
            value={_.get(this.state.slide, "media.thumbnail", "")}
            error={_.get(this.state.errors, "media.thumbnail", "")}
            onChange={({ url, error }) =>
              this.updateAndGo("media.thumbnail", url, error)
            }
          />
        </Cell>
        <Cell
          className="text-width"
          error={_.get(this.state.errors, "background")}
        >
          <BackgroundInputCell
            settings={this.props.settings}
            value={_.get(this.state.slide, "background") || {}}
            onChange={({ value, error }) =>
              this.updateAndGo("background", value, error)
            }
          />
        </Cell>
        <Cell className="category text-width" error={this.state.errors.group}>
          {/* Category */}
          <CategoryInput
            className="custom-select"
            value={this.state.slide.group}
            onChange={e => this.updateAndGo("group", e.target.value, null)}
            choices={
              this.props.timeline
                ? Object.keys(this.props.timeline.categories)
                : undefined
            }
          />
        </Cell>
        {this.props.slide.id ? (
          <div
            className="cell delete-btn"
            title="delete"
            onClick={this.confirmDelete.bind(this)}
          >
            <i className="fa fa-trash-o" />
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
RowSlideEditor.propTypes = {
  index: React.PropTypes.number.isRequired,
  timeline: React.PropTypes.object,
  slide: React.PropTypes.object.isRequired,
  onChange: React.PropTypes.func.isRequired
};
const ConnectedRowSlideEditor = connect(state => ({
  settings: state.settings
}))(RowSlideEditor);
export { ConnectedRowSlideEditor as RowSlideEditor };
