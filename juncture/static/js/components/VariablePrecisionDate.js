import React from "react";
import _ from "lodash";
import { timelineDateToJsDate, MONTHS, TIME_PARTS } from "../utils/dates";

const hasOwnProperty = Object.prototype.hasOwnProperty;

const commaAnd = list => {
  if (list.length === 1) {
    return list[0];
  } else if (list.length === 2) {
    return list.join(" and ");
  } else {
    return `${list.slice(0, list.length - 1).join(", ")}, and ${
      list[list.length - 1]
    }`;
  }
};

export default class VariablePrecisionDate extends React.Component {
  setDatePart(part, valueStr) {
    const { date, error } = this.cleanDateHash({
      ...this.props.value,
      [part]: valueStr
    });
    this.props.onChange({ date, error });
  }

  cleanDateHash(origDateHash) {
    const errors = [];
    let dateHash = { ...origDateHash };
    // Clean dateHash parts to ints, accumulating errors.
    TIME_PARTS.forEach(part => {
      if (!!dateHash[part] || dateHash[part] === 0) {
        const num = parseInt(dateHash[part], 10);
        if (isNaN(num)) {
          errors.push(`${part} is invalid.`);
        } else {
          dateHash[part] = num;
        }
      } else if (hasOwnProperty.call(dateHash, part)) {
        delete dateHash[part];
      }
    });

    // Ensure that we don't have precision gaps
    let missing = [];
    let precision = null;
    _.forEachRight(TIME_PARTS, part => {
      let has = hasOwnProperty.call(dateHash, part);
      if (has && !precision) {
        precision = part;
      } else if (!has && precision) {
        missing.push(part);
      }
    });
    if (missing.length > 0) {
      missing = missing.map(_.capitalize);
      errors.push(
        `${commaAnd(missing)} ${missing.length > 1 ? "are" : "is"} ` +
          `required if ${_.capitalize(precision)} is specified.`
      );
    }

    // Ensure that the date is a valid calendar date
    if (errors.length === 0 && hasOwnProperty.call(dateHash, "year")) {
      const date = timelineDateToJsDate(dateHash);
      const dateChecks = {
        year: () => date.getFullYear(),
        month: () => date.getMonth() + 1,
        day: () => date.getDate(),
        hour: () => date.getHours(),
        minute: () => date.getMinutes(),
        second: () => date.getSeconds(),
        millisecond: () => date.getMilliseconds()
      };
      _.forEach(dateChecks, (fn, part) => {
        if (hasOwnProperty.call(dateHash, part) && dateHash[part] !== fn()) {
          errors.push(
            "Invalid date. Please check that the date and time are real calendar values."
          );
          return false;
        }
      });
    }
    return { date: dateHash, error: errors.join(" ") };
  }

  render() {
    const date = this.props.value || {};

    return (
      <div className={`date-input${this.props.error ? " has-danger" : ""}`}>
        {this.props.error ? (
          <div className="form-control-feedback offset-1">
            {this.props.error}
          </div>
        ) : (
          ""
        )}
        <div className="form-inline row">
          {this.props.label ? (
            <label className="form-control-label col-1 text-right">
              {this.props.label}
            </label>
          ) : (
            ""
          )}
          <div className="col-11">
            <input
              className="form-control"
              type="text"
              size={4}
              value={hasOwnProperty.call(date, "year") ? date.year : ""}
              onChange={e => this.setDatePart("year", e.target.value)}
              placeholder="Year"
            />

            <select
              className="custom-select"
              value={hasOwnProperty.call(date, "month") ? date.month : ""}
              onChange={e => this.setDatePart("month", e.target.value)}
            >
              <option value="">----</option>
              {MONTHS.map((month, i) => (
                <option value={i + 1} key={month}>
                  {month}
                </option>
              ))}
            </select>

            <input
              className="form-control"
              type="text"
              size={2}
              value={hasOwnProperty.call(date, "day") ? date.day : ""}
              onChange={e => this.setDatePart("day", e.target.value)}
              placeholder="Day"
            />
          </div>
        </div>
      </div>
    );
  }
}
VariablePrecisionDate.propTypes = {
  value: React.PropTypes.object,
  label: React.PropTypes.string,
  error: React.PropTypes.node,
  onChange: React.PropTypes.func.isRequired
};
