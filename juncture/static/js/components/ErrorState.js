import React from "react";
import { connect } from "react-redux";

class ErrorState extends React.Component {
  render() {
    let error;
    if (this.props.message) {
      error = this.props.message;
    } else if (this.props.id) {
      error = this.props.error[this.props.id];
    } else if (this.props.ids) {
      const errors = [];
      this.props.ids.forEach(id => {
        if (this.props.error[id]) {
          errors.push(this.props.error[id]);
        }
      });
      error = errors.join(" ");
    }
    let classes = ["error-state", "has-danger"];
    if (this.props.className) {
      classes.push(this.props.className);
    }
    if (error) {
      return (
        <div className={classes.join(" ")}>
          <div className="form-control-feedback">
            {this.props.prefix ? this.props.prefix : ""}
            {error}
            {this.props.suffix ? this.props.suffix : ""}
          </div>
          {this.props.children || null}
        </div>
      );
    }
    return this.props.children || null;
  }
}
ErrorState.propTypes = {
  id: React.PropTypes.string,
  ids: React.PropTypes.array,
  error: React.PropTypes.object,
  message: React.PropTypes.string,
  suffix: React.PropTypes.string,
  prefix: React.PropTypes.string,
  className: React.PropTypes.string,
  children: React.PropTypes.node
};

export default connect(state => ({ error: state.error }))(ErrorState);
