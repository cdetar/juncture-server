import React from "react";
import _ from "lodash";
import VariablePrecisionDate from "./VariablePrecisionDate";
import { Cell, BaseObjEditor } from "./SlideEditor";
import { timelineDateToJsDate } from "../utils/dates";

export default class RowEraEditor extends BaseObjEditor {
  constructor(props) {
    super(props);
    this.objName = "era";
    this.objNoun = "Era";
    this.fields = [
      "id",
      "start_date.year",
      "start_date.month",
      "start_date.day",
      "end_date.year",
      "end_date.month",
      "end_date.day",
      "text.headline"
    ];
    this.state = this.getInitialState(props);
  }

  updateEra(...params) {
    return this.updateObj(...params);
  }

  errorChecks(era, errors) {
    const hasStart = this.findDeepTruthy(era.start_date);
    const hasEnd = this.findDeepTruthy(era.end_date);

    this.checkError(errors, !hasStart, "start_date", "Start date is required");
    this.checkError(errors, !hasEnd, "end_date", "End date is required");
    this.checkError(
      errors,
      !_.get(era, "text.headline"),
      "text.headline",
      "Era name is required"
    );
    this.checkError(
      errors,
      hasStart && hasEnd && era.end_date.month && !era.start_date.month,
      "end_date",
      "Start date must have a month if end date has a month."
    );
    this.checkError(
      errors,
      hasStart && hasEnd && era.end_date.day && !era.end_date.month,
      "end_date",
      "Start date must have a day if end date has a day."
    );
    this.checkError(
      errors,
      hasStart &&
        hasEnd &&
        timelineDateToJsDate(era.end_date) <=
          timelineDateToJsDate(era.start_date),
      "end_date",
      "End date must come after start date."
    );
  }
  render() {
    return (
      <div className="row-era-editor" id={`row-era-${this.props.era.id}`}>
        <div className="cell index">{this.props.index + 1}</div>
        <Cell error={this.state.errors.start_date}>
          <VariablePrecisionDate
            value={this.state.era.start_date}
            onChange={({ date, error }) =>
              this.updateAndGo("start_date", date, error)
            }
          />
        </Cell>
        <Cell error={this.state.errors.end_date}>
          <VariablePrecisionDate
            value={this.state.era.end_date}
            onChange={({ date, error }) =>
              this.updateAndGo("end_date", date, error)
            }
          />
        </Cell>
        <Cell error={_.get(this.state.errors, "text.headline")}>
          <input
            type="text"
            className="form-control"
            value={_.get(this.state.era, "text.headline") || ""}
            onChange={e => this.updateAndGo("text.headline", e.target.value)}
          />
        </Cell>
        <div
          className="cell delete-btn"
          title="delete"
          onClick={this.confirmDelete.bind(this)}
        >
          <i className="fa fa-trash-o" />
        </div>
      </div>
    );
  }
}
