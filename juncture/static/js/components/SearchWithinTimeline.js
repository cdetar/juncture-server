import React from "react";
import _ from "lodash";
import jQuery from "jquery";
import { displayTimelineDate } from "../utils/dates";
import "./searchwithintimeline.scss";

const quoteRe = pattern => pattern.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");

export default class SearchWithinTimeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searching: false,
      q: ""
    };
    this._cancelOnClickout = event => {
      if (this.state.searching) {
        if (
          !jQuery(event.target).closest(
            ".search-within-timeline .toggle-search"
          ).length &&
          !jQuery(event.target).closest(".search-within-timeline-form").length
        ) {
          this.setState({ searching: false });
        }
      }
    };
  }

  componentWillMount() {
    jQuery(document).on("click", this._cancelOnClickout);
  }
  componentWillUnmount() {
    jQuery(document).off("click", this._cancelOnClickout);
  }

  toggleSearch(event) {
    event && event.preventDefault();
    this.setState({ searching: !this.state.searching });
  }

  truncate(text, length = 140) {
    if (text.length < length) {
      return text;
    } else {
      return text.substring(0, 140) + "...";
    }
  }

  getSearchRegExp() {
    const tokens = this.state.q.toLowerCase().split(" ");
    return new RegExp(`(${tokens.map(quoteRe).join("|")})`, "i");
  }

  searchSlides() {
    if (!this.state.q) {
      return [];
    }
    const tokens = this.state.q.toLowerCase().split(" ");
    const slides = this.props.timeline.slides.map(id => this.props.slides[id]);
    return slides.filter(slide => {
      const parts = [];
      slide.text && slide.text.headline && parts.push(slide.text.headline);
      slide.text && slide.text.text && parts.push(slide.text.text);
      slide.media && slide.media.url && parts.push(slide.media.url);
      const text = parts.join(" ").toLowerCase();
      return _.every(tokens, token => text.indexOf(token) !== -1);
    });
  }

  goTo(slideId) {
    this.toggleSearch();
    return this.props.onGoTo(slideId);
  }

  highlight(text) {
    if (!this.state.q) {
      return text;
    }
    const parts = text.split(this.getSearchRegExp());
    return parts.map((part, i) => {
      let key = `h-${i}`;
      if (i % 2 == 0) {
        return <span key={key}>{part}</span>;
      }
      return <b key={key}>{part}</b>;
    });
  }

  render() {
    return (
      <div className="search-within-timeline">
        <a
          href="#"
          className="toggle-search"
          onClick={this.toggleSearch.bind(this)}
        >
          <i className="fa fa-search" />
          {this.state.searching ? <div className="arrow-up" /> : ""}
        </a>
        {this.state.searching ? (
          <form className="search-within-timeline-form">
            <input
              className="form-control"
              placeholder="Search"
              value={this.state.q}
              autoFocus={true}
              onChange={e => this.setState({ q: e.target.value })}
            />
            {this.searchSlides().map(slide => (
              <div className="search-result" key={slide.id}>
                <a href="#" onClick={() => this.goTo(slide.id)}>
                  {slide.group ? (
                    <span className="badge badge-default">{slide.group}</span>
                  ) : (
                    ""
                  )}{" "}
                  {slide.start_date && slide.start_date.year
                    ? displayTimelineDate(slide.start_date)
                    : ""}{" "}
                  {slide.text && slide.text.headline
                    ? this.highlight(slide.text.headline)
                    : "Untitled"}
                </a>{" "}
                {slide.text && slide.text.text ? (
                  <span className="search-result-text">
                    {this.highlight(this.truncate(slide.text.text))}
                  </span>
                ) : (
                  ""
                )}{" "}
                {slide.media && slide.media.url ? (
                  <div className="search-result-media-url">
                    {this.highlight(this.truncate(slide.media.url))}
                  </div>
                ) : (
                  ""
                )}
              </div>
            ))}
          </form>
        ) : (
          ""
        )}
      </div>
    );
  }
}
SearchWithinTimeline.propTypes = {
  timeline: React.PropTypes.object.isRequired,
  slides: React.PropTypes.object.isRequired,
  onGoTo: React.PropTypes.func.isRequired
};
