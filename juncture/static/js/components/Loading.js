import React from "react";
import { connect } from "react-redux";

class Loading extends React.Component {
  render() {
    let loading = this.props.message || this.props.loading[this.props.id];
    let classes = [];
    if (this.props.className) {
      classes.push(this.props.className);
    }
    if (loading) {
      classes.push("loading");

      return (
        <div className={classes.join(" ")}>
          <i className="fa fa-spinner fa-spin" />{" "}
          {loading === true ? "" : loading}
        </div>
      );
    } else {
      return <div className={classes.join(" ")}>{this.props.children}</div>;
    }
  }
}
Loading.propTypes = {
  id: React.PropTypes.string,
  loading: React.PropTypes.object,
  message: React.PropTypes.string,
  className: React.PropTypes.string,
  children: React.PropTypes.node
};

export default connect(state => ({ loading: state.loading }))(Loading);
