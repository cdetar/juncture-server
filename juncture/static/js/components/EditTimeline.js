import React from "react";

import TimelineProvider from "./TimelineProvider";
import Timeline3Viewer from "./Timeline3Viewer";
import TimelineEditBar from "./TimelineEditBar";
import RedirectReceiver from "./RedirectReceiver";
import SpreadsheetEditor from "./SpreadsheetEditor";
import "./edittimeline.scss";

export default class EditTimeline extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curSlideId: null,
      mode: "viewer"
      //mode: "spreadsheet",
    };
  }
  render() {
    let empty = <p>Add your first story!</p>;
    return (
      <TimelineProvider
        timelineId={this.props.routeParams.id}
        live
        className="timeline-editor"
      >
        <TimelineEditBar
          className="timeline-editor-bar"
          curSlideId={this.state.curSlideId}
          onGoTo={curSlideId => this.setState({ curSlideId: curSlideId + "" })}
          mode={this.state.mode}
          onChangeMode={mode => this.setState({ mode })}
        />
        {this.state.mode === "spreadsheet" ? (
          <SpreadsheetEditor
            className="timeline-spreadsheet-editor"
            curSlideId={this.state.curSlideId}
          />
        ) : (
          <Timeline3Viewer
            empty={empty}
            className="timeline-editor-viewer"
            curSlideId={this.state.curSlideId}
            onChange={uniqueId => this.setState({ curSlideId: uniqueId })}
          />
        )}
        <RedirectReceiver router={this.props.router} />
      </TimelineProvider>
    );
  }
}
