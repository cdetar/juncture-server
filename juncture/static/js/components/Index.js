import React from "react";
import { Router, Route, IndexRoute, browserHistory } from "react-router";

import ConnectionStatus from "./ConnectionStatus";
import CreateTimeline from "./CreateTimeline";
import ImportTimeline from "./ImportTimeline";
import EditTimeline from "./EditTimeline";
import FrontPage from "./FrontPage";
import FourOhFour from "./FourOhFour";
import Groups from "./Groups";

const Page = props => (
  <div className="juncture">
    <ConnectionStatus />
    {props.children}
  </div>
);

export default function Index(props) {
  return (
    <Router history={browserHistory}>
      <Route path="/" component={Page}>
        <IndexRoute component={FrontPage} />
        <Route path="create" component={CreateTimeline} />
        <Route path="import" component={ImportTimeline} />
        <Route path="groups" component={Groups} />
        <Route path="timelines/:id/edit" component={EditTimeline} />
      </Route>
      <Route path="*" component={FourOhFour} />
    </Router>
  );
}
