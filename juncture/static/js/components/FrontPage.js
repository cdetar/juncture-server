import React from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { Link } from "react-router";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import Navbar from "./Navbar";
import * as A from "../store/actions";
import { dateFmt } from "../utils/dates.js";

class FrontPage extends React.Component {
  componentWillMount() {
    this.fetchTimelineList(this.props);
    document.title = "Timelines";
  }
  componentWillReceiveProps(newProps) {
    if (newProps.auth.id !== this.props.auth.id) {
      this.fetchTimelineList(newProps);
    }
  }
  fetchTimelineList(props) {
    props.fetchTimelineList({});
  }
  render() {
    return (
      <div className="front-page">
        <Navbar />
        <div className="container">
          <h1>Hello, {this.props.auth.display_name}!</h1>
          <ul>
            <li>
              <Link to="/create">Create new timeline</Link>
            </li>
            <li>
              <Link to="/import">Import an existing timeline</Link>
            </li>
            <li>
              <Link to="/groups">My Groups</Link>
            </li>
          </ul>
          <ErrorState id={A.FETCH_TIMELINE_LIST_TYPE} />
          <Loading id={A.FETCH_TIMELINE_LIST_TYPE}>
            <ul>
              {this.props.timelineList
                ? _.map(this.props.timelineList, timeline => (
                    <li key={timeline.secret_id}>
                      <Link to={`/timelines/${timeline.secret_id}/edit`}>
                        {timeline.name}
                      </Link>
                      {" - "}
                      {timeline.slides_count} stor
                      {timeline.slides_count === 1 ? "y, " : "ies, "}
                      created {dateFmt(timeline.reated)}
                    </li>
                  ))
                : ""}
            </ul>
          </Loading>
        </div>
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    auth: state.auth,
    loading: state.loading,
    timelineList: state.timelineList
  }),
  dispatch => ({
    fetchTimelineList: payload => dispatch(A.fetchTimelineList(payload))
  })
)(FrontPage);
