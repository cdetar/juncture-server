import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import moment from "moment";

import * as A from "../store/actions.js";
import ErrorState from "./ErrorState";
import Loading from "./Loading";
import Navbar from "./Navbar";
import { dateFmt } from "../utils/dates.js";
import "../../scss/pages/groups.scss";

const WhoRow = props => {
  const { who, role, auth, isOwner, onRemove, onLeave } = props;
  const removeClickHandler = e => {
    e.preventDefault();
    onRemove(who.id);
  };
  const leaveClickHandler = e => {
    e.preventDefault();
    onLeave(who.id);
  };
  return (
    <tr>
      <td>
        {who.user ? (
          <span>
            {who.user.profile_image ? (
              <img src={who.user.profile_image} width={32} height={32} />
            ) : null}{" "}
            {who.user.display_name}
          </span>
        ) : (
          <span>{who.invitee}</span>
        )}
      </td>
      <td>{role}</td>
      <td>
        {moment(who.expiration).year() > 2400
          ? "Never"
          : dateFmt(who.expiration)}
      </td>
      <td>
        {props.onRemove && isOwner ? (
          <a
            href="#"
            className="btn btn-outline-danger"
            onClick={removeClickHandler}
          >
            Remove
          </a>
        ) : props.onLeave && _.get(who, "user.id") === auth.id ? (
          <a
            href="#"
            className="btn btn-outline-danger"
            onClick={leaveClickHandler}
          >
            Leave
          </a>
        ) : (
          ""
        )}
      </td>
    </tr>
  );
};
WhoRow.propTypes = {
  who: React.PropTypes.object.isRequired,
  role: React.PropTypes.oneOf(["Owner", "Member"]).isRequired,
  isOwner: React.PropTypes.bool.isRequired,
  auth: React.PropTypes.object.isRequired,
  onRemove: React.PropTypes.func,
  onLeave: React.PropTypes.func
};

class GroupEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      invitee: "",
      role: "owner",
      expiration: "-1",
      editingName: false
    };
  }

  onRemoveWho(whoId) {
    const newGroup = { ...this.props.group };
    const who =
      _.find(newGroup.owners, w => w.id === whoId) ||
      _.find(newGroup.members, w => w.id === whoId);
    if (
      who.user &&
      who.user.id === this.props.auth.id &&
      !confirm(
        `Are you sure you want to remove yourself from "${this.props.group.name}"?`
      )
    ) {
      return;
    }
    newGroup.owners = _.filter(newGroup.owners, w => w.id !== whoId);
    newGroup.members = _.filter(newGroup.members, w => w.id !== whoId);
    if (newGroup.owners.lenth === 0) {
      alert("Must leave at least one owner");
      return;
    }
    this.props.changeGroup(newGroup);
  }

  onLeaveWho(whoId) {
    this.props.leaveGroup({ id: this.props.group.id });
  }

  onAddWho(event) {
    event && event.preventDefault();
    if (this.state.invitee.trim()) {
      const newGroup = { ...this.props.group };
      const invitee = {
        invitee: this.state.invitee,
        expiration: parseInt(this.state.expiration)
      };

      if (this.state.role === "owner") {
        newGroup.owners = [...this.props.group.owners, invitee];
      } else if (this.state.role === "member") {
        newGroup.members = [...this.props.group.members, invitee];
      }
      this.props.changeGroup(newGroup);
      this.setState({ invitee: "" });
    }
  }

  onDeleteGroup(event) {
    event && event.preventDefault();
    this.props.deleteGroup(this.props.group);
  }

  onEditName(event) {
    event && event.preventDefault();
    if (!this.state.groupName) {
      return;
    }
    this.props.changeGroup({ ...this.props.group, name: this.state.groupName });
    this.setState({ editingName: false });
  }

  toggleEditName(event) {
    event && event.preventDefault();
    this.setState({
      editingName: !this.state.editingName,
      groupName: this.props.group.name
    });
  }

  render() {
    const { group, isOwner } = this.props;
    return (
      <div className="group-editor">
        <div className="name">
          {this.state.editingName ? (
            <form onSubmit={this.onEditName.bind(this)} className="form-inline">
              <input
                type="text"
                className="form-control"
                placeholder="Group name"
                value={this.state.groupName}
                onChange={e => this.setState({ groupName: e.target.value })}
              />
              <button type="submit" className="btn btn-primary">
                Save
              </button>
              <a
                href="#"
                onClick={this.toggleEditName.bind(this)}
                className="btn btn-default"
              >
                Cancel
              </a>
            </form>
          ) : (
            <div>
              <h4>{group.name}</h4>
              {isOwner ? (
                <a href="#" onClick={this.toggleEditName.bind(this)}>
                  <i className="fa fa-pencil" />
                </a>
              ) : null}
              {isOwner ? (
                <a
                  className="btn btn-outline-danger pull-right"
                  tabIndex="-1"
                  href="#"
                  onClick={this.onDeleteGroup.bind(this)}
                >
                  Delete group
                </a>
              ) : null}
            </div>
          )}
        </div>
        <form onSubmit={this.onAddWho.bind(this)}>
          <table className="table">
            <thead>
              <tr>
                <th>User</th>
                <th>Role</th>
                <th>Membership Expires</th>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {[
                [group.owners, "Owner"],
                [group.members, "Member"]
              ].map(([list, role]) =>
                list.map(who => (
                  <WhoRow
                    who={who}
                    key={`${who.id}`}
                    role={role}
                    isOwner={isOwner}
                    auth={this.props.auth}
                    onRemove={
                      role === "Owner" && this.props.group.owners.length === 1
                        ? undefined
                        : this.onRemoveWho.bind(this)
                    }
                    onLeave={this.onLeaveWho.bind(this)}
                  />
                ))
              )}
            </tbody>
            {isOwner ? (
              <tfoot>
                <tr>
                  <td>
                    <input
                      className="form-control"
                      placeholder="Email or username"
                      value={this.state.invitee}
                      onChange={e => this.setState({ invitee: e.target.value })}
                    />
                  </td>
                  <td>
                    <select
                      className="form-control"
                      value={this.state.role}
                      onChange={e => this.setState({ role: e.target.value })}
                    >
                      <option value="owner">Owner</option>
                      <option value="member">Member</option>
                    </select>
                  </td>
                  <td>
                    <select
                      className="form-control"
                      value={this.state.expiration}
                      onChange={e =>
                        this.setState({ expiration: e.target.value })
                      }
                    >
                      <option value="-1">Never</option>
                      <option value="1">in 1 day</option>
                      <option value="7">in 7 days</option>
                      <option value="14">in 2 weeks</option>
                      <option value="90">in 3 months</option>
                    </select>
                  </td>
                  <td>
                    <button type="submit" className="btn btn-primary">
                      Add user
                    </button>
                  </td>
                </tr>
              </tfoot>
            ) : null}
          </table>
        </form>
      </div>
    );
  }
}
GroupEditor.propTypes = {
  group: React.PropTypes.object.isRequired,
  auth: React.PropTypes.object.isRequired,
  isOwner: React.PropTypes.bool.isRequired,
  leaveGroup: React.PropTypes.func,
  changeGroup: React.PropTypes.func,
  deleteGroup: React.PropTypes.func
};

class Groups extends React.Component {
  constructor(props) {
    super(props);
    this.state = { adding: false, group_name: "" };
  }

  componentWillMount() {
    if (!this.props.auth.is_authenticated) {
      let url = `/accounts/login/?next=${encodeURIComponent("/groups")}`;
      document.location.href = url;
      return;
    }
    document.title = "Groups";
    this.props.fetchGroups();
  }

  changeGroup(group) {
    this.props.changeGroup({ group: group });
  }

  removeGroup(group) {
    if (
      confirm(
        `Are you sure you want to permanently delete the group "${group.name}"?`
      )
    ) {
      this.props.removeGroup({ group: { id: group.id } });
    }
  }

  leaveGroup(group) {
    if (confirm(`Are you sure you want to leave "${group.name}"?`)) {
      this.props.leaveGroup({ group: group });
    }
  }

  addGroup(event) {
    event && event.preventDefault();
    this.props.addGroup({ group: { name: this.state.group_name } });
    this.setState({ adding: false, group_name: "" });
  }

  render() {
    const owned = _.filter(
      this.props.groups,
      g =>
        !!_.find(
          g.owners,
          who => who.user && who.user.id === this.props.auth.id
        )
    );
    const membered = _.filter(
      this.props.groups,
      g =>
        !!_.find(
          g.members,
          who => who.user && who.user.id === this.props.auth.id
        )
    );
    return (
      <div className="groups">
        <Navbar />
        <div className="container">
          <Loading id={A.FETCH_GROUPS_TYPE}>
            <ErrorState id={A.FETCH_GROUPS_TYPE} />

            {owned.length > 0 ? (
              <div>
                <h1>Your Groups</h1>
                <ErrorState
                  ids={[
                    A.CHANGE_GROUP_TYPE,
                    A.ADD_GROUP_TYPE,
                    A.LEAVE_GROUP_TYPE
                  ]}
                />
                {owned.map(group => (
                  <Loading id={A.CHANGE_GROUP_TYPE} key={group.id}>
                    <GroupEditor
                      group={group}
                      auth={this.props.auth}
                      changeGroup={this.changeGroup.bind(this)}
                      deleteGroup={this.removeGroup.bind(this)}
                      leaveGroup={this.leaveGroup.bind(this)}
                      isOwner={true}
                    />
                  </Loading>
                ))}
              </div>
            ) : null}

            {membered.length > 0 ? (
              <div>
                <h2>Your Memberships</h2>
                {membered.map(group => (
                  <GroupEditor
                    key={group.id}
                    group={group}
                    auth={this.props.auth}
                    leaveGroup={this.leaveGroup.bind(this)}
                    isOwner={false}
                  />
                ))}
              </div>
            ) : null}

            {owned.length === 0 && membered.length === 0 ? (
              <p>No groups yet.</p>
            ) : null}

            {this.state.adding ? (
              <form className="form-inline" onSubmit={this.addGroup.bind(this)}>
                <input
                  type="text"
                  className="form-control"
                  placeholder="New group name"
                  value={this.state.group_name}
                  onChange={e => this.setState({ group_name: e.target.value })}
                />
                <button className="btn btn-primary" type="submit">
                  Add Group
                </button>
              </form>
            ) : (
              <button
                className="btn btn-primary"
                onClick={e => this.setState({ adding: true })}
              >
                Add Group
              </button>
            )}
          </Loading>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth,
    groups: state.groups
  }),
  dispatch => ({
    fetchGroups: payload => dispatch(A.fetchGroups(payload)),
    changeGroup: payload => dispatch(A.changeGroup(payload)),
    addGroup: payload => dispatch(A.addGroup(payload)),
    removeGroup: payload => dispatch(A.removeGroup(payload)),
    leaveGroup: payload => dispatch(A.leaveGroup(payload))
  })
)(Groups);
