import React from "react";
import { connect } from "react-redux";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import * as A from "../store/actions";

/**
 * Component that connects to and provides the props {timeline, slides, eras}
 * to its children. If used with the `live` prop, will join a socket to listen
 * for real-time updates.
 */
class TimelineProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { joined: null };
  }
  componentWillMount() {
    this.fetchTimeline(this.props);
  }
  componentWillReceiveProps(newProps) {
    if (this.props.timelineId !== newProps.timelineId) {
      this.fetchTimeline(newProps);
    }
  }
  componentWillUnmount() {
    this.props.leave({ timeline_id: this.props.timelineId });
  }
  fetchTimeline(props) {
    props = props || this.props;
    if (this.state.joined && this.state.joined !== props.timelineId) {
      props.leave({ timeline_id: this.state.joined });
      this.setState({ joined: null });
    }
    if (props.live) {
      props.join({ timeline_id: props.timelineId });
      this.setState({ joined: props.timelineId });
    }
    props.fetchTimeline(props.timelineId);
  }
  render() {
    const {
      timelineId,
      className,
      timelines,
      slides,
      eras,
      permissions,
      auth,
      children
    } = this.props;
    const timeline = timelines && timelines[timelineId];
    if (timeline && slides && eras) {
      let providedChildren = React.Children.map(children, child => {
        return React.cloneElement(child, {
          timeline: timeline,
          slides: slides,
          eras: eras,
          permissions: permissions,
          auth: auth,
          ...child.props
        });
      });
      return <div className={className}>{providedChildren}</div>;
    } else {
      return (
        <div className={className}>
          <Loading id={A.FETCH_TIMELINE_TYPE}>
            <ErrorState id={A.FETCH_TIMELINE_TYPE} />
          </Loading>
        </div>
      );
    }
  }
}

export default connect(
  state => ({
    timelines: state.timelines,
    slides: state.slides,
    eras: state.eras,
    permissions: state.permissions,
    auth: state.auth
  }),
  dispatch => ({
    fetchTimeline: payload => dispatch(A.fetchTimeline(payload)),
    join: payload => dispatch(A.join(payload)),
    leave: payload => dispatch(A.leave(payload))
  })
)(TimelineProvider);
