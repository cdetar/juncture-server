import React from "react";
import jQuery from "jquery";
import "./inplaceeditor.scss";

export default class InPlaceEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      value: this.props.value
    };
    this._cancelOnClickout = event => {
      if (this.state.editing) {
        if (
          !jQuery(event.target).closest(".in-place-editor-trigger").length &&
          !jQuery(event.target).closest(".in-place-editor-form").length
        ) {
          this.setState({ editing: false });
        }
      }
    };
  }

  componentWillMount() {
    jQuery(document).on("click", this._cancelOnClickout);
  }
  componentWillUnmount() {
    jQuery(document).off("click", this._cancelOnClickout);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value !== this.props.value) {
      this.setState({ value: newProps.value });
    }
  }

  toggleEditing(event) {
    event && event.preventDefault();
    if (this.props.editable) {
      if (this.state.editing) {
        this.setState({ value: this.props.value });
      }
      this.setState({ editing: !this.state.editing });
    }
  }

  onChange(event) {
    event && event.preventDefault();
    if (!this.props.allowEmpty && !this.state.value) {
      this.toggleEditing();
    } else {
      this.props.onChange && this.props.onChange(this.state.value);
      this.toggleEditing();
    }
  }

  render() {
    let classes = [
      "in-place-editor",
      this.props.editable ? "editable" : "readonly"
    ];
    if (this.state.editing) {
      classes.push("editing");
    }

    return (
      <div className={classes.join(" ")}>
        <div
          className="in-place-editor-trigger"
          onClick={this.toggleEditing.bind(this)}
        >
          {this.props.children}
        </div>
        {this.state.editing ? (
          <form
            className="form-inline in-place-editor-form"
            onSubmit={this.onChange.bind(this)}
          >
            <div className="arrow-up"></div>
            <input
              className="form-control"
              type="text"
              value={this.state.value}
              autoFocus={true}
              placeholder={this.props.placeholder}
              onChange={e => this.setState({ value: e.target.value })}
            />
            <button className="btn btn-default" type="submit">
              Save
            </button>
          </form>
        ) : (
          ""
        )}
      </div>
    );
  }
}
