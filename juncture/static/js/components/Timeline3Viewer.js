/* globals TL */
import React from "react";
import jQuery from "jquery";
import _ from "lodash";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import { addCategoryColors, applyMediaTypes } from "../utils/timeline-utils";
import * as permutils from "../utils/permutils.js";

const TIMELINE3_JS =
  "https://cdn.knightlab.com/libs/timeline3/3.6.6/js/timeline.js";
//const TIMELINE3_JS = 'http://localhost:9000/js/timeline.js';
const TIMELINE3_CSS =
  "https://cdn.knightlab.com/libs/timeline3/3.6.6/css/timeline.css";
//const TIMELINE3_CSS = 'http://localhost:9000/css/timeline.css';

/**
 * Component for displaying a timeline in TimelineJS3.  Handles interfacing
 * with the non-React TimelineJS3 code, and updating state as needed. Requires
 * `timeline`, `slides`, `eras`, `permissions`, and `auth` props as provided by
 * the `TimelineProvider` component.  Example:
 *   <TimelineProvider timelineId={...} live>
 *     <Timeline3Viewer />
 *   </TimelineProvider>
 */
export default class Timeline3Viewer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingTL3: false,
      loadingTL3Error: null,
      mounted: false
    };
    this.debug = false;
    // eslint-disable-next-line no-console
    this.log = (...params) => this.debug && console.log.apply(console, params);
  }

  componentWillMount() {
    this.log("Timeline3Viewer.componentWillMount");
    if (!window.TL) {
      this.loadScripts();
    }
  }

  componentDidMount() {
    this.log(
      "Timeline3Viewer.componentDidMount",
      window.TL,
      !this.timeline3obj
    );
    this.setState({ mounted: true }, () => {
      this.updateTimeline(this.props);
    });
  }

  componentWillReceiveProps(newProps) {
    this.log("Timeline3Viewer.componentWillReceiveProps");
    if (newProps.curSlideId !== this.props.curSlideId) {
      this.timeline3obj.goToId(newProps.curSlideId);
    }
    this.updateTimeline(newProps);
  }

  shouldComponentUpdate(newProps, newState) {
    this.log("Timeline3Viewer.shouldComponentUpdate");
    return (
      newState.loadingTL3 !== this.state.loadingTL3 ||
      newState.empty !== this.state.empty ||
      newState.permissionDenied !== this.state.permissionDenied ||
      newProps.timeline.secret_id !== this.props.timeline.secret_id
    );
  }

  loadScripts() {
    this.setState({ loadingTL3: true });
    jQuery
      .ajax({ url: TIMELINE3_JS, dataType: "script", cache: true })
      .done(() => {
        this.setState({ loadingTL3: false });
        if (this.state.mounted) {
          this.updateTimeline(this.props);
        }
      })
      .fail((jqxhr, settings, exception) =>
        this.setState({
          loadingTL3Error: exception.toString()
        })
      );

    let linkId = "#timeline3-css";
    let link = document.querySelector(linkId);
    if (!link) {
      link = document.createElement("link");
      link.rel = "stylesheet";
      link.href = TIMELINE3_CSS;
      link.id = linkId;
      document.querySelector("head").appendChild(link);
    }
  }

  _packTLData(props) {
    let title =
      props.slides[
        _.find(props.timeline.slides, id => {
          return props.slides[id] && props.slides[id].is_title;
        })
      ];
    let nonTitles = _.reject(props.timeline.slides, id => {
      return !props.slides[id] || props.slides[id].is_title;
    }).map(id => props.slides[id]);

    let data = applyMediaTypes(
      _.cloneDeep({
        title: title,
        eras: props.timeline.eras.map(id => props.eras[id]),
        events: nonTitles,
        scale: props.timeline.scale,
        categories: props.timeline.categories
      })
    );

    // Remove empty date hashes, and set unique_id param for every slide and era.
    [data.eras, data.events, [data.title]].forEach(
      list =>
        list &&
        list.forEach(obj => {
          if (obj) {
            if (obj.start_date && _.isNil(obj.start_date.year)) {
              delete obj.start_date;
            }
            if (obj.end_date && _.isNil(obj.end_date.year)) {
              delete obj.end_date;
            }
            if (obj.media && _.isNil(obj.media.url)) {
              delete obj.media;
            }
            if (
              obj.text &&
              _.isNil(obj.text.text) &&
              _.isNil(obj.text.headline)
            ) {
              delete obj.text;
            }
            obj.unique_id = `${obj.id}`;
          }
        })
    );
    return data;
  }

  updateTimeline(props) {
    this.log("Timeline3Viewer.updateTimeline");
    if (!(window.TL && props.timeline && this.state.mounted)) {
      return;
    }
    // Check permissions.
    const perms = props.permissions.timelines[props.timeline.secret_id];
    const canView = permutils.isViewer(perms, props.auth);
    if (!canView) {
      return this.setState({ permissionDenied: true, renderedData: null });
    } else if (this.state.permissionDenied) {
      return this.setState({ permissionDenied: false }, () =>
        this.updateTimeline(props)
      );
    }

    // Data packed in the manner TimelineJS3 expects it; cloned so that
    // timelinejs's munging of it doesn't hurt editing.
    const tlData = this._packTLData(props);

    // Handle empty message.
    if (tlData.events.length === 0 && tlData.eras.length === 0) {
      return this.setState({ empty: true, renderedData: null });
    } else if (this.state.empty) {
      // No longer empty? Recurse after clearing empty state.
      return this.setState({ empty: false }, () => this.updateTimeline(props));
    }
    // Don't update if our data hasn't changed.
    if (_.isEqual(tlData, this.state.renderedData)) {
      return;
    }
    // Clone data again for state persistence, so that we can check if it's
    // changed in the future.
    this.setState({ renderedData: _.cloneDeep(tlData) });

    // Prepare options
    let options = { debug: false };

    let domId = `timeline3-${props.timeline.secret_id}`;
    // Initialize
    if (this.timeline3obj) {
      // Remove change listener so changes on initialization don't trigger
      // state updates.
      this.timeline3obj.removeEventListener(
        "change",
        this.timeline3obj._junctureChangeListener
      );

      // update category colors
      addCategoryColors(this.timeline3obj, props.timeline.categories).then(
        () => {
          if (this.state.curSlideId) {
            this.timeline3obj.goToId(this.state.curSlideId);
          }
          // Restore change listener
          this.timeline3obj.addEventListener(
            "change",
            this.timeline3obj._junctureChangeListener,
            false
          );
        }
      );
      // update timeline data; will result in resolving addCategoryColors' promise
      this.timeline3obj.initialize(domId, tlData, options);
    } else {
      this.timeline3obj = new TL.Timeline(domId, tlData, options);
      this.timeline3obj._junctureChangeListener = event => {
        if (event.unique_id) {
          this.setState({ curSlideId: event.unique_id });
          props.onChange && props.onChange(event.unique_id);
        }
      };
      this.timeline3obj.addEventListener(
        "change",
        this.timeline3obj._junctureChangeListener,
        false
      );

      let onChange = props.onChange;
      addCategoryColors(this.timeline3obj, props.timeline.categories).then(
        () => {
          let cur = this.timeline3obj.getCurrentSlide();
          cur && cur.data && onChange && onChange(cur.data.unique_id);
        }
      );

      window.__timeline3obj = this.timeline3obj;
    }
  }

  render() {
    if (this.state.empty) {
      return (
        <div className={this.props.className}>{this.props.empty || null}</div>
      );
    } else if (this.state.permissionDenied) {
      return <div className="has-danger">Permission denied</div>;
    } else {
      return (
        <Loading
          message={this.state.loadingTL3 ? "Loading TimelineJS" : null}
          className={this.props.className}
        >
          <ErrorState message={this.state.loadingTL3Error} />
          <div id={`timeline3-${this.props.timeline.secret_id}`} />
        </Loading>
      );
    }
  }
}
Timeline3Viewer.propTypes = {
  timeline: React.PropTypes.object,
  eras: React.PropTypes.object,
  slides: React.PropTypes.object,
  permissions: React.PropTypes.object,
  curSlideId: React.PropTypes.string,
  className: React.PropTypes.string,
  empty: React.PropTypes.node
};
