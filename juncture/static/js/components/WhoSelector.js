import React from "react";
import { connect } from "react-redux";
import { Creatable } from "react-select";
import _ from "lodash";
import { whoKey, Who } from "./Who";
import * as permutils from "../utils/permutils";
import * as A from "../store/actions";
import "react-select/dist/react-select.css";

class WhoSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: whoKey(this.props.who) || "" };
    this._debouncedSearchWhos = _.debounce(this.props.searchWhos, 100);
  }

  componentWillMount() {
    const hasGroups =
      !!this.props.groups ||
      !!_.get([A.FETCH_GROUPS_TYPE, "loading"], this.props.loading);
    if (!hasGroups) {
      this.props.fetchGroups();
    }
  }

  tokenize(value) {
    return value.toLowerCase().split(" ");
  }

  findAllTokensInString(tokens, string) {
    string = string.toLowerCase();
    for (let i = 0; i < tokens.length; i++) {
      if (string.indexOf(tokens[i]) === -1) {
        return false;
      }
    }
    return true;
  }

  render() {
    const { who } = this.props;

    // construct a pool of all the "who's" we might be searching for.
    const choicePool = {};
    [
      who,
      { public: true },
      ..._.map(this.props.groups, group => ({ group: group })),
      ..._.values(this.props.whoSearch)
    ].forEach(choice => (choicePool[whoKey(choice)] = choice));

    const selectProps = {
      value: this.state.value,
      placeholder: "User or group",
      isLoading: !!this.props.loading[A.SEARCH_WHOS_TYPE],
      // When a new option is selected or created
      onChange: option => {
        if (option) {
          this.setState({ value: option.value });
          if (choicePool[option.value]) {
            this.props.onChange(choicePool[option.value]);
          } else {
            this.props.onChange({ invitee: option.label });
          }
        } else {
          this.setState({ value: null });
          this.props.onChange(null);
        }
      },
      // The options to show
      options: _.map(choicePool, (choice, key) => ({
        value: key,
        label: <Who who={choice} />
      })),
      // search function mapping a value to boolean
      filterOption: (option, filter) => {
        const key = option.value;
        const tokens = this.tokenize(filter);
        if (!choicePool[key]) {
          return this.findAllTokensInString(tokens, key);
        }
        let { user, group, invitee } = choicePool[key];
        let isPublic = !!choicePool[key]["public"];
        let searchString = [
          isPublic ? "public" : "",
          user
            ? [
                user.email || "",
                user.display_name || "",
                user.username || ""
              ].join(" ")
            : "",
          group ? group.name : "",
          invitee ? invitee : ""
        ].join(" ");
        let res = this.findAllTokensInString(tokens, searchString);
        return res;
      },
      // User typed some keys
      onInputChange: string => {
        this.setState({ value: string });
        this._debouncedSearchWhos({ q: string });
        return string;
      },
      newOptionCreator: ({ label, labelKey, valueKey }) => {
        return {
          [labelKey]: label,
          [valueKey]: whoKey({ invitee: label })
        };
      },
      isOptionUnique: ({ labelKey, option, valueKey }) => {
        let string = option[valueKey].trim().toLowerCase();
        let unique = !_.any(choicePool, who => {
          if (who.user) {
            return (
              who.user.username.toLowerCase() === string ||
              who.user.email.toLowerCase() === string
            );
          } else if (who.group) {
            return who.group.name.toLowerCase() === string;
          } else if (who["public"]) {
            return string !== "public";
          } else if (who.invitee) {
            return who.invitee.toLowerCase() === string;
          }
        });
        return unique;
      },
      isValidNewOption: ({ label }) => {
        let valid = !!label && label.length > 2 && /^[^\s]+$/.test(label);
        return valid;
      },
      promptTextCreator: label => {
        if (label.indexOf("@") !== -1) {
          return `Add email ${label}`;
        } else {
          return `Add username ${label}`;
        }
      }
    };
    return (
      <div className="who-selector">
        <Creatable {...selectProps} />
      </div>
    );
  }
}
WhoSelector.propTypes = {
  // connect
  loading: React.PropTypes.object.isRequired,
  groups: React.PropTypes.object,
  whoSearch: React.PropTypes.object,
  fetchGroups: React.PropTypes.func,
  searchWhos: React.PropTypes.func,

  // caller
  who: React.PropTypes.object,
  onChange: React.PropTypes.func
};
WhoSelector.defaultProps = {
  who: { expiration: permutils.NO_EXPIRATION }
};
export default connect(
  state => ({
    loading: state.loading,
    groups: state.groups,
    whoSearch: state.whoSearch
  }),
  dispatch => ({
    fetchGroups: payload => dispatch(A.fetchGroups(payload)),
    searchWhos: payload => dispatch(A.searchWhos(payload))
  })
)(WhoSelector);
