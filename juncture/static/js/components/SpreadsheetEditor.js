import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import * as A from "../store/actions";
import Loading from "./Loading";
import ErrorState from "./ErrorState";
import { RowSlideEditor } from "./SlideEditor";
import RowEraEditor from "./EraEditor";
import "./spreadsheeteditor.scss";

class SpreadsheetEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      focusRow: 0,
      focusCol: 0
    };
    this._debouncedChangeSlide = _.debounce(
      slide => this.props.changeSlide({ ...slide }),
      200
    );
    this._debouncedChangeEra = _.debounce(
      era => this.props.changeEra({ ...era }),
      200
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.curSlideId !== this.props.curSlideId) {
      setTimeout(() => {
        let id = `row-${this.props.curSlideId}`;
        let el = document.getElementById(id);
        if (el) {
          let input = el.querySelector('input[type="text"]');
          if (input) {
            input.scrollIntoView({ behavior: "smooth", block: "end" });
            input.focus();
          } else {
            el.scrollIntoView({ behavior: "smooth", block: "end" });
          }
        }
      }, 200);
    }
  }
  changeSlide(slide) {
    this._debouncedChangeSlide({
      timeline_id: this.props.timeline.secret_id,
      slide: slide
    });
  }
  addSlide() {
    this.props.addSlide({
      timeline_id: this.props.timeline.secret_id,
      slide: {
        start_date: { year: new Date().getFullYear() },
        text: { headline: "Edit me" }
      }
    });
  }
  deleteSlide(slide) {
    this.props.deleteSlide({
      timeline_id: this.props.timeline.secret_id,
      slide_id: slide.id
    });
  }

  changeEra(era) {
    this._debouncedChangeEra({
      timeline_id: this.props.timeline.secret_id,
      era: era
    });
  }

  addEra() {
    this.props.addEra({
      timeline_id: this.props.timeline.secret_id,
      era: {
        start_date: { year: new Date().getFullYear() - 1 },
        end_date: { year: new Date().getFullYear() },
        text: { headline: "Edit me" }
      }
    });
  }

  deleteEra(era) {
    this.props.deleteEra({
      timeline_id: this.props.timeline.secret_id,
      era_id: era.id
    });
  }

  render() {
    const slides = this.props.timeline.slides.map(id => this.props.slides[id]);
    const eras = this.props.timeline.eras.map(id => this.props.eras[id]);
    if (!this.props.timeline) {
      return <Loading />;
    }
    return (
      <div>
        <h2>Stories</h2>
        <div className="timeline-spreadsheet-editor">
          <ErrorState
            ids={[A.CHANGE_SLIDE_TYPE, A.ADD_SLIDE_TYPE, A.DELETE_SLIDE_TYPE]}
            className="spreadsheet-error"
            prefix="Server Error: "
            suffix=""
          />
          <div className="header">
            <div className="cell" />
            <div className="cell">Start date</div>
            <div className="cell">End date </div>
            <div className="cell">Title</div>
            <div className="cell">Headline</div>
            <div className="cell">Text</div>
            <div className="cell">Media URL</div>
            <div className="cell">Credits</div>
            <div className="cell">Caption</div>
            <div className="cell">Thumbnail</div>
            <div className="cell">Background</div>
            <div className="cell">Category</div>
            <div className="cell" />
          </div>
          {slides.map((slide, i) => (
            <RowSlideEditor
              slide={slide}
              index={i}
              key={`slide-${i}`}
              timeline={this.props.timeline}
              onChange={this.changeSlide.bind(this)}
              onDelete={this.deleteSlide.bind(this)}
            />
          ))}
        </div>
        <button
          className="btn btn-default mt-2 ml-2"
          onClick={this.addSlide.bind(this)}
        >
          <Loading id={A.ADD_SLIDE_TYPE}>
            <i className="fa fa-plus" /> Add Story
          </Loading>
        </button>
        <h2>Eras</h2>
        <div className="timeline-spreadsheet-editor">
          <ErrorState
            ids={[A.CHANGE_ERA_TYPE, A.ADD_ERA_TYPE, A.DELETE_ERA_TYPE]}
            className="spreadsheet-error"
            prefix="Server Error: "
            suffix=""
          />
          <div className="header">
            <div className="cell" />
            <div className="cell">Start date</div>
            <div className="cell">End date </div>
            <div className="cell">Era name</div>
            <div className="cell" />
          </div>
          {eras.map((era, i) => (
            <RowEraEditor
              era={era}
              index={i}
              key={`era-${i}`}
              timeline={this.props.timeline}
              onChange={this.changeEra.bind(this)}
              onDelete={this.deleteEra.bind(this)}
            />
          ))}
        </div>
        <button
          className="btn btn-default mt-2 ml-2"
          onClick={this.addEra.bind(this)}
        >
          <Loading id={A.ADD_ERA_TYPE}>
            <i className="fa fa-plus" /> Add Era
          </Loading>
        </button>
      </div>
    );
  }
}
SpreadsheetEditor.propTypes = {
  // connect
  addSlide: React.PropTypes.func.isRequired,
  changeSlide: React.PropTypes.func.isRequired,
  deleteSlide: React.PropTypes.func.isRequired,
  addEra: React.PropTypes.func.isRequired,
  changeEra: React.PropTypes.func.isRequired,
  deleteEra: React.PropTypes.func.isRequired,

  // TimelineProvider
  slides: React.PropTypes.object,
  eras: React.PropTypes.object,
  timeline: React.PropTypes.object,
  permissions: React.PropTypes.object,
  auth: React.PropTypes.object,

  // caller
  curSlideId: React.PropTypes.any
};

export default connect(
  state => ({}),
  dispatch => ({
    addSlide: payload => dispatch(A.addSlide(payload)),
    changeSlide: payload => dispatch(A.changeSlide(payload)),
    deleteSlide: payload => dispatch(A.deleteSlide(payload)),
    addEra: payload => dispatch(A.addEra(payload)),
    changeEra: payload => dispatch(A.changeEra(payload)),
    deleteEra: payload => dispatch(A.deleteEra(payload))
  })
)(SpreadsheetEditor);
