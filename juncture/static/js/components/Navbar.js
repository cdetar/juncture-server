import React from "react";
import { connect } from "react-redux";

class Navbar extends React.Component {
  render() {
    return (
      // Largely duplicated from base template.
      <div className="navbar-outer">
        <div className="flex-navbar container">
          <a className="brand" href="/">
            {this.props.settings.SITE.name}
          </a>
          <ul className="nav">
            {this.props.auth.is_authenticated ? (
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="/accounts/settings/account"
                  id="navbar-account-menu-link"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  {this.props.auth.display_name}
                </a>
                <div
                  className="dropdown-menu dropdown-menu-right"
                  aria-labelledby="navbar-account-menu-link"
                >
                  <a
                    className="dropdown-item"
                    href="/accounts/settings/account"
                  >
                    Settings
                  </a>
                  <a
                    className="dropdown-item js-logout"
                    href="/accounts/logout/"
                  >
                    Logout
                  </a>
                  {this.props.auth.is_staff ? (
                    <a className="dropdown-item" href="/admin/">
                      Admin
                    </a>
                  ) : null}
                </div>
              </li>
            ) : (
              [
                <li className="nav-item" key="login">
                  <a className="nav-link" href="/accounts/login/">
                    Login
                  </a>
                </li>,
                <li className="nav-item" key="register">
                  <a className="nav-link" href="/accounts/signup/">
                    Register
                  </a>
                </li>
              ]
            )}
          </ul>
        </div>
      </div>
    );
  }
}
export default connect(state => ({
  auth: state.auth,
  settings: state.settings
}))(Navbar);
