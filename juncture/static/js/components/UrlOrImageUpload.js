import React from "react";
import _ from "lodash";
import Help from "./Help";
import superagent from "superagent/lib/client";
import "./urlorimageupload.scss";

const IMGUR_UPLOAD_URL = "https://api.imgur.com/3/image";

const imgurUpload = (imgurApiKey, imageDataUrl, progressListener) => {
  return new Promise((resolve, reject) => {
    superagent
      .post(IMGUR_UPLOAD_URL)
      .set("Authorization", `Client-Id ${imgurApiKey}`)
      .set("Accept", "application/json")
      .send({ image: imageDataUrl.split("base64,")[1], type: "base64" })
      .on("progress", progressListener)
      .end((err, res) => {
        if (err) {
          return reject(err);
        }
        try {
          let data = JSON.parse(res.text).data;
          let url = data.link.replace("http:", "https:");
          return resolve(url);
        } catch (err) {
          return reject(err);
        }
      });
  });
};

export default class UrlOrImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      progress: 0,
      lastDragEnter: null
    };
  }

  triggerFileChooser(event) {
    event.preventDefault();
    if (document.createEvent) {
      let evt = document.createEvent("MouseEvents");
      evt.initEvent("click", true, false);
      this.fileInput.dispatchEvent(evt);
    } else if (document.createEventObject) {
      this.fileInput.fireEvent("onclick", document.createEventObject());
    } else {
      this.fileInput.click();
    }
  }

  onDragEnter(event) {
    this.setState({ lastDragEnter: event.target, dragging: true });
  }

  onDragLeave(event) {
    if (this.state.lastDragEnter === event.target) {
      this.setState({ lastDragEnter: null, dragging: false });
    }
  }

  onDragOver(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  onDrop(event) {
    event.stopPropagation();
    event.preventDefault();
    if (event.dataTransfer.files[0]) {
      this.ingestFile(event.dataTransfer.files[0]);
    }
  }

  onChangeFile(event) {
    event.stopPropagation();
    event.preventDefault();
    if (event.target.files[0]) {
      this.ingestFile(event.target.files[0]);
    }
  }

  ingestFile(file) {
    if (!file.type.match("image.*")) {
      this.setState({ error: "not-image-file", loading: false });
      return;
    }
    this.props.onChange({ url: "", error: "" });
    this.setState({ loading: true, dragging: false });
    let reader = new FileReader();
    reader.onload = fileEvent => {
      imgurUpload(this.props.imgurApiKey, fileEvent.target.result, p =>
        this.setState({ progress: p.percent })
      )
        .then(url => {
          this.setState({ loading: false, progress: 0 }, () =>
            this.setURL(url)
          );
        })
        .catch(err => {
          console.error(err);
          let msg;
          try {
            msg = _.escape(JSON.parse(err.response.text).data.error);
          } catch (e) {
            msg = err.message;
          }
          this.setState({ loading: false }, () =>
            this.props.onChange({
              url: null,
              error: `Error uploading image - server said: "${msg}"`
            })
          );
        });
    };
    reader.readAsDataURL(file);
  }

  setURL(val) {
    // validate the url.
    let error = "";
    const url = document.createElement("a");
    url.href = val;
    if (_.find(["http:", "https:"], url.protocol)) {
      error = "not-valid-url";
    } else if (!url.pathname) {
      error = "not-valid-url-or-image";
    } else if (val) {
      if (this.isImage(val)) {
        if (url.protocol !== "https:") {
          error = "images-https-only";
        }
      } else if (this.props.imageOnly) {
        error = "not-image-url";
      }
    }
    this.props.onChange({ url: val, error: error });
  }

  isImage(val) {
    const a = document.createElement("a");
    a.href = val;
    return /\.(jpe?g|gif|png|svg)$/i.test(a.pathname || "");
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <div>
            <i className="fa fa-spinner fa-spin" /> Uploading...
          </div>
          <div className="progress">
            <div
              className="progress-bar progress-bar-striped progress-bar-animated"
              role="progressbar"
              style={{ width: `${this.state.progress}%` }}
              aria-valuenow={this.state.progress}
              aria-valuemin="0"
              aria-valuemax="100"
            />
          </div>
        </div>
      );
    }

    let classes = ["url-or-image-upload", "drop-target"];
    if (this.state.dragging) {
      classes.push("dragging");
    }
    if (this.props.error) {
      classes.push("has-danger");
    }

    return (
      <div
        className={classes.join(" ")}
        onDragEnter={this.onDragEnter.bind(this)}
        onDragOver={this.onDragOver.bind(this)}
        onDragLeave={this.onDragLeave.bind(this)}
        onDrop={this.onDrop.bind(this)}
      >
        <input
          type="file"
          style={{ display: "none" }}
          onChange={this.onChangeFile.bind(this)}
          ref={el => (this.fileInput = el)}
        />
        <div className="preview-and-input">
          {this.props.showPreview &&
          this.props.value &&
          this.isImage(this.props.value) ? (
            <img src={this.props.value} className="preview" width={60} />
          ) : (
            ""
          )}
          <div className="input-group">
            <input
              className={this.props.className}
              placeholder={this.props.placeholder}
              type="url"
              value={this.props.value}
              onChange={e => this.setURL(e.target.value)}
            />
            <a
              href="#"
              title="Choose file"
              onClick={this.triggerFileChooser.bind(this)}
              className="input-group-addon file-picker-icon"
            >
              <i className="fa fa-file-image-o" />
            </a>
          </div>
        </div>
        {this.props.error ? (
          <div className="form-control-feedback">
            <Help id={this.props.error} />
          </div>
        ) : (
          ""
        )}
        {this.props.showHelp ? (
          <small className="form-text text-muted">
            Enter URL to anything, drag and drop image, or{" "}
            <a href="#" onClick={this.triggerFileChooser.bind(this)}>
              choose image file
            </a>
            . Uploaded images are publicly visible on{" "}
            <a href="https://imgur.com">Imgur</a>.
          </small>
        ) : (
          ""
        )}
      </div>
    );
  }
}
UrlOrImageUpload.propTypes = {
  imgurApiKey: React.PropTypes.string.isRequired,
  showHelp: React.PropTypes.bool,
  showPreview: React.PropTypes.bool,
  imageOnly: React.PropTypes.bool,
  onChange: React.PropTypes.func.isRequired,
  value: React.PropTypes.string,
  error: React.PropTypes.any,
  className: React.PropTypes.string,
  placeholder: React.PropTypes.string
};
UrlOrImageUpload.defaultProps = {
  showHelp: true,
  showPreview: true,
  imageOnly: false
};
