import React from "react";
import Navbar from "./Navbar";

const FourOhFour = props => (
  <div>
    <Navbar />
    <div className="container">
      <h1>Page Not Found</h1>
    </div>
  </div>
);

export default FourOhFour;
