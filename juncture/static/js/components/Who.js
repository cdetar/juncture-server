import React from "react";

export const whoKey = who => {
  return who.group
    ? `group-${who.group.id}`
    : who.user
    ? `user-${who.user.id}`
    : who.invitee
    ? `invitee-${who.invitee}`
    : who["public"]
    ? "public"
    : null;
};

export const Who = props => {
  const { who } = props;
  return who.user ? (
    <div className="who-display user">
      <span className="image">
        {who.user.profile_image ? (
          <img src={who.user.profile_image} width={32} height={32} />
        ) : (
          <i className="fa fa-user" />
        )}
      </span>
      <span className="name">{who.user.display_name}</span>
    </div>
  ) : who.group ? (
    <div className="who-display group">
      <span className="image">
        <i className="fa fa-users" />
      </span>
      <span className="name">
        {who.group.name} ({who.group.owners.length + who.group.members.length}{" "}
        members)
      </span>
    </div>
  ) : who["public"] ? (
    <div className="who-display public">
      <span className="image">
        <i className="fa fa-globe" />
      </span>
      <span className="name">{"Public"}</span>
    </div>
  ) : who.invitee ? (
    <div className="who-display invitee">
      <span className="image">
        <i className="fa fa-user" />
      </span>
      <span className="name">
        {who.invitee} <em>(invited)</em>
      </span>
    </div>
  ) : null;
};
