/* globals TL TIMELINE_DATA */
import { addCategoryColors, applyMediaTypes } from "./utils/timeline-utils";

const tlData = applyMediaTypes(TIMELINE_DATA["timeline"]);

const timeline3Obj = new TL.Timeline("embed", tlData, { debug: true });
addCategoryColors(timeline3Obj, TIMELINE_DATA["categories"]);
