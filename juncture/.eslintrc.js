module.exports = {
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module",
    allowImportExportEverywhere: false,
    codeFrame: true
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:import/errors"
  ],
  settings: {
    react: {
      version: "15.4"
    }
  },
  parserOptions: {
    ecmaFeatures: {
      /* For decorated classes that are exported, babel-eslint defaults to
       * `export @deco class` syntax; but babel is defaulting to `@deco export
       * class` syntax. We just need the two to agree; the parser option here
       * settles on `@deco export class`.  We may eventually switch back if
       * changes to our babel config prefer the export-first syntax.
       * https://github.com/babel/babel-eslint/issues/662
       * https://github.com/tc39/proposal-decorators/issues/69
       */
      legacyDecorators: true
    }
  },
  rules: {
    // Allow console.warn and console.error, but not console.log
    "no-console": ["error", { allow: ["warn", "error"] }],
    // Allow unused vars as function arguments, since these document interfaces.
    "no-unused-vars": [
      "error",
      {
        vars: "all",
        args: "none",
        ignoreRestSiblings: false
      }
    ],
    // Don't issue prop-types warnings for components without declared
    // propTypes, as all function components will be.
    "react/prop-types": [
      "error",
      {
        skipUndeclared: true
      }
    ]
  },
  env: {
    browser: true,
    jquery: true,
    es6: true
  },
  overrides: [
    // We use commonjs 'require's for some frontend files where the interaction
    // with global includes like jquery/bootstrap/etc gets tricky.
    {
      files: [
        "static/js/index.js",
        "static/js/utils/jquery.js",
        "richtext/static/richtext/editor.js",
      ],
      env: {
        commonjs: true
      }
    }
  ]
};
