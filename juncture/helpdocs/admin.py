from django.contrib import admin

from helpdocs.models import HelpText


# Register your models here.
@admin.register(HelpText)
class HelpTextAdmin(admin.ModelAdmin):
    search_fields = ("key", "text")
    list_display = ["key", "text"]
