from django.db import models


class HelpText(models.Model):
    key = models.SlugField(
        primary_key=True, help_text="Internal id for help text. Don't change this."
    )
    text = models.TextField()

    def __str__(self):
        return self.key
