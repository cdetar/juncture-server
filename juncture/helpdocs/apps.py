from django.apps import AppConfig


class HelpdocsConfig(AppConfig):
    name = "helpdocs"
