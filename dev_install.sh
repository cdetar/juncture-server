#!/bin/bash

set -e

if ! which python3 > /dev/null 2>&1; then
    echo "Please install python3 first."
    exit 1
fi
if ! which node > /dev/null 2>&1; then
    echo "Please install nodejs version 4 or 5."
    exit 1
fi

NODE_VERSION=`node --version`
if ! [[ $NODE_VERSION =~ ^v[67]\.[0-9]+\.[0-9]+$ ]]; then
    echo "Please install nodejs version 6 or 7.  $NODE_VERSION installed."
    exit 1
fi

DIR=$(cd -P -- "$(dirname -- "$0")" && pwd -P)

pipenv install -d
cd $DIR/juncture
npm install
npm run build:dev

if [ ! -e $DIR/juncture/juncture/settings.py ]; then
    cp $DIR/juncture/juncture/example.settings.py $DIR/juncture/juncture/settings.py
fi

pipenv run $DIR/juncture/manage.py makemigrations
pipenv run $DIR/juncture/manage.py migrate

echo 'Install successful!'
